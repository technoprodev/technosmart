<?php
namespace technosmart\models;

use Yii;

class Login extends \yii\base\Model
{
    public $login;
    public $username;
    public $email;
    public $phone;
    public $password;
    public $rememberMe = true;

    public $user;

    public function rules()
    {
        return [
            ['login', 'required', 'on' => 'using-login'],
            ['username', 'required', 'on' => 'using-username'],
            ['email', 'required', 'on' => 'using-email'],
            ['phone', 'required', 'on' => 'using-phone'],
            
            ['password', 'required'],
            ['password', 'validatePassword'],

            ['rememberMe', 'boolean'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'login' => 'Email or Username',
            'username' => 'Username',
            'email' => 'Email',
            'phone' => 'Phone',
            'password' => 'Password',
            'rememberMe' => 'Remember Me',
        ];
    }

    public function validatePassword($attribute, $params)
    {
        if ($user = $this->getUser()) {
            if (!$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect password.');
            }
        } else {
            switch ($this->scenario) {
                case 'using-login':
                    $this->addError('login', 'Username or email is not exists.');
                    break;
                case 'using-username':
                    $this->addError('username', 'Username is not exists.');
                    break;
                case 'using-email':
                    $this->addError('email', 'Email is not exists.');
                    break;
                case 'using-phone':
                    $this->addError('phone', 'Phone is not exists.');
                    break;
                default:
                    $this->addError('login', 'Account is not exists.');
            }
        }
    }

    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? Yii::$app->params['user.passwordResetTokenExpire'] * 24 * 30 : 0);
        } else {
            return false;
        }
    }

    // wajib di override agar bisa makai model User khusus project
    protected function getUser()
    {
        if ($this->user === null) {
            switch ($this->scenario) {
                case 'using-login':
                    $this->user = User::findByLogin($this->login);
                    break;
                case 'using-username':
                    $this->user = User::findByUsername($this->username);
                    break;
                case 'using-email':
                    $this->user = User::findByEmail($this->email);
                    break;
                case 'using-phone':
                    $this->user = User::findByPhone($this->phone);
                    break;
                default:
                    $this->user = User::findByLogin($this->login);
            }
        }

        return $this->user;
    }
}
