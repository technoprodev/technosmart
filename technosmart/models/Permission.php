<?php
namespace technosmart\models;

use Yii;
use yii\helpers\FileHelper;

class Permission
{
    /**
     * clear permissions cache and re-register all controller permissions
     *
     * @param  array  $dir ["namespace\path" => "@folder/path"]
     */
    public static function initAllController(array $dir)
    {
        foreach ($dir as $namespace => $path) {
            $path = Yii::getAlias($path);
            $controllers = FileHelper::findFiles($path, ["only" => ["*Controller.php"]]);

            foreach ($controllers as $file) {
                $strstart = strpos($file, $path)+strlen($path)+1;
                $classname = str_replace("/", "\\", substr($file, $strstart, strrpos($file, ".php")-$strstart));
                $class = $namespace."\\".$classname;

                if (!class_exists($class))
                    continue;

                if (isset($class::$permissions))
                    $permissions = $class::$permissions;
                else
                    $permissions = null;

                if (isset($permissions) && method_exists($class, "id")) {
                    $id = $class::id();
                    
                    Yii::$app->cache->delete("$id-permissions");
                    self::initController($id, $permissions);
                }
            }
        }
        return true;
    }

    /**
     * Checking for permissions update and registering permissions
     *
     * @param string $id controller id to be initialized
     * @param array $permissions
     * @return 
     */
    public static function initController($id, array $permissions)
    {
        if (!is_array($permissions))
            $permissions = [$permissions];

        $cache = Yii::$app->cache;
        $update_cache = false;
        $key = "$id-permissions";

        // get current permissions from cache | database
        $current = $cache->exists($key) ? $cache->get($key) : self::getPermissionsByController($id);
        // get new permissions from $permissions in controller
        $new = self::parse($id, $permissions);


        $update_diff = [];
        // insert new permissions
        $insert_diff = array_diff_ukey($new, $current, function($x, $y) use ($current, $new, &$update_diff) {
            if ($x == $y) {
                if ($current[$x]['description'] != $new[$x]['description']) {
                    // update description
                    $update_diff[] = $new[$x];
                }

                return 0;
            }
            else return $x > $y ? 1 : -1;
        });

        // remove old permissions
        $delete_diff = count($current)>0 ? array_diff_key($current, $new) : [];

        // update description
        if (count($update_diff) > 0) {
            self::update($update_diff);
            $update_cache = true;
        }

        // create new permissions
        if (count($insert_diff) > 0) {
            self::register($insert_diff);
            $update_cache = true;
        }

        // remove old permissions
        if (count($delete_diff) > 0) {
            self::remove(array_keys($delete_diff));
            $update_cache = true;
        }

        // update cache
        if ($update_cache === true)
            $cache->set($key, self::getPermissionsByController($id));
    }

    /**
     * register permission(s) to database
     *
     * @param  array $permission 
     * e.g [["name"=>"perm-name1", "description"=>"description1"], ["perm-name2", "description2"]]
     * @return int "affected rows"
     */
    protected static function register(array $permissions)
    {
        $data = [];
        $time = time();

        foreach ($permissions as $key => $perm)
            $data[] = [
                isset($perm['name']) ? $perm['name'] : current($perm),
                2,
                isset($perm['description']) ? $perm['description'] : next($perm),
                $time,
                $time,
            ];

        return Yii::$app->db->createCommand()->batchInsert("auth_item", ["name", "type", "description", "created_at", "updated_at"], $data)->execute();
    }

    /**
     * update permission(s) descripton
     *
     * @param  array $permission e.g [["permName", "description"]]
     * @return int "affected rows"
     */
    protected static function update($permissions)
    {
        foreach ($permissions as $permission) {
            $updatedRows[] = [$permission['description']];
            $whereRows[] = [$permission['name']];
        }
        self::batchUpdate('auth_item', ['description'], $updatedRows, ['name'], $whereRows);
        return true;
    }

    protected static function batchUpdate(string $table = null, array $updatedColumns = null, array $updatedRows = null, array $whereColumns = [], array $whereRows = [])
    {
        if(!isset($table) || !isset($updatedColumns) || !isset($updatedRows) || count($updatedRows) != count($whereRows))
            throw new \yii\web\HttpException(404, 'Wrong call. Parameters must be (string $table, array $updatedColumns, array $updatedRows, array $whereColumns, array $whereRows');
        
        $sql = null;
        foreach ($updatedRows as $row => $updatedValues) {
            $sql .= "UPDATE {$table} SET ";

            $totalUpdatedColumns = count($updatedColumns);

            foreach ($updatedColumns as $key => $updatedColumn) {
                $updatedValue = $updatedValues[$key];
                if ($updatedValue == null)
                    $sql .= "[[$updatedColumn]] IS NULL";
                else
                    $sql .= "[[$updatedColumn]] = '$updatedValue'";

                if($key == $totalUpdatedColumns - 1)
                    $sql .= " ";
                else
                    $sql .= ", ";
            }
            
            if (count($whereColumns) == 0) {
                $sql .= ";\n";
                continue;
            }

            $sql .= "WHERE ";
            foreach ($whereColumns as $key => $whereColumn) {
                $whereRow = $whereRows[$row][$key];
                if ($whereRow == null)
                    $sql .= "[[$whereColumn]] IS NULL";
                else
                    $sql .= "[[$whereColumn]] = '$whereRow'";

                if($key == $totalUpdatedColumns)
                    $sql .= " AND ";
                else
                    $sql .= ";\n";
            }
        }
        return \Yii::$app->db->createCommand($sql)->execute();
    }

    /**
     * remove permission(s) from database
     *
     * @param  string|array $permission e.g ["permName1", "permName2"]
     * @return int "affected rows"
     */
    protected static function remove($permissions)
    {
        if (!is_array($permissions))
            $permissions = [$permissions];

        return Yii::$app->db->createCommand()->delete("auth_item", "name in ('".implode("','", $permissions)."')")->execute();
    }

    /**
     * get all permissions on specific controller
     * 
     * @param string $controller controller id
     * @return array all permissions belong to $controller
     */
    protected static function getPermissionsByController($controller)
    {
        return (new \yii\db\Query)
            ->select('name, description')
            ->from('auth_item')
            ->where('type = 2')
            ->andwhere('name like "' . $controller . ':%"')
            ->indexBy('name')->all();
    }

    /**
     * parsing data permissions from controller
     *
     * @param  array  $permissions 
     * @param  string $controller controller id
     * @return array formatted new permission(s)
     *
     * $permissions Format
     * [["perm-name", "description"]]
     * [["perm-name"]]
     * ["perm-name"]
     */
    protected static function parse($controller, $permissions)
    {
        $data = [];

        foreach ($permissions as $perm) {
            $desc = null;

            if (is_array($perm)) {
                if (count($perm) == 0)
                    continue;

                @list($perm, $desc) = $perm;
            }
            else if (empty($perm) || trim($perm)=='')
                continue;

            // auto-defined description
            if (!isset($desc))
                $desc = ucwords(str_replace("-", " ", $controller)." ".$perm);

            $perm = self::generatePermissionName($controller, $perm);
            $data[$perm] = ["name" => $perm, "description" => $desc];
        }

        return $data;
    }

    /**
     * generate formatted permission name i.e. controller-id:permission
     *
     * @param string $controller controller id
     * @param string $permission
     * @return string permission name
     */
    protected static function generatePermissionName($controller, $permission)
    {
        return $controller . ':' . $permission;
    }
}