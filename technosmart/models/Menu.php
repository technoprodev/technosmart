<?php
namespace technosmart\models;

use Yii;

class Menu extends \yii\base\Model
{
    public static function menuVisible($menuUrlController, $menuUrlAction)
    {
        try {
            return Yii::$app->createControllerByID($menuUrlController)->canAccess($menuUrlAction);
        } catch (\Throwable $e) {
            return false;
        }
    }
}
