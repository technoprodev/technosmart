<?php
namespace technosmart\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 */
class User extends \technosmart\yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    public $password;
    public $password_repeat;
    public $password_old;

    const STATUS_ACTIVE = '1';
    const STATUS_DISABLE = '0';
    const STATUS_DELETED = '-1';

    public static function tableName()
    {
        return 'user';
    }

    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression("now()"),
            ],
        ];
    }

    public function rules()
    {
        return [
            //id

            //username
            [['username'], 'trim', 'when' => function($model) {
                return $model->username != NULL;
            }],
            [['username'], 'unique', 'message' => 'This username has already been taken.'],
            [['username'], 'string', 'min' => 2, 'max' => 16],
            [['username'], 'match', 'pattern' => '/^(?=.*[a-z])([a-z0-9._-]+)$/i', 'message' => 'Username at least contain 1 word. And only number, letter, dot, dashed and underscore allowed.'],

            //name
            [['name'], 'trim', 'when' => function($model) {
                return $model->name != NULL;
            }],
            [['name'], 'string', 'max' => 64],

            //email
            [['email'], 'trim', 'when' => function($model) {
                return $model->email != NULL;
            }],
            [['email'], 'unique', 'message' => 'This email address has already been taken.'],
            [['email'], 'string', 'max' => 64],
            [['email'], 'email'],

            //phone
            [['phone'], 'trim', 'when' => function($model) {
                return $model->phone != NULL;
            }],
            [['phone'], 'number'],
            [['phone'], 'string', 'min' => 6, 'max' => 32],

            //auth_key
            [['auth_key'], 'required'],
            [['auth_key'], 'string', 'max' => 32],

            //password_hash
            [['password_hash'], 'required'],
            [['password_hash'], 'string', 'max' => 255],

            //password_reset_token
            [['password_reset_token'], 'unique'],
            [['password_reset_token'], 'string', 'max' => 255],

            // status
            [['status'], 'default', 'value' => self::STATUS_ACTIVE],
            [['status'], 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DISABLE, self::STATUS_DELETED]],

            //created_at
            [['created_at'], 'safe'],

            //updated_at
            [['updated_at'], 'safe'],

            //password
            [['password'], 'required', 'on' => ['password', 'password-change']],
            [['password'], 'string', 'min' => 6],

            //password_repeat
            [['password_repeat'], 'required', 'on' => ['password', 'password-change']],
            [['password_repeat'], 'compare', 'compareAttribute'=> 'password', 'message'=> 'Passwords do not match'],

            //password_old
            [['password_old'], 'required', 'on' => ['password-change']],
            [['password_old'], function ($attribute, $params, $validator) {
                if (!$this->validatePassword($this->$attribute)) {
                    $this->addError($attribute, 'Incorrect old password.');
                }
            }, 'on' => ['password-change']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'name' => 'Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'password' => 'Password',
            'password_repeat' => 'Password Repeat',
            'password_old' => 'Old Password',
        ];
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Login secara API based
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['auth_key' => $token, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findByLogin($login)
    {
        return static::find()
            ->where('username = :login or email = :login', [':login' => $login])
            ->andWhere(['status' => self::STATUS_ACTIVE])
            ->one();
    }

    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findByPhone($phone)
    {
        return static::findOne(['phone' => $phone, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * Membandingkan Authkey existing dengan Authkey yang ada di browser
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function getStatusName()
    {
        switch ($this->status) {
            case self::STATUS_ACTIVE:
                return 'Active';
                break;
            case self::STATUS_INACTIVE:
                return 'Inactive';
                break;
            case self::STATUS_DELETED:
                return 'Deleted';
                break;
            default:
                return null;
                break;
        }
    }

    public function getStatusLabels()
    {
        return [
            self::STATUS_ACTIVE => 'Active',
            self::STATUS_INACTIVE => 'Inactive',
            self::STATUS_DELETED => 'Deleted',
        ];
    }
}
