<?php
namespace technosmart\models;

use Yii;

/**
 * This is the model class for table "configuration_file".
 *
 * @property string $target
 * @property string $alias_upload_root
 * @property string $alias_download_base_url
 */
class ConfigurationFile extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'configuration_file';
    }

    public function rules()
    {
        return [
            //target
            [['target'], 'required'],
            [['target'], 'string', 'max' => 64],

            //alias_upload_root
            [['alias_upload_root'], 'required'],
            [['alias_upload_root'], 'string', 'max' => 64],

            //alias_download_base_url
            [['alias_download_base_url'], 'required'],
            [['alias_download_base_url'], 'string', 'max' => 64],
        ];
    }

    public function attributeLabels()
    {
        return [
            'target' => 'Target',
            'alias_upload_root' => 'Alias Upload Root',
            'alias_download_base_url' => 'Alias Download Base Url',
        ];
    }
}
