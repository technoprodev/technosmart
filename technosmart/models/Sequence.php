<?php
namespace technosmart\models;

use Yii;

/**
 * This is the model class for table "sequence".
 *
 * @property string $target
 * @property string $last_sequence
 * @property string $first
 * @property string $second
 * @property string $third
 */
class Sequence extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'sequence';
    }

    public function rules()
    {
        return [
            //target
            [['target'], 'required'],
            [['target'], 'string', 'max' => 32],

            //last_sequence
            [['last_sequence'], 'string', 'max' => 64],

            //first
            [['first'], 'required'],
            [['first'], 'string', 'max' => 1],

            //second
            [['second'], 'string', 'max' => 1],

            //third
            [['third'], 'string', 'max' => 1],
        ];
    }

    public function attributeLabels()
    {
        return [
            'target' => 'Target',
            'last_sequence' => 'Last Sequence',
            'first' => 'First',
            'second' => 'Second',
            'third' => 'Third',
        ];
    }
}
