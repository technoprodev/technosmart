<?php
namespace technosmart\models;

use Yii;

/**
 * This is the model class for table "configuration".
 *
 * @property integer $id
 * @property string $name
 * @property string $type
 * @property string $value
 */
class Configuration extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'configuration';
    }

    public function rules()
    {
        return [
            //id

            //name
            [['name'], 'required'],
            [['name'], 'string', 'max' => 64],

            //type
            [['type'], 'string', 'max' => 8],

            //value
            [['value'], 'string', 'max' => 64],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'type' => 'Type',
            'value' => 'Value',
        ];
    }
}
