<?php
namespace technosmart\assets_manager;

use yii\web\AssetBundle;

class FileInputAsset extends AssetBundle
{
    public $sourcePath = '@technosmart/assets/technoart/asset';
    public $js = [
        'plugin/jasny-fileinput/js/fileinput.min.js',
    ];
    public $css = [
        'plugin/jasny-fileinput/css/fileinput.min.css',
    ];
    public $depends = [
        'technosmart\assets_manager\RequiredAsset',
    ];
}