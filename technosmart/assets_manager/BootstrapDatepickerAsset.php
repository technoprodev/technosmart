<?php
namespace technosmart\assets_manager;

use yii\web\AssetBundle;

class BootstrapDatepickerAsset extends AssetBundle
{
    public $sourcePath = '@technosmart/assets/technoart/asset';
    public $css = [
        'plugin/bootstrap-datepicker/css/bootstrap-datepicker.min.css',
    ];
    public $js = [
        'plugin/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
    ];
    public $depends = [
        'technosmart\assets_manager\RequiredAsset',
    ];
}