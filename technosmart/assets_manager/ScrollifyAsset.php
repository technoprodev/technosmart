<?php
namespace technosmart\assets_manager;

use yii\web\AssetBundle;

class ScrollifyAsset extends AssetBundle
{
    public $sourcePath = '@technosmart/assets/technoart/asset-advance';
    public $js = [
        'plugin/scrollify/jquery.scrollify.min',
    ];
    public $depends = [
        'technosmart\assets_manager\RequiredAsset',
    ];
}