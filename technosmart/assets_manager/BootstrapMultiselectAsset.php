<?php
namespace technosmart\assets_manager;

use yii\web\AssetBundle;

class BootstrapMultiselectAsset extends AssetBundle
{
    public $sourcePath = '@technosmart/assets/technoart/asset';
    public $css = [
        'plugin/bootstrap-multiselect/dist/css/bootstrap-multiselect.css',
    ];
    public $js = [
        'plugin/bootstrap-multiselect/dist/js/bootstrap-multiselect.js',
    ];
    public $depends = [
        'technosmart\assets_manager\RequiredAsset',
    ];
}