<?php
namespace technosmart\assets_manager;

use yii\web\AssetBundle;

class RainbowCoffeeScriptAsset extends AssetBundle
{
    public $sourcePath = '@technosmart/assets/technoart/asset-advance';
    public $css = [
        'plugin-override/rainbow/themes/light.css',
    ];
    public $js = [
        'plugin/rainbow/js/rainbow.min.js',
        'plugin/rainbow/js/language/coffeescript.js',
    ];
    public $depends = [
        'technosmart\assets_manager\RequiredAsset',
    ];
}