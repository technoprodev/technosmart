<?php
namespace technosmart\assets_manager;

use yii\web\AssetBundle;

class ChartAsset extends AssetBundle
{
    public $sourcePath = '@technosmart/assets/technosmart';
    public $css = [
    ];
    public $js = [
        'plugin/chart/Chart.bundle.min.js',
    ];
    public $depends = [
    ];
}