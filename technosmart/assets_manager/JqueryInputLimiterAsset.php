<?php
namespace technosmart\assets_manager;

use yii\web\AssetBundle;

class JqueryInputLimiterAsset extends AssetBundle
{
    public $sourcePath = '@technosmart/assets/technoart/asset';
    public $js = [
        'plugin/jquery.inputlimiter/jquery.inputlimiter.1.3.1.min.js',
    ];
    public $depends = [
        'technosmart\assets_manager\RequiredAsset',
    ];
}