<?php
namespace technosmart\assets_manager;

use yii\web\AssetBundle;

class JqueryMaskedInputAsset extends AssetBundle
{
    public $sourcePath = '@technosmart/assets/technoart/asset';
    public $js = [
        'plugin/jquery.maskedinput/dist/jquery.maskedinput.min.js',
    ];
    public $depends = [
        'technosmart\assets_manager\RequiredAsset',
    ];
}