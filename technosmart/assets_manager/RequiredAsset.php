<?php
namespace technosmart\assets_manager;

use yii\web\AssetBundle;

class RequiredAsset extends AssetBundle
{
    public $sourcePath = '@technosmart/assets/technosmart';
    public $css = [
    ];
    public $js = [
        'js/technosmart5.js',
    ];
    public $depends = [
        'technosmart\assets_manager\TechnoartNewAsset',
    ];
}