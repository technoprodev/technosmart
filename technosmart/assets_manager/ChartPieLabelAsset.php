<?php
namespace technosmart\assets_manager;

use yii\web\AssetBundle;

class ChartPieLabelAsset extends AssetBundle
{
    public $sourcePath = '@technosmart/assets/technosmart';
    public $css = [
    ];
    public $js = [
        'plugin/chart/chartjs-plugin-labels.min.js',
    ];
    public $depends = [
    	'technosmart\assets_manager\ChartAsset',
    ];
}