<?php
namespace technosmart\assets_manager;

use yii\web\AssetBundle;

class IoniconsAsset extends AssetBundle
{
    public $sourcePath = '@technosmart/assets/technoart/asset-advance';
    public $css = [
        'plugin/ionicons-2.0.1/css/ionicons.min.css',
    ];
    public $depends = [
        'technosmart\assets_manager\RequiredAsset',
    ];
}