<?php
namespace technosmart\assets_manager;

use yii\web\AssetBundle;

class FlatpickrAsset extends AssetBundle
{
    public $sourcePath = '@technosmart/assets/technoart/asset';
    public $css = [
        'plugin/flatpickr/flatpickr.min.css',
    ];
    public $js = [
        'plugin/flatpickr/flatpickr.min.js',
    ];
    public $depends = [
        'technosmart\assets_manager\RequiredAsset',
    ];
}