<?php
namespace technosmart\assets_manager;

use yii\web\AssetBundle;

class ChoicesAsset extends AssetBundle
{
    public $sourcePath = '@technosmart/assets/technoart/asset';
    public $css = [
        'plugin/choices/choices.min.css',
    ];
    public $js = [
        'plugin/choices/choices.min.js',
    ];
    public $depends = [
        'technosmart\assets_manager\RequiredAsset',
    ];
}