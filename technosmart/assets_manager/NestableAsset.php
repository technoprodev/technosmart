<?php
namespace technosmart\assets_manager;

use yii\web\AssetBundle;

class NestableAsset extends AssetBundle
{
    public $sourcePath = '@technosmart/assets/technosmart';
    public $css = [
        'plugin/nestable/jquery.nestable.css',
    ];
    public $js = [
        'plugin/nestable/jquery.nestable.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset'
    ];
}