<?php
namespace technosmart\assets_manager;

use yii\web\AssetBundle;

class VueResourceAsset extends AssetBundle
{
    public $sourcePath = '@technosmart/assets/technosmart';
    public $css = [
    ];
    public $js = [
        'plugin/vue/vue-resource.min.js',
    ];
    public $depends = [
    	'technosmart\assets_manager\VueAsset',
    ];
}