<?php
namespace technosmart\assets_manager;

use yii\web\AssetBundle;

class DatatablesAsset extends AssetBundle
{
    public $sourcePath = '@technosmart/assets/technoart/asset';
    public $css = [
        'plugin/datatables/extensions/ColReorder/css/colReorder.dataTables.min.css',
    ];
    public $js = [
        'plugin/datatables/js/jquery.dataTables.min.js',
        'plugin-override/datatables/js/dataTables.bootstrap.js',

        'plugin/datatables/extensions/Buttons/js/dataTables.buttons.min.js',
        'plugin/datatables/extensions/Buttons/js/buttons.flash.min.js',
        'plugin/datatables/extensions/Buttons/js/jszip.min.js',
        'plugin/datatables/extensions/Buttons/js/pdfmake.min.js',
        'plugin/datatables/extensions/Buttons/js/vfs_fonts.min.js',
        'plugin/datatables/extensions/Buttons/js/buttons.html5.min.js',
        'plugin/datatables/extensions/Buttons/js/buttons.print.min.js',
        'plugin/datatables/extensions/Buttons/js/buttons.colVis.min.js',

        'plugin/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js',
    ];
    public $depends = [
        'technosmart\assets_manager\RequiredAsset',
    ];
}