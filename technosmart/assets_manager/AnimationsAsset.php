<?php
namespace technosmart\assets_manager;

use yii\web\AssetBundle;

class AnimationsAsset extends AssetBundle
{
    public $sourcePath = '@technosmart/assets/technoart/asset';
    public $css = [
        'plugin/animations/animations.css',
    ];
    public $depends = [
        'technosmart\assets_manager\RequiredAsset',
    ];
}