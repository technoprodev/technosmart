<?php
namespace technosmart\assets_manager;

use yii\web\AssetBundle;

class Select2Asset extends AssetBundle
{
    public $sourcePath = '@technosmart/assets/technoart/asset';
    public $css = [
        'plugin/select2-4.0.3/dist/css/select2.min.css',
    ];
    public $js = [
        'plugin/select2-4.0.3/dist/js/select2.min.js',
    ];
    public $depends = [
        'technosmart\assets_manager\RequiredAsset',
    ];
}