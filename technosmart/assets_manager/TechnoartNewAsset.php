<?php
namespace technosmart\assets_manager;

use yii\web\AssetBundle;

class TechnoartNewAsset extends AssetBundle
{
    public $sourcePath = '@technosmart/assets/technoart/asset';
    public $css = [
        'plugin/font-awesome-4.6.2/css/font-awesome.min.css',
        'plugin/bootstrap/css/bootstrap.css',
        'css-new/technoart.css',
    ];
    public $js = [
        'plugin/jquery/jquery-1.12.3.min.js',
        'plugin/bootstrap/js/bootstrap.min.js',
        'js-new/technoart.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}