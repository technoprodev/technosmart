<?php
namespace technosmart\assets_manager;

use yii\web\AssetBundle;

class VueDefaultValueAsset extends AssetBundle
{
    public $sourcePath = '@technosmart/assets/technosmart';
    public $css = [
    ];
    public $js = [
        'plugin/vue/vue-default-value.min.js',
    ];
    public $depends = [
    	'technosmart\assets_manager\VueAsset',
    ];
}