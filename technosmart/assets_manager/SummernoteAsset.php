<?php
namespace technosmart\assets_manager;

use yii\web\AssetBundle;

class SummernoteAsset extends AssetBundle
{
    public $sourcePath = '@technosmart/assets/technoart/asset';
    public $js = [
        'plugin/summernote/dist/summernote.min.js',
    ];
    public $css = [
        'plugin/summernote/dist/summernote.css',
    ];
    public $depends = [
        'technosmart\assets_manager\RequiredAsset',
    ];
}