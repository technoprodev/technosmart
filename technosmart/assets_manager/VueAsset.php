<?php
namespace technosmart\assets_manager;

use yii\web\AssetBundle;

class VueAsset extends AssetBundle
{
    public $sourcePath = '@technosmart/assets/technosmart';
    public $css = [
    ];
    public $js = [
        'plugin/vue/vue.min.js',
    ];
    public $depends = [
    ];
}