<?php
namespace technosmart\assets_manager;

use yii\web\AssetBundle;

class AutosizeAsset extends AssetBundle
{
    public $sourcePath = '@technosmart/assets/technoart/asset';
    public $js = [
        'plugin/autosize/dist/autosize.min.js',
    ];
    public $depends = [
        'technosmart\assets_manager\RequiredAsset',
    ];
}