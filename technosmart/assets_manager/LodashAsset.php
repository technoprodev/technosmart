<?php
namespace technosmart\assets_manager;

use yii\web\AssetBundle;

class LodashAsset extends AssetBundle
{
    public $sourcePath = '@technosmart/assets/technosmart';
    
    public $js = [
        'plugin/lodash/lodash.min.js',
    ];

    public $depends = [];
}