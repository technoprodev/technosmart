<?php
namespace technosmart\assets_manager;

use yii\web\AssetBundle;

class DeepstreamAsset extends AssetBundle
{
    public $sourcePath = '@technosmart/assets/technosmart';
    public $css = [
    ];
    public $js = [
        'plugin/deepstream.io-client-js/2.2.1/deepstream.js',
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD,
    ];
    public $depends = [
    ];
}