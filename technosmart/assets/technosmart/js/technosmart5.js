(function($) {
	// Overrides yii action confirmation popup
	yii.confirm = function(msg, func) {
		var modal = fn.confirm({
			title: $(this).attr('confirm-title'),
			body: msg,
			yes: func
		});

		if (!modal.hasClass('in'))
			return false;
	};

	window.baseUrl = $('meta[name="base-url"]').attr("content");

	window.csrfToken = $('meta[name="csrf-token"]').attr("content");
	
	$.ajaxSetup({
		headers: {'X-CSRF-Token': csrfToken}
	});

	var url = function(route, params) {
		var query = [];
		for (var key in params)
			query.push(key + '=' + encodeURIComponent(params[key]));

		if (route === undefined || route == '') {
			if (params===undefined || params.length==0)
				return '';
			else
				return '?' + query.join("&");
		}
		else {
			if (params===undefined || params.length==0)
				return route;
			else
				return route + '?' + query.join("&");
		}
	};

	fn.urlTo = function(route, params) {
		return baseUrl + url(route, params);
	};

	// fn.urlTo();
	// fn.urlTo('dev/index');
	// fn.urlTo('', {id: '1'});
	// fn.urlTo('dev/index', {id: '1'});

})(jQuery);

// START @PLUGIN INIT
var pluginInit = function(scope) {
    var $$ = function(ele) {
        return $(ele, scope);
    };

    // add datatables callback
    if (typeof $.fn.dataTable === 'function') {
        window.datatablesCustomClass = function() {
            $.fn.dataTableExt.oStdClasses.sWrapper = 'dataTables_wrapper';
            $.fn.dataTableExt.oStdClasses.sLength = 'dataTables_length form-wrapper pull-left m-pull-none';
            $.fn.dataTableExt.oStdClasses.sLengthSelect = 'form-dropdown';
            $.fn.dataTableExt.oStdClasses.sFilter = 'dataTables_filter form-wrapper pull-right m-pull-none';
            $.fn.dataTableExt.oStdClasses.sFilterInput = 'form-text';
            $.fn.dataTableExt.oStdClasses.sInfo = 'dataTables_info pull-left m-pull-none m-text-center';
            $.fn.dataTableExt.oStdClasses.sPaging = 'dataTables_paginate pull-right m-pull-none m-margin-top-15 m-text-center paging_';
            $.fn.dataTableExt.oStdClasses.sPageButtonActive = 'active';
        }

        window.datatablesDefaultOptions = {
            autoWidth: false,
            deferRender: true,
            processing: true,
            serverSide: true,
            dom: '<"clearfix margin-bottom-30"lBf><"clearfix margin-bottom-30 scroll-x"rt><"clearfix"ip>',
            lengthMenu: [[10, 25, 50, 100, -1], ['10 entries', '25 entries', '50 entries', '100 entries', 'All entries']],
            orderCellsTop: true,
            // stateSave: true,
            // ordering: false,
            // order: [[1, 'asc'], [2, 'asc']],
            order: [],
            buttons: {
                dom: {
                    container: {
                        className: 'dt-buttons pull-left hidden-sm-less'
                    },
                    button: {
                        className: 'margin-left-5 margin-bottom-5 button',
                        active: 'bg-azure border-azure'
                    }
                },
                buttons: [
                    {
                        extend: 'colvis',
                        title: 'Data show/hide',
                        text: 'Show/hide <i class="fa fa-angle-down"></i>'
                    },
                    {
                        extend: 'copy',
                        title: 'Data export',
                        text: 'Copy'
                    },
                    {
                        extend: 'csv',
                        title: 'Data export',
                        text: 'Csv'
                    },
                    {
                        extend: 'excel',
                        title: 'Data export',
                        text: 'Excel'
                    },
                    {
                        extend: 'pdf',
                        title: 'Data export',
                        text: 'Pdf'
                    },
                    {
                        extend: 'print',
                        title: 'Data export',
                        text: 'Print'
                    }/*,
                    {
                        text: 'My button',
                        action: function ( e, dt, node, config ) {
                            alert( 'Button activated' );
                        }
                    },
                    {
                        extend: 'collection',
                        text: 'Table control',
                        autoClose: true,
                        buttons: [
                            {
                                text: 'Toggle start date',
                                action: function ( e, dt, node, config ) {
                                    dt.column( -2 ).visible( ! dt.column( -2 ).visible() );
                                }
                            },
                            {
                                text: 'Toggle salary',
                                action: function ( e, dt, node, config ) {
                                    dt.column( -1 ).visible( ! dt.column( -1 ).visible() );
                                }
                            }
                        ]
                    }*/
                ]
            },
            language: {
                lengthMenu : '_MENU_',
                search: '',
                searchPlaceholder: 'search all here...',
                buttons: {
                    copyTitle: 'Title',
                    copyKeys: 'copy keys',
                    copySuccess: {
                        _: '%d rows copied',
                        1: '1 row copied'
                    }
                }
            },
            colReorder: true
        }

        $.extend( true, $.fn.dataTable.defaults, {
            fnDrawCallback: function( settings ) {
                technoart.footerFixed();
            }
        });
    }

    // bootstrap scrollspy override
    $.fn.scrollspy.Constructor.prototype.activate = function (target) {
        this.activeTarget = target

        this.clear()

        var selector = this.selector +
          '[data-target="' + target + '"],' +
          this.selector + '[href="' + target + '"]'

        var active = $(selector)
          .parents('li')
          .addClass('active')
          .addClass('open')

        if (active.parent('.dropdown-menu').length) {
          active = active
            .closest('li.dropdown')
            .addClass('active')
        }

        active.trigger('activate.bs.scrollspy')
    }

    $.fn.scrollspy.Constructor.prototype.clear = function () {
        $(this.selector)
          .parentsUntil(this.options.target, '.active')
          .removeClass('active')
          .removeClass('open')
    }

    // bootstrap modal
    $$("[modal],[modal-sm],[modal-md],[modal-lg]").click(function(e) {
        // must left button
        if (e.which != 1)
            return true;
        
        var button = $(this);
        var target = $(button.attr('modal') || '#modal-alert');
        var content = button.attr('modal-content') || '.page-content';
        var title = button.attr('modal-title') || $("title").text();
        var href = button.attr("href");

        var sizeSet = ["modal-sm","modal-md","modal-lg"];

        for (var i in sizeSet) {
            if (button.attr(sizeSet[i])!==undefined) {
                $(".modal-dialog", target).removeClass('modal-sm');
                $(".modal-dialog", target).addClass(sizeSet[i]);
                break;
            }
        }

        target.find(".modal-title").html(title);
        target.modal("show");

        if (href != undefined) {
            var body = target.find(".modal-body");

            body.html('<div class="text-center">Loading ..</div>');

            $.ajax({
                url: href,
                success: function(res) {
                    var content = $(res).find(content);

                    if (content.length == 0)
                        content = $(res);

                    pluginInit(content)
                    body.html(content);
                }
            })
        }

        e.preventDefault();
    });

    // bootstrap plugin
    if (typeof $.fn.tooltip === 'function') {
        $$('[data-toggle="tooltip"]').tooltip();
    }
    if (typeof $.fn.popover === 'function') {
        $$('[data-toggle="popover"]').popover();
    }

    // summernote
    if (typeof $.fn.summernote === 'function') {
        $$('.summernote-default').summernote({
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'strikethrough', 'superscript', 'subscript']],
                ['clear', ['clear']],
                ['fontsize', ['fontsize']],
                ['paragraph', ['ul', 'ol', 'paragraph']],
                ['object', ['hr', 'link', 'picture', 'video', 'table']]
            ]
        });
        $$('.summernote-simple').summernote({
            toolbar: [
                ['style', ['bold', 'italic', 'underline']],
                ['clear', ['clear']]
            ]
        });
    }

    // textarea autosize
    if (typeof autosize === 'function') {
        autosize($$('textarea.textarea-autosize'));
    }

    // input limiter
    if (typeof $.fn.inputlimiter === 'function') {
        $$('.limited').inputlimiter({
            remText: 'You typed %n out of ',
            limitText: '%n char%s allowed.'
        });
    }
    
    // input date picker
    if (typeof $.fn.datepicker === 'function') {
        // adding calendar icon
        var el = $$('.input-datepicker');
        $.each(el, function() {
            var $this = $(this);

            $this.wrap('<div class="input-group"></div>').before('<span class="input-group-addon square"><i class="fa fa-calendar"></i></span>');

            $this.datepicker({
                autoclose: true,
                showDropdowns: true,
                format : 'dd/mm/yyyy',
            });
            
            // $this.trigger2('change');
        });
    }
    
    // input flatpickr
    if (typeof flatpickr === 'function') {
        // adding calendar icon
        var el = $$('.input-flatpickr');

        $.each(el, function(i, input_el) {
            var $this = $(input_el);

            if ($this.siblings(".input-group-addon").length == 0) {
                $this.wrap('<div class="input-group"></div>').before('<span class="input-group-addon square"><i class="fa fa-calendar"></i></span>');
                flatpickr(input_el, {allowInput: true, dateFormat: 'd/m/Y', defaultDate: $this.val()});
            }
        });

        var el = $$('.input-flatpickr-time');
        
        $.each(el, function(i, input_el) {
            var $this = $(input_el);

            if ($this.siblings(".input-group-addon").length == 0) {
                $this.wrap('<div class="input-group"></div>').before('<span class="input-group-addon square"><i class="fa fa-calendar"></i></span>');
                flatpickr(input_el, {allowInput: true, timeFormat: 'd/m/Y H:i', enableTime: true, time_24hr: true, defaultHour: 8});
            }
        });
    }

    // input mask
    if (typeof $.fn.mask === 'function'){
        $.mask.definitions['~']='[+-]';
        $$('.input-mask-date').mask('99/99/9999', {autoclear: false});
    }

    // scrollToFixed
    if (typeof $.fn.scrollToFixed === 'function'){
        $('.fixed').scrollToFixed({
            zIndex: 0
        });
    }

    // scrollify
    if (typeof $.scrollify === 'function'){
        $.scrollify({
            section : ".scrollify"
        });
    }

    // select2
    if (typeof $.fn.select2 === 'function'){
        var ele = $$('.select2');

        ele.on('select2:close', function() {
            this.dispatchEvent(new CustomEvent('change'));
        });
        
        ele.select2();
    }

    // choices
    if (typeof Choices === 'function' && $$('.input-choices').length){
        window.choices = new Choices('.input-choices');
    }
};
// /END @PLUGIN INIT

$(document).ready(function() {
    pluginInit();
});