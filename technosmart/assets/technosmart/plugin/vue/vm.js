var router = new VueRouter()

const Home = { template: '<div>Home</div>' }
const SecretQuote = { template: '<div>SecretQuote</div>' }
const Login = { template: '<div>Login</div>' }
const Signup = { template: '<div>Signup</div>' }

router.routes = [
    { path: '/home', component: Home},
    { path: '/secretquote', component: SecretQuote},
    { path: '/login', component: Login},
    { path: '/signup', component: Signup}
]

// Redirect to the home route if any routes are unmatched
/*router.redirect({
  '*': '/home'
})*/

/*Vue.nextTick(function (el) {
    pluginInit(el);
})*/

Vue.component('todo-item', {
    // The todo-item component now accepts a
    // "prop", which is like a custom attribute.
    // This prop is called todo.
    props: ['todong'],
    template: '<li>{{ todong.text }}</li>'
});

Vue.component('coba1', {
    template: '<div class="border-red padding-20" style="width:60px;"></div>'
});

Vue.component('app-layout', {
    template:
    '<div class="container">' +
        '<header>' +
            '<slot name="header">no header, sorry</slot>' +
        '</header>' +
        '<main>' +
            '<slot>nothing to display, sorry</slot>' +
        '</main>' +
        '<footer>' +
            '<slot name="footer"></slot>' +
        '</footer>' +
        '<coba1/>' +
        '<coba2/>' +
    '</div>'
    // template: 'ngento' kl gini doang ngga bisa ya fan
    // template: '<div>ngento</div><div>ngento</div>' gini juga ngga bisa ya fan
});

Vue.component('coba2', {
    template: '<div class="border-blue padding-20" style="width:60px;"></div>'
});

Vue.component('coba', {
    props: {
        todong: null,
        propsMessage: null,
        propsString: null,
        // basic type check (`null` means accept any type)
        propA: Number,
        // multiple possible types
        propB: [String, Number],
        // a required string
        propC: {
          type: String,
          required: true
        },
        // a number with default value
        propD: {
          type: Number,
          default: 100
        },
        // object/array defaults should be returned from a
        // factory function
        propE: {
          type: Object,
          default: function () {
            return { message: 'hello' }
          }
        },
        // custom validator function
        propF: {
          validator: function (value) {
            return value > 10
          }
        }
    },
    data: function() {
        return { message: this.propsMessage }
    },
    methods: {
        methodchild: function() {
            this.$emit('methodchild');
            console.log('methodchild triggered');
        }
    },
    template: 
    '<div> {{this.message}} + {{this.propsMessage}} + {{this.propsString}}' +
        '<template v-for="(value, key, index) in this.todong">' +
            '<div class="form-group">' +
                '<label class="control-label col-xs-1">{{ key+1 }}</label>' +
                '<div class="col-xs-4">' +
                    '<input v-model="value.text" v-on:focus="methodchild" class="form-control">' +
                '</div>' +
            '</div>' +
        '</template>' +
    '</div>'
});

Vue.component('currency-input', {
  template: '\
    <span>\
      $\
      <input\
        ref="inpud"\
        v-bind:value="value"\
        v-on:input="updateValue($event.target.value)"\
        class="form-control"\
      >\
    </span>\
  ',
  props: ['value'],
  methods: {
    // Instead of updating the value directly, this
    // method is used to format and place constraints
    // on the input's value
    updateValue: function (value) {
      var formattedValue = value
        // Remove whitespace on either side
        .trim()
        // Shorten to 2 decimal places
        .slice(0, value.indexOf('.') + 3)
      // If the value was not already normalized,
      // manually override it to conform
      if (formattedValue !== value) {
        // this.value = formattedValue
        // this.$emit('input', Number(formattedValue))
        this.$refs.inpud.value = formattedValue
      }
      // Emit the number value through the input event
      this.$emit('input', Number(formattedValue))
    }
  }
});

var vm = new Vue({
    el: '#app',
    router,
    data: {
        message: 'Hello Vue.js!',
        seen: true,

        todos: [
            { text: 'Learn JavaScript' },
            { text: 'Learn Vue' },
            { text: 'Build something awesome' }
            // vm.todos.push({ text: 'New item' })
        ],
        object: {
            FirstName: 'John',
            LastName: 'Doe',
            Age: 30
        },

        firstName: 'Foo',
        lastName: 'Bar',

        bgLightAzure: false,
        classObject: {
            border: false,
            'text-azure': false
        },
        ffParisienne: 'ff-parisienne',
        fBold: 'f-bold',

        inputText: null,
        inputTextLazy: null,
        inputTextNumber: null,
        inputTextTrim: null,
        textarea: null,
        radio: null,
        checkbox: [],
        checkboxSingle: null,
        select: null,
        selectMultiple: [],
        selectOption: null,
        option: [
            { value: 'Alpha', text: 'Alpha'},
            { value: 'Echo', text: 'Echo'},
            { value: 'India', text: 'India'},
            { value: 'Oscar', text: 'Oscar'},
            { value: 'Ultra', text: 'Ultra'},
            { value: 'Bravo', text: 'Bravo'},
            { value: 'Charlie', text: 'Charlie'},
            { value: 'Delta', text: 'Delta'},
            { value: 'Fanta', text: 'Fanta'},
            { value: 'Golf', text: 'Golf'},
            { value: 'Hotel', text: 'Hotel'},
            { value: 'Juliet', text: 'Juliet'},
            { value: 'Kilo', text: 'Kilo'},
            { value: 'London', text: 'London'},
            { value: 'Mama', text: 'Mama'},
            { value: 'Nano', text: 'Nano'},
            { value: 'Papa', text: 'Papa'},
            { value: 'Queen', text: 'Queen'},
            { value: 'Romeo', text: 'Romeo'},
            { value: 'Sierra', text: 'Sierra'},
            { value: 'Tango', text: 'Tango'},
            { value: 'Victor', text: 'Victor'},
            { value: 'Whiskey', text: 'Whiskey'},
            { value: 'X-ray', text: 'X-ray'},
            { value: 'Yankee', text: 'Yankee'},
            { value: 'Zebra', text: 'Zebra'}
        ],

        currentView: 'home',

        price: 900.00,
    },
    computed: {
        // a computed getter
        computedMessage: function () {
            // `this` points to the vm instance
            return this.message.split('').reverse().join('')
        },
        fullName: {
            // getter
            get: function () {
                return this.firstName + ' ' + this.lastName
            },
            // setter
            set: function (newValue) {
                var names = newValue.split(' ')
                this.firstName = names[0]
                this.lastName = names[names.length - 1]
            }
        }
    },
    methods: {
        reverseMessage: function (arg1, arg2) {
            // fn.alert(arg1, arg2);
            this.message = this.message.split('').reverse().join('')
        },

        toggleBgLightAzure: function() {
            this.bgAzure = !this.bgAzure;
        },
        toggleBorder: function() {
            this.classObject.border = !this.classObject.border;
        },
        toggleTextAzure: function() {
            this.classObject['text-azure'] = !this.classObject['text-azure'];
        },
        toggleFfParisienne: function() {
            if (this.ffParisienne)
                this.ffParisienne = '';
            else
                this.ffParisienne = 'ff-parisienne';
        },
        toggleFBold: function() {
            if (this.fBold)
                this.fBold = '';
            else
                this.fBold = 'f-bold';
        },
        methodParent1: function() {
            console.log('method parent1 triggered');
        },
        methodParent2: function() {
            console.log('method parent2 triggered');
        }
    },
    filters: {
        capitalize: function (value, arg1 = 'default', arg2 = 'default') {
            if (!value) return '';
            value = value.toString();
            return value.charAt(0).toUpperCase() + value.slice(1) + ' ' + arg1 + ' ' + arg2;
        }
    },
    watch: {
        message: function(newVal, oldVal){
            this.seen = !this.seen;
        }
    },
    components: {
        home: {
            template: '<div>home!</div>'
        },
        posts: {
            template: '<div>post!</div>'
        },
        archive: {
            template: '<div>archive!</div>'
        }
    }
});

var i = 0;
vm.$watch('seen', function (newVal, oldVal) {
    var temp = vm.firstName;
    vm.firstName = vm.lastName;
    vm.lastName = temp;
    i++; console.log(i);
});

var watchExampleVM = new Vue({
    el: '#watch-example',
    data: {
        question: '',
        answer: 'I cannot give you an answer until you ask a question!'
    },
    watch: {
        // whenever question changes, this function will run
        question: function (newQuestion) {
            this.answer = 'Waiting for you to stop typing...'
            this.getAnswer()
        }
    },
    methods: {
        // _.debounce is a function provided by lodash to limit how
        // often a particularly expensive operation can be run.
        // In this case, we want to limit how often we access
        // yesno.wtf/api, waiting until the user has completely
        // finished typing before making the ajax request. To learn
        // more about the _.debounce function (and its cousin
        // _.throttle), visit: https://lodash.com/docs#debounce
        getAnswer: _.debounce(
            function () {
                var vm = this
                if (this.question.indexOf('?') === -1) {
                    vm.answer = 'Questions usually contain a question mark. ;-)'
                    return
                }
                vm.answer = 'Thinking...'
                this.$http.get('https://yesno.wtf/api').then( function(response) {
                    vm.answer = _.capitalize(response.data.answer)
                }, function (error) {
                    vm.answer = 'Error! Could not reach the API. ' + error
                })
            },
            // This is the number of milliseconds we wait for the
            // user to stop typing.
            500
        )
    }
});