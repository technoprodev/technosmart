<?php
$config = [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'view' => [
            'class' => 'technosmart\yii\web\View',
        ],
        'd' => [
            'class' => 'technosmart\yii\base\Debug',
        ],
    ],
];

if (php_sapi_name() != 'cli') {
    $config['components']['assetManager'] = [
        'bundles' => [
            'yii\web\JqueryAsset' => [
                'sourcePath' => '@technosmart/assets/technoart/asset',
                'js' => [
                    'plugin/jquery/jquery-1.12.3.min.js',
                ]
            ],
            'yii\bootstrap\BootstrapAsset' => [
                'sourcePath' => '@technosmart/assets/technoart/asset',
                'css' => [
                    'plugin/bootstrap/css/bootstrap.min.css',
                ]
            ],
            'yii\bootstrap\BootstrapPluginAsset' => [
                'sourcePath' => '@technosmart/assets/technoart/asset',
                'js' => [
                    'plugin/bootstrap/js/bootstrap.min.js',
                ],
            ],
            'yii\gii\GiiAsset' => [
                'sourcePath' => '@technosmart/yii/gii/assets',
            ],
        ],
    ];

    $config['components']['urlManager'] = [
        'class' => 'yii\web\UrlManager',
        'enablePrettyUrl' => true,
        'showScriptName' => false,
        'enableStrictParsing' => true,
        // Whether to enable strict parsing. If strict parsing is enabled, the incoming requested URL must match at least one of the $rules in order to be treated as a valid request. Otherwise, the path info part of the request will be treated as the requested route. This property is used only when $enablePrettyUrl is true.
        'rules' => [
            '' => 'site/index',

            '<alias:contact-us|signup|confirm|login|logout|error>' => 'site/<alias>',
            '<controller:\b(?!\b(site)\b)[\w\-]+>/<id:[\d]+>' => '<controller>/index',
            '<controller:\b(?!\b(site)\b)[\w\-]+>' => '<controller>/index',

            '<controller:\b(?!\b(site)\b)[\w\-]+>/<action:\b(?!\b(index)\b)[\w\-]+>/<id:[\d]+>' => '<controller>/<action>',
            '<controller:\b(?!\b(site)\b)[\w\-]+>/<action:\b(?!\b(index)\b)[\w\-]+>' => '<controller>/<action>',

            '<module:[\w\-]+>/<controller:[\w\-]+>/<id:[\d]+>' => '<module>/<controller>/index',
            '<module:[\w\-]+>/<controller:[\w\-]+>' => '<module>/<controller>/index',

            '<module:[\w\-]+>/<controller:[\w\-]+>/<action:\b(?!\b(index)\b)[\w\-]+>/<id:[\d]+>' => '<module>/<controller>/<action>',
            '<module:[\w\-]+>/<controller:[\w\-]+>/<action:\b(?!\b(index)\b)[\w\-]+>' => '<module>/<controller>/<action>',

            '<url:.+/>' => 'site/redirect-slash',
        ],
        // <word: -> cuma nangkap yang sebelum tanda tanya
        // '<controller:[\w\-]+>/<action:[\w\-]+>/<<param1:[\w\-]+>:[a-zA-Z0-9_\-\/]+>' => '<controller>/<action>?param1',
    ];

    /*$config['components']['response'] = [
        'class' => 'yii\web\Response',
        'on beforeSend' => function ($event) {
            $response = $event->sender;
            // var_dump($response);
            if ($response->data !== null && Yii::$app->request->get('suppress_response_code')) {
                // exit();
                $response->data = [
                    'success' => $response->isSuccessful,
                    'data' => $response->data,
                ];
                $response->statusCode = 200;
            }
        },
    ];*/
}

if (php_sapi_name() != 'cli' && YII_ENV == 'dev') {
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'technosmart\yii\gii\Module',
        'generators' => [
            'model' => [
                'class' => 'technosmart\yii\gii\generators\ultimate\Generator',
                'templates' => [
                    'default' => '@technosmart/yii/gii/generators/ultimate/modified-template',
                ]
            ]
        ],
    ];

    $config['components']['assetManager']['forceCopy'] = true;
}

if (php_sapi_name() != 'cli' && YII_ENV == 'dev' && YII_DEBUG) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];
}

if (php_sapi_name() == 'cli' && YII_ENV == 'dev') {
    $config['controllerMap']['migrate-init'] = [
        'class' => 'yii\console\controllers\MigrateController',
        'migrationPath' => '@technosmart/modules/dev/migrations',
    ];
    $config['controllerMap']['migrate-dev'] = [
        'class' => 'yii\console\controllers\MigrateController',
        'migrationPath' => '@app/migrations_dev',
    ];
    $config['controllerMap']['migrate-blog'] = [
        'class' => 'yii\console\controllers\MigrateController',
        'migrationPath' => '@technosmart/modules/blog/migrations',
    ];
    $config['controllerMap']['migrate-faq'] = [
        'class' => 'yii\console\controllers\MigrateController',
        'migrationPath' => '@technosmart/modules/faq/migrations',
    ];
    $config['controllerMap']['migrate-location'] = [
        'class' => 'yii\console\controllers\MigrateController',
        'migrationPath' => '@technosmart/modules/location/migrations',
    ];
    $config['controllerMap']['migrate-testimonial'] = [
        'class' => 'yii\console\controllers\MigrateController',
        'migrationPath' => '@technosmart/modules/testimonial/migrations',
    ];
}

return $config;