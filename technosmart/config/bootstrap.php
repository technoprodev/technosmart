<?php
Yii::setAlias('@technosmart', dirname(__DIR__));

function ddx() {
    return call_user_func_array([Yii::$app->d, "ddx"], func_get_args());
}

function dd() {
    return call_user_func_array([Yii::$app->d, "dd"], func_get_args());
}
