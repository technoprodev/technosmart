import { Component, ViewChild } from '@angular/core';
import { ionicBootstrap, Platform, Nav, NavController, Storage, LocalStorage,AlertController} from 'ionic-angular';
import { StatusBar ,Push} from 'ionic-native';
import { Http, Headers, RequestOptions, Response , Jsonp, ConnectionBackend,HTTP_PROVIDERS} from '@angular/http';  
import { bootstrap } from '@angular/platform-browser-dynamic';
import { disableDeprecatedForms, provideForms } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

import { Page1 } from './pages/page1/page1';
import { Page2 } from './pages/page2/page2';
import { Page3 } from './pages/page3/page3';
import { Login } from './pages/login/login';
import { Logout } from './pages/logout/logout';

@Component({
  templateUrl: 'build/app.html'
})
class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = Login;

  pages: Array<{title: string, component: any}>;

  public local = new Storage(LocalStorage);

  constructor(public platform: Platform,public alertCtrl: AlertController,public http : Http) {
    this.initializeApp();
    
    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Dashboard', component: Page1 },
      { title: 'Form', component: Page2 },
      { title: 'List', component: Page3 },
      { title: 'Logout', component: Logout},
    ];
  }

  ngAfterViewInit() {
    this.local.get("isLogin").then((value) => {
      if(value == "true"){
        this.nav.setRoot(Page1);
      }
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      let push = Push.init({
          android: {
              senderID: "782410942969"
          },
          ios: {
              alert: "true",
              badge: false,
              sound: "true"
          },
          windows: {}
      });
      push.on('registration', (data) => {
          console.log("device token ->", data.registrationId);
          //TODO - send device token to server
          /*let link = "http://192.168.1.3/ionic-api/api.php";

          let datas = JSON.stringify(data.registrationId);
          
          this.http.post(link, datas)
          .subscribe(data => {
            console.log(data.text());
          },error => {
              console.log("Oooops!");
          });*/
      });
      push.on('notification', (data) => {
          console.log('message', data.message);
          let self = this;
          //if user using app and push notification comes
          if (data.additionalData.foreground) {
              // if application open, show popup
              let confirmAlert = this.alertCtrl.create({
                  title: 'New Notification2',
                  message: data.message,
                  buttons: [{
                      text: 'Ignore',
                      role: 'cancel'
                  }, {
                      text: 'View',
                      handler: () => {
                          //TODO: Your logic here
                          //self.nav.push(SomeComponent, {message:data.message});
                      }
                  }]
              });
              confirmAlert.present();
          } else {
              //if user NOT using app and push notification comes
              //TODO: Your logic on click of push notification directly
              //self.nav.push(SomeComponent, {message:data.message});
              console.log("Push notification clicked");
              let confirmAlerts = this.alertCtrl.create({
                  title: 'New Notification1',
                  message: data.message,
                  buttons: [{
                      text: 'Ignore',
                      role: 'cancel'
                  }, {
                      text: 'View',
                      handler: () => {
                          //TODO: Your logic here
                          //self.nav.push(SomeComponent, {message:data.message});
                      }
                  }]
              });
              confirmAlerts.present();
          }
      });
      push.on('error', (e) => {
          console.log(e.message);
      });

    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  

}

ionicBootstrap(MyApp, [
  disableDeprecatedForms(), // disable deprecated forms
  provideForms(),
  HTTP_PROVIDERS
]).catch(err => console.error(err));
