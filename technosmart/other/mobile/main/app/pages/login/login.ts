import { Component , OnInit, ViewChild} from '@angular/core';
import { NavController, ToastController, Storage, LocalStorage, MenuController, AlertController} from 'ionic-angular';
import { LocalNotifications } from 'ionic-native';
import { Validators } from '@angular/common';
import { REACTIVE_FORM_DIRECTIVES, FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Http, Response} from '@angular/http';
import { Page1 } from '../page1/page1';

@Component({
  selector : 'my-app',
  templateUrl: 'build/pages/login/login.html',
  directives : [REACTIVE_FORM_DIRECTIVES]
})
export class Login implements OnInit{

  public loginForm: FormGroup;
  public inputForm: FormGroup;
  public authType : string;
  public submitted: boolean;
  public local;

  constructor(public navCtrl: NavController,public toastCtrl: ToastController,public http : Http,public menu : MenuController,public alertCtrl: AlertController) {
  	this.authType = "login";
  	this.local = new Storage(LocalStorage);
  	this.menu.swipeEnable(false);

    this.schedule();
    console.log(new Date("2016-09-20 10:20:30"));
    LocalNotifications.on("click", (notification, state) => {
          let alert = alertCtrl.create({
              title: "Notification Clicked",
              subTitle: "You just clicked the scheduled notification",
              buttons: ["OK"]
          });
          alert.present();
      });
  }

  ngOnInit() {
    this.loginForm = new FormGroup({
        username: new FormControl('', [<any>Validators.required, <any>Validators.minLength(5)]),
        password: new FormControl('', [<any>Validators.required, <any>Validators.minLength(5)]),
    });

    this.inputForm = new FormGroup({
        name: new FormControl('', [<any>Validators.required]),
        birthplace: new FormControl('', [<any>Validators.required]),
        birthdate: new FormControl('', [<any>Validators.required]),
        address: new FormControl('', [<any>Validators.required]),
        username: new FormControl('', [<any>Validators.required, <any>Validators.minLength(5)]),
        password: new FormControl('', [<any>Validators.required, <any>Validators.minLength(5)]),
    });
  }

  login(model,isValid){
  	this.submitted = true; // set form submit to true
    if(!isValid){
      this.presentToast('Harap lengkapi data Anda dengan benar!');
    }else{
      let link = "http://192.168.1.3/technosmart-rekruitasi/recruitment/web/index.php?r=rest/login";

      let data = JSON.stringify(model);

      this.http.post(link, data)
      .subscribe(data => {
      	var d = data.json();
        this.presentToast(d.message);
        this.local.set("isLogin",true);
        this.local.set("id",d.data.id);
        this.local.set("username",d.data.username);
        this.local.set("token",d.data.token);
        this.navCtrl.setRoot(Page1);
      },error => {
        var e = error.json();
        this.presentToast(e.message);
      });
    }
  }

  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }; 

  schedule() {
        LocalNotifications.schedule({
            title: "Test Title",
            text: "Delayed Notification",
            at: new Date(new Date().getTime() + 10 * 1000 * 60),
            sound: null
        });
    }
}
