import { Component, OnInit } from '@angular/core';
import { NavController, NavParams , AlertController ,ToastController } from 'ionic-angular';
import { Validators } from '@angular/common';
import { REACTIVE_FORM_DIRECTIVES, FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Http, Response} from '@angular/http';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'my-app',
  templateUrl: 'build/pages/page2/page2.html',
  directives: [REACTIVE_FORM_DIRECTIVES]
})
export class Page2 implements OnInit{
  public inputForm: FormGroup;
  public submitted: boolean; 
  public data;

  constructor(public navCtrl: NavController, navParams: NavParams ,public alertCtrl : AlertController,private _fb: FormBuilder, public toastCtrl : ToastController,public http : Http) {
    this.http   = http;
    this.data = {};
    this.data.name = '';
    this.data.response = '';
  }

  ngOnInit() {
    this.inputForm = new FormGroup({
        name: new FormControl('', [<any>Validators.required, <any>Validators.minLength(6)]),
        birthplace: new FormControl('', [<any>Validators.required]),
        birthdate: new FormControl('', [<any>Validators.required]),
        address: new FormControl('', [<any>Validators.required])
    });
  }

/*  save(model, isValid) {
    this.submitted = true; // set form submit to true
    if(!isValid){
      this.presentToast('Harap lengkapi data Anda dengan benar!');
    }else{
      let url = "http://192.168.1.3/technosmart-rekruitasi/recruitment/web/index.php?r=dev/rest";
      let body = JSON.stringify(this.inputForm.value);
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });

      this.http.post(url,body,options)
       .subscribe(
            function(response) {
              console.log("Response :" + response);
            },
            function(error) { 
              console.log("Error :" + error);
            },
            function() { console.log("the subscription is completed")}
        );

    }
  }*/
  save(model,isValid){
    this.submitted = true; // set form submit to true
    if(!isValid){
      this.presentToast('Harap lengkapi data Anda dengan benar!');
    }else{
      // let link = "http://192.168.1.3/ionic-api/api.php";
      let link = "http://192.168.1.3/technosmart-rekruitasi/recruitment/web/index.php?r=rest";

      let data = JSON.stringify(this.inputForm.value);
      
      this.http.post(link, data)
      .subscribe(data => {
        console.log(data.text());
      },error => {
          console.log("Oooops!");
      });
    }
  }
  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }; 

}
