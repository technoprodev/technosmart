import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Http, Response} from '@angular/http';

import { Config } from '../config/config';

@Component({
  templateUrl: 'build/pages/job-list/job-list.html',
  providers: [Config]
})
export class JobList {

  public joblist = [];
  public jobcategory = [];
  detailJob: any;
  public jobdata = [];
  public category = "null";
  public infiniteNow = 0;
  public noMore = false;
  public limitItem = 10;

  constructor(private navCtrl: NavController,public http : Http,public conf: Config,navParams: NavParams) {
    this.detailJob = navParams.get('id');
  	if(!this.detailJob){
	  	this.getJob();
  	}else{
  		this.getJob(this.detailJob);
  	}
	if(this.jobcategory.length < 1){
		this.getCategory();
	}
  }

  categoryChange(){
  	this.getJob();
  	this.infiniteNow = 0;
  	this.noMore = false;
  }

  doRefresh(refresher) {
  	this.getJob();
  	this.infiniteNow = 0;
  	this.noMore = false;
	refresher.complete();
  }

  getJob(id = null,start = 0,limit = this.limitItem){
  	if(id){
	    this.jobdata = [];

	    let link = this.conf.url('technosmart-rekruitasi/recruitment/web/index.php?r=rest/job&id=' + id + '&category=' + this.category);

	    this.http.get(link)
	      .subscribe(data => {
	      	var d = data.json();
	        this.jobdata = d[0];
	    },error => {
	        var e = error.json();
	        console.log(e);
	    });
  	}else{

	    let link = this.conf.url('technosmart-rekruitasi/recruitment/web/index.php?r=rest/job&start=' + start + '&limit=' + limit + '&category=' + this.category);

	    this.http.get(link)
	      .subscribe(data => {
	      	var d = data.json();
	      	if(start==0){
		        this.joblist = d;
	      	}else{
	      		if(d.length > 0){
			      	for (var key in d) {
			      		this.joblist.push(d[key]);
					}
	      		}else{
	      			this.noMore = true;
	      		}
	      	}
	    },error => {
	        var e = error.json();
	        console.log(e);
	    });
  	}
  }

  getCategory(){
	this.jobcategory = [];

    let link = this.conf.url('technosmart-rekruitasi/recruitment/web/index.php?r=rest/job-category');

    this.http.get(link)
      .subscribe(data => {
      	var d = data.json();
        this.jobcategory = d;
    },error => {
        var e = error.json();
        console.log(e);
    });
  }

  toHumanDate(date){
  	return this.conf.toHumanDate(date);
  }

  tapToDetail(event,id) {
    // That's right, we're pushing to ourselves!
    this.navCtrl.push(JobList, {
      id : id
    });
  }

  doInfinite(event){
  	if(!this.noMore){
  	  	this.infiniteNow =  this.infiniteNow + this.limitItem;
  	  	this.getJob(null,this.infiniteNow);
  	}
  	event.complete();
  }

  tapToBookmark($event,id){
  
  }
}
