export class Config{

  public serverDestination: string;
  public loginUrlDestination: string;
  public companyName: string;
  public monthList = [];
  public access_token;
  public auth;

  constructor() {
     this.access_token          = "04aec03ad1c692825a0285b8ebf2415a";
     this.auth                  = "&access-token="+this.access_token;
     this.serverDestination     = "http://localhost";
     this.loginUrlDestination   = this.url("technosmart-rekruitasi/recruitment/web/index.php?r=rest/login");
     this.companyName           = "Technosmart";
     this.monthList             = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];
  }

  url(destination: string){
    return this.serverDestination + "/" + destination + this.auth;
  }

  toHumanDate(date: string){
    var mydate = new Date(date);
    var month = this.monthList[mydate.getMonth()];
    var str = mydate.getDate() + ' ' + month + ' ' + mydate.getFullYear();
    return str;
  }

}
