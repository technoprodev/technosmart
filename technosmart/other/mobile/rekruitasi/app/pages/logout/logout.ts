import { Component } from '@angular/core';
import { NavController , Storage, LocalStorage} from 'ionic-angular';
import { Login } from '../login/login';

@Component({
  templateUrl: 'build/pages/logout/logout.html',
})

export class Logout {

  public local = new Storage(LocalStorage);

  constructor(private navCtrl: NavController) {
  	this.local.clear();
  	this.navCtrl.setRoot(Login);
  }

}
