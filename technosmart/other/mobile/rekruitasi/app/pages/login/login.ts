import { Component, OnInit } from '@angular/core';
import { NavController, MenuController, ToastController, Storage, LocalStorage, } from 'ionic-angular';
import { Validators } from '@angular/common';
import { REACTIVE_FORM_DIRECTIVES, FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Http, Response} from '@angular/http';

import { Config } from '../config/config';
import { JobList } from '../job-list/job-list';


@Component({
  selector : 'my-app',
  templateUrl: 'build/pages/login/login.html',
  providers: [Config]
})
export class Login implements OnInit{

  public loginForm: FormGroup;
  public inputForm: FormGroup;
  public submitted: boolean;
  public nowYear = new Date().getFullYear();
  public companyName: String;
  public authType : string;
  public local;


  constructor(private navCtrl: NavController,public menuCtrl: MenuController,public conf: Config, public toastCtrl: ToastController,public http : Http) {
  	  this.authType = 'login';
	  this.menuCtrl.swipeEnable(false);
  	  this.local = new Storage(LocalStorage);
  }

  ngOnInit() {
    this.loginForm = new FormGroup({
        username: new FormControl('', [<any>Validators.required, <any>Validators.minLength(5)]),
        password: new FormControl('', [<any>Validators.required, <any>Validators.minLength(5)]),
    });

    this.inputForm = new FormGroup({
        name: new FormControl('', [<any>Validators.required]),
        birthplace: new FormControl('', [<any>Validators.required]),
        birthdate: new FormControl('', [<any>Validators.required]),
        address: new FormControl('', [<any>Validators.required]),
        username: new FormControl('', [<any>Validators.required, <any>Validators.minLength(5)]),
        password: new FormControl('', [<any>Validators.required, <any>Validators.minLength(5)]),
    });
  }

  login(model,isValid){
  	this.submitted = true; // set form submit to true
    console.log(model);
  	if(!isValid){
  		this.presentToast("Please complete the form correctly");
  	}else{
  		let link = this.conf.loginUrlDestination;

	    let data = JSON.stringify(model);

	    this.http.post(link, data)
	      .subscribe(data => {
	      	var d = data.json();
	        this.presentToast(d.message);
	        this.local.set("isLogin",true);
	        this.local.set("id",d.data.id);
          this.local.set("username",d.data.username);
	        this.local.set("email",d.data.email);
	        this.local.set("token",d.data.token);
	        this.navCtrl.setRoot(JobList);
	    },error => {
	        var e = error.json();
	        this.presentToast(e.message);
	    });
  	}
  }

  goToSlide(page){
  	this.authType = page;
  }

  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }; 

}
