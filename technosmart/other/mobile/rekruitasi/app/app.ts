import { Component, ViewChild } from '@angular/core';
import { ionicBootstrap, Platform, Nav, Storage, LocalStorage } from 'ionic-angular';
import { StatusBar } from 'ionic-native';

import { JobList } from './pages/job-list/job-list';
import { Page2 } from './pages/page2/page2';
import { Login } from './pages/login/login';
import { Logout } from './pages/logout/logout';

@Component({
  templateUrl: 'build/app.html'
})
class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = Login;

  pages: Array<{title: string, component: any}>;

  pages2 = [
    [
      { title: 'Page uno', component: JobList },
      { title: 'Page dos', component: Page2 }
    ],
    [
      { title: 'Logout', component: Logout},
      { title: 'Logout', component: Logout}
    ]
  ];
  
  public local = new Storage(LocalStorage);
  public username;
  public email;
  
  constructor(public platform: Platform) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Job List', component: JobList },
      { title: 'Page dos', component: Page2 },
      { title: 'Logout', component: Logout},
    ];


  }

  ngAfterViewInit() {
    this.local.get("isLogin").then((value) => {
      if(value == "true"){
        this.nav.setRoot(JobList);
      }
    });
    this.local.get("username").then((value) => {
      this.username = value;
    });
    this.local.get("email").then((value) => {
      this.email = value;
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}

ionicBootstrap(MyApp);
