<?php
namespace technosmart\yii\base;

use Yii;
use yii\base\Component;

class Library extends Component
{
    public function xmlEncode($data)
    {
        $xml = '';

        foreach ($data as $key => $value) {
            // if array is sequential
            if (is_array($value) && array_keys($value)===range(0, count($value)-1)) {
                foreach ($value as $val)
                    $xml .= '<$key>'.(is_array($val) ? $this->xmlEncode($val) : $val).'</$key>';
            }
            else $xml .= '<$key>'.(is_array($value) ? $this->xmlEncode($value) : $value).'</$key>';
        }

        return $xml;
    }
}
