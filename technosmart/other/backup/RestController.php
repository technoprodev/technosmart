<?php
namespace technosmart\controllers;

use Yii;
use technosmart\yii\web\Controller;
use yii\helpers\Json;
use technosmart\models\User;
use yii\filters\auth\QueryParamAuth;

class RestController extends Controller
{

    public function behaviors()
    {
        return [
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to
                    'Access-Control-Allow-Origin' => ['*'],
                    'Access-Control-Request-Method' => ['POST', 'GET' , 'OPTIONS'],
                    // Allow only POST and PUT methods
                    'Access-Control-Request-Headers' => ['X-Wsse'],
                    // Allow only headers 'X-Wsse'
                    'Access-Control-Allow-Credentials' => true,
                    // Allow OPTIONS caching
                    'Access-Control-Max-Age' => 86400,
                    // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
                ],

            ],
            'authenticator' => [
                'class' => QueryParamAuth::className(),
            ]
        ];
    }

    public function beforeAction($action)
    {            
        $this->enableCsrfValidation = false;
        header("Access-Control-Allow-Origin: *");
        header("Access-Control-Allow-Credentials: true");
        header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
        header("Access-Control-Allow-Headers: Authorization, X-Requested-With");
        return parent::beforeAction($action);
    }

    public function actionSend(){

        //Checking http request we are using post here 
            
            //Getting api key 
            $api_key = "AIzaSyD9KZEMiOusMWaDXorZlDPXZ4L6EL0VoCc";   
            
            //Getting registration token we have to make it as array 
            
            //Getting the message 
            
            //Creating a message array 
            $msg = array
            (
                'message'   => "nih pesan",
                'title'     => 'Message from Simplified Coding',
                'subtitle'  => 'Android Push Notification using GCM Demo',
                'tickerText'    => 'Ticker text here...Ticker text here...Ticker text here',
                'vibrate'   => 1,
                'sound'     => 1,
                'largeIcon' => 'large_icon',
                'smallIcon' => 'small_icon'
            );
            
            //Creating a new array fileds and adding the msg array and registration token array here 
            $fields = array
            (
                'registration_ids'  => ['dsimbk112IE:APA91bF_qCKzgTYyqw4-cSghRTgncBrZUTkAwc-ldTryagT9UcBJN8Eoqhlipe3CwEfVTnJAtHR-UaGQX4uBVlrnM278BsOSJ5EKztgwO6YsJE25uwxOHMd8CkKqR7x1MIsz3bQYlwex'],
                // 'notification' => ["body" =>"sIni pesan","title"=>"ini judul","vibrate"=>0],
                //data saja supaya trigge nya terpanggil
                'data' => ["message"=>"aaaaa","title"=>"Data baru yeay"],
            );
            
            //Adding the api key in one more array header 
            $headers = array
            (
                'Authorization: key=' . $api_key,
                'Content-Type: application/json'
            ); 
            
            //Using curl to perform http request 
            $ch = curl_init();
            curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
            curl_setopt( $ch,CURLOPT_POST, true );
            curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
            
            //Getting the result 
            $result = curl_exec($ch );
            curl_close( $ch );
            
            //Decoding json from result 
            $res = json_decode($result);
            echo $result;
            exit();

            
            //Getting value from success 
            $flag = $res->success;
            
            //if success is 1 means message is sent 
            if($flag == 1){
                //Redirecting back to our form with a request success 
                header('Location: index.php?success');
            }else{
                //Redirecting back to our form with a request failure 
                header('Location: index.php?failure');
            }
    }

    public function actionIndex(){

        //http://stackoverflow.com/questions/15485354/angular-http-post-to-php-and-undefined
        $postdata = file_get_contents("php://input");
        if (isset($postdata)) {
            $request = json_decode($postdata);
            $ret = $request;

            if ($ret) {
                echo json_encode($ret);
            }
            else {
                echo "Empty username parameter!";
            }
        }
        else {
            echo "Not called properly with username parameter!";
        }
    }

    public function actionLogin(){
        $postdata = file_get_contents("php://input");
        if(isset($postdata)){
            $req = json_decode($postdata);
            $username = $req->username;
            $password = $req->password;
            $user = User::findByUsername($username);
            if(!empty($user)){
              if($user->validatePassword($password)){
                Yii::$app->response->statusCode = 200;
                return Json::encode([ 
                                  'status' => 'success',
                                  'message' => 'Login successful!',
                                  'data' => [
                                      'id' => $user->id,
                                      'username' => $user->username,
                                      'email' => $user->email,
                                      // token diambil dari field auth_key
                                      'token' => $user->auth_key,
                                  ]
                                ]);
              }else{
                Yii::$app->response->statusCode = 422;
                return Json::encode([
                                  'status' => 'error',
                                  'message' => 'Wrong password!',
                                  'data' => '',
                                ]);
              }
            }else{
                Yii::$app->response->statusCode = 422;
                return Json::encode([
                                    'status' => 'error',
                                    'message' => 'Username not found! Please Sign Up first!',
                                    'data' => '',
                                ]);
            }
        }else{
            Yii::$app->response->statusCode = 422;
            return Json::encode([
                            "status"    => "error",
                            "message"   => "No data passed!",
                            "data"      => ""
                        ]);
        }
    }

}
