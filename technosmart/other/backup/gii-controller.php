public function actionIndex(<?= $actionParamsNull ?>)
    {
        // only nulled all params or valued all params allowed
        $nulledParams = $valuedParams = false;
        foreach (func_get_args() as $key => $value) {
            if (is_null($value))
                $nulledParams = true;
            else
                $valuedParams = true;
        }
        if ($nulledParams && $valuedParams)
            throw new \yii\web\HttpException(404, "Page not found.");

        // view all data
        if ($nulledParams) {
            return $this->render('list', [
                'title' => 'List of <?= $titlePlural ?>',
            ]);
        }
        // view single data
        else {
            $model['<?= $modelId ?>'] = $this->findModel(<?= $actionParams ?>);<?php  // $model['child'] = $model['child']->child; ?>

            return $this->render('one', [
                'model' => $model,
                'title' => 'Detail of <?= $title ?> ' . $model['<?= $modelId ?>']->id,
            ]);
        }
    }

    /**
     * If param(s) is null, creates new data(s) from model(s).
     * If all param(s) is not null, updates existing data(s) from model(s).
     * If submission is successful, the browser will be redirected to the 'view' page.
     * <?= implode("\n     * ", $actionParamComments) . "\n" ?>
     * @return mixed
     */
    public function actionForm(<?= $actionParamsNull ?>)
    {
        $render = true;

        // only nulled all params or valued all params allowed
        $nulledParams = $valuedParams = false;
        foreach (func_get_args() as $key => $value) {
            if (is_null($value))
                $nulledParams = true;
            else
                $valuedParams = true;
        }
        if ($nulledParams && $valuedParams)
            throw new \yii\web\HttpException(404, "Page not found.");

        // init all models
        $model['<?= $modelId ?>'] = $nulledParams ? new <?= $modelClass ?>() : $this->findModel(<?= $actionParams ?>);<?php // $model['child'] = $model['parent']->child; ?>


        // init value if new request
        if (Yii::$app->request->isGet && $model['<?= $modelId ?>']->isNewRecord) {<?php // $model[parent]->attribute = $this->sequence('table-column'); ?>

        }
        // save all models & serve ajax validation
        else if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['<?= $modelId ?>']->load($post);<?php // $model['child']->load($post); ?>


            if (Yii::$app->request->isAjax && isset($post['ajax'])) {
                $result = array_merge(
                    ActiveForm::validate($model['<?= $modelId ?>'])<?php // , ActiveForm::validate($model['child']) ?>

                );
                return $this->json($result);
            }

            // models modification goes here<?php // $model['parent']->attribute = $model['parent']->attribute . ' asdf'; ?>


            if ($model['<?= $modelId ?>']->validate()<?php /* && $model['child']->validate() */ ?>) {
                if ($model['<?= $modelId ?>']->save(false)) {
                    $render = false;<?php // $model['child']->id_parent = $model['child']->id;if ($model['child']->save(false)) {$render = false;} ?>

                }
            }
        }

        // if new request / if saving all models fails
        if ($render)
            return $this->render('form', [
                'model' => $model,
                'title' => '<?= $modelClass ?>',
            ]);
        // if saving all models success
        else
            return $this->redirect(['index', 'id' => $model['<?= $modelId ?>']->id]);
    }