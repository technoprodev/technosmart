<?php
namespace technosmart\models;

use Yii;

/**
 * This is the model class for table "menu".
 *
 * @property string $code
 * @property integer $id
 * @property integer $parent
 * @property integer $order
 * @property integer $enable
 * @property string $title
 * @property string $icon
 * @property string $url
 * @property string $url_controller
 * @property string $url_action
 * @property string $param_key_1
 * @property string $param_value_1
 * @property string $param_key_2
 * @property string $param_value_2
 * @property string $param_key_3
 * @property string $param_value_3
 */
class Menu extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'menu';
    }

    public function rules()
    {
        return [
            //code
            [['code'], 'string', 'max' => 32],

            //id

            //parent
            [['parent'], 'integer'],

            //order
            [['order'], 'integer'],
            [['order'], 'required'],

            //enable
            [['enable'], 'integer'],

            //title
            [['title'], 'required'],
            [['title'], 'string', 'max' => 64],

            //icon
            [['icon'], 'string', 'max' => 64],

            //url
            [['url'], 'string', 'max' => 32],

            //url_controller
            [['url_controller'], 'string', 'max' => 32],

            //url_action
            [['url_action'], 'string', 'max' => 32],

            //param_key_1
            [['param_key_1'], 'string', 'max' => 32],

            //param_value_1
            [['param_value_1'], 'string', 'max' => 32],

            //param_key_2
            [['param_key_2'], 'string', 'max' => 32],

            //param_value_2
            [['param_value_2'], 'string', 'max' => 32],

            //param_key_3
            [['param_key_3'], 'string', 'max' => 32],

            //param_value_3
            [['param_value_3'], 'string', 'max' => 32],
        ];
    }

    public function attributeLabels()
    {
        return [
            'code' => 'Code',
            'id' => 'ID',
            'parent' => 'Parent',
            'order' => 'Order',
            'enable' => 'Enable',
            'title' => 'Title',
            'icon' => 'Icon',
            'url' => 'Url',
            'url_controller' => 'Url Controller',
            'url_action' => 'Url Action',
            'param_key_1' => 'Param Key 1',
            'param_value_1' => 'Param Value 1',
            'param_key_2' => 'Param Key 2',
            'param_value_2' => 'Param Value 2',
            'param_key_3' => 'Param Key 3',
            'param_value_3' => 'Param Value 3',
        ];
    }

    // only for MenuWidget::widget();
    public static function menuTree($list, $parent = null, $menuFancy = false)
    {
        $tree = [];

        foreach ($list as $menu) {
            $item = [];
            if ($menu['parent'] == $parent) {
                if ($menu['url_controller']) {
                    if (!$menu['url_action'])
                        $menu['url_action'] = 'index';

                    // ddx(Yii::$app->createControllerByID('xswzaq')->canAccess('index'));

                    try {
                        $item['visible'] = $menu['enable'] && Yii::$app->createControllerByID($menu['url_controller'])->canAccess($menu['url_action']);
                    } catch (\Exception $e) {
                        $item['visible'] = false;
                    } catch (\Throwable $e) {
                        $item['visible'] = false;
                    }

                    $item['url'][] = $menu['url_controller'] . '/' . $menu['url_action'];
                    if (isset($menu['param_key_1']))
                        $item['url'][$menu['param_key_1']] = $menu['param_value_1'];
                    if (isset($menu['param_key_2']))
                        $item['url'][$menu['param_key_2']] = $menu['param_value_2'];
                    if (isset($menu['param_key_3']))
                        $item['url'][$menu['param_key_3']] = $menu['param_value_3'];
                }
                else if ($menu['url']) {
                    $item['url'] = $menu['url'];
                }

                if ($menuFancy && !is_null($menu['parent']))
                    $item['label'] = '<i class="menu-icon"></i>' . $menu['title'];
                else
                    $item['label'] = $menu['title'];

                // $item['id'] = $menu['id'];
                $item['order'] = $menu['order'];

                $child = self::menuTree($list, $menu['id'], $menuFancy);
                if (!is_null($child))
                    $item['items'] = $child;

                $tree[] = $item;
            }
        }

        if ($tree) {
            usort($tree, function($a, $b) {
                return $a['order'] - $b['order'];
            });
        }

        return $tree;
    }

    public static function menuVisible($menuUrlController, $menuUrlAction)
    {
        try {
            return Yii::$app->createControllerByID($menuUrlController)->canAccess($menuUrlAction);
        } catch (\Throwable $e) {
            return false;
        }
    }

    public static function menuTreeSortOnly($list, $parent = null)
    {
        $tree = [];

        foreach ($list as $menu) {
            $item = [];
            if ($menu['parent'] == $parent) {
                $item = $menu;

                $child = self::menuTreeSortOnly($list, $menu['id']);
                if (!is_null($child))
                    $item['items'] = $child;

                $tree[] = $item;
            }
        }

        if ($tree) {
            usort($tree, function($a, $b) {
                return $a['order'] - $b['order'];
            });
        }

        return $tree;
    }

    public static function move($id, $order, $idSourceParent, $idDestinationParent, $orderDestinationPrevSibling)
    {
        $params = [
            ':id' => (int) $id, 
            ':order' => (int) $order, 
            ':idSourceParent' => $idSourceParent == 'null' ? null : (int) $idSourceParent, 
            ':idDestinationParent' => $idDestinationParent == 'null' ? null : (int) $idDestinationParent, 
            ':orderDestinationPrevSibling' => (int) $orderDestinationPrevSibling
        ];

        $transaction = self::getDb()->beginTransaction();

        try {
            if (is_null($params[':idSourceParent'])) {
                $d = null;
                self::getDb()
                    ->createCommand("update `menu` set `order` = `order` - 1 where parent is null and `order` > :order;")
                    ->bindValue(':order', $order)
                    ->execute();
            } else {
                self::getDb()
                    ->createCommand("update `menu` set `order` = `order` - 1 where parent = :idSourceParent and `order` > :order;")
                    ->bindValue(':idSourceParent', $idSourceParent)
                    ->bindValue(':order', $order)
                    ->execute();
            }

            if (is_null($params[':idDestinationParent'])) {
                self::getDb()
                    ->createCommand("update `menu` set `order` = `order` + 1 where parent is null and `order` > :orderDestinationPrevSibling and id <> :id;")
                    ->bindValue(':orderDestinationPrevSibling', $orderDestinationPrevSibling)
                    ->bindValue(':id', $id)
                    ->execute();

                self::getDb()
                    ->createCommand("update `menu` set parent = null, `order` = :orderDestinationPrevSibling + 1 where id = :id;")
                    ->bindValue(':orderDestinationPrevSibling', $orderDestinationPrevSibling)
                    ->bindValue(':id', $id)
                    ->execute();
            } else {
                 self::getDb()
                    ->createCommand("update `menu` set `order` = `order` + 1 where parent = :idDestinationParent and `order` > :orderDestinationPrevSibling and id <> :id;")
                    ->bindValue(':idDestinationParent', $idDestinationParent)
                    ->bindValue(':orderDestinationPrevSibling', $orderDestinationPrevSibling)
                    ->bindValue(':id', $id)
                    ->execute();

                self::getDb()
                    ->createCommand("update `menu` set parent = :idDestinationParent, `order` = :orderDestinationPrevSibling + 1 where id = :id;")
                    ->bindValue(':idDestinationParent', $idDestinationParent)
                    ->bindValue(':orderDestinationPrevSibling', $orderDestinationPrevSibling)
                    ->bindValue(':id', $id)
                    ->execute();
            }
            
            $transaction->commit();
            Yii::$app->cache->flush();
            return true;
        } catch (\Exception $e) {
            $transaction->rollBack();
            return false;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            return false;
        }
    }
}
