<?php
namespace technosmart\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $name
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends \technosmart\yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    const STATUS_DELETED = '-1';
    const STATUS_DISABLE = '0';
    const STATUS_ACTIVE = '1';

    public $login;
    public $password;
    public $repassword;
    public $rememberMe = true;

    public static function tableName()
    {
        return 'user';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression("now()"),
            ],
        ];
    }

    public function rules()
    {
        return [
            //id

            //username
            [['username'], 'trim', 'when' => function($model) {
                return $model->username != NULL;
            }],
            // [['username'], 'required'],
            [['username'], 'unique', 'message' => 'This username has already been taken.'],
            [['username'], 'string', 'min' => 2, 'max' => 16],
            [['username'], 'match', 'pattern' => '/^(?=.*[a-z])([a-z0-9._-]+)$/i', 'message' => 'Username at least contain 1 word. And only number, letter, dot, dashed and underscore allowed.'],

            //name
            [['name'], 'trim', 'when' => function($model) {
                return $model->name != NULL;
            }],
            // [['name'], 'required'],
            [['name'], 'string', 'max' => 64],

            //email
            [['email'], 'trim', 'when' => function($model) {
                return $model->email != NULL;
            }],
            // [['email'], 'required'],
            [['email'], 'unique', 'message' => 'This email address has already been taken.'],
            [['email'], 'string', 'max' => 64],
            [['email'], 'email'],

            //auth_key
            [['auth_key'], 'required'],
            [['auth_key'], 'string', 'max' => 32],

            //password_hash
            [['password_hash'], 'required'],
            [['password_hash'], 'string', 'max' => 255],

            //password_reset_token
            [['password_reset_token'], 'unique'],
            [['password_reset_token'], 'string', 'max' => 255],

            // status
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DISABLE, self::STATUS_DELETED]],

            //created_at
            [['created_at'], 'safe'],

            //updated_at
            [['updated_at'], 'safe'],

            //login
            [['login'], 'required', 'on' => 'login'],

            //password
            ['password', 'required', 'on' => 'pass'],
            ['password', 'string', 'min' => 6, 'on' => 'pass'],

            ['password', 'required', 'on' => 'repass'],
            ['password', 'string', 'min' => 6, 'on' => 'repass'],

            ['password', 'required', 'on' => 'login'],
            ['password', 'validatePass', 'on' => 'login'],

            //repassword
            ['repassword', 'required', 'on' => 'repass'],
            ['repassword', 'string', 'min' => 6, 'on' => 'repass'],
            ['repassword', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match", 'on' => 'repass'],

            //remember_me
            ['rememberMe', 'boolean', 'on' => 'login'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'name' => 'Name',
            'email' => 'Email',
            'login' => 'Username or Email',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',

            'password' => 'Password',
            'repassword' => 'Repeat Password',
        ];
    }

    public function validatePass($attribute, $params)
    {
        if (!$this->id) {
            $this->addError('login', 'Email or Username is not exists.');
        } else if (!Yii::$app->security->validatePassword($this->password, $this->password_hash)) {
            $this->addError($attribute, 'Incorrect password.');
        }
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Login secara API based
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['auth_key' => $token, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findByLogin($login)
    {
        return static::find()
            ->where('username = :login or email = :login', [':login' => $login])
            ->andWhere(['status' => self::STATUS_ACTIVE])
            ->one();
    }

    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * Membandingkan Authkey existing dengan Authkey yang ada di browser
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function getStatusName()
    {
        switch ($this->status) {
            case self::STATUS_ACTIVE:
                return 'Active';
                break;
            case self::STATUS_INACTIVE:
                return 'Inactive';
                break;
            case self::STATUS_DELETED:
                return 'Deleted';
                break;
            default:
                return null;
                break;
        }
    }

    public function getStatusLabels()
    {
        return [
            self::STATUS_ACTIVE => 'Active',
            self::STATUS_INACTIVE => 'Inactive',
            self::STATUS_DELETED => 'Deleted',
        ];
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) return false;

        $this->setPassword($this->password);
        $this->generateAuthKey();

        return true;
    }

    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this, $this->rememberMe ? 3600 * 24 * 30 : 0);
        } else {
            return false;
        }
    }
}
