/*========================================================================*\
  Produksi
\*========================================================================*/
	Done is better than perfect
	Menganggap tahu segalanya, analisis terbaik adalah berbicara dengan data dan pengalaman pribadi
	Orang yand diapresiasi akan melakukan lebih dari yang diharapkan
	Mengontrol usaha
		Pengawasan berkala untuk mengetahui perkembangan bisnis secara kontinu
		Pencatatan transaksi disertai bukti
		Kehadiran bos secara langsung
		Setor pendapatan ke bank untuk menghindari fraud terhadap duit tunai
		Gunakan sistem untuk meningkatkan produktifitas
	Dari sudut pandang gw, ada satu pola kenapa orang bisa ahli di suatu bidang tertentu. Pola simpelnya begini, orang itu melakukan sesuatu, lalu orang sekitar memberikan dampak positif terhadap sesuatu itu sehingga dirinya termotivasi untuk ingin mengulang-ulang kebiasaan itu, sehingga jadilah dia orang yang ahli. Liat aja itu para YouTuber.

/*========================================================================*\
  Marketing
\*========================================================================*/
	Menyadarkan client betapa pentingnya efek dari penerapan solusi
	Beri manfaat terlebih dahulu ke pelanggan.
	Customer tidak peduli dengan betapa hebatnya produk anda, tapi manfaat yang mereka dapatkan.
	Pahami kebutuhan pembeli : Dream, Need & Pain
		Pria
			Butuh diterima, pengakuan, kekaguman, kepercayaan, motivasi, penghargaan
		Wanita
			Butuh pengertian, perhatian, kesetiaan, penghormatan, pembenaran, jaminan
	People buy value
	Jelaskan manfaat produk Anda (dampak positif), dibandingkan fitur-fiturnya.
		Pada hakikatnya, orang lain TIDAK PERNAH membeli produk Anda. Mereka membeli ‘sesuatu’ di balik produk tersebut. Orang tidak pernah membeli shampoo, mereka membeli khasiat dari shampoo
	Social proof strategy agar pembeli percaya dengan kita
		jumlah pelanggan dan siapa pelanggan sebelumnya
		testimoni
		rating dan review
		media yang meliput
	Kenapa orang harus membeli produk kamu sekarang juga
	Penawaran
		GRATIS, DISKON, BONUS, Hadiah, Garansi, Jaminan, Voucher, Paket, Cash Back, Limited Offer, Poin, Data, Testimoni, Display, Sample, Trial, Demo
	Mending banyak paragraf sedikit kalimat, daripada sedikit paragraf banyak kalimats
	Kalau sudah terkenal, tulislah buku
	Gunakan kalimat pengandaian
	The goal of the video is to answer all of your potential customers’ questions and concerns. If you can do that in a short video, you’ll see an increase in your conversions. If you can’t, you won’t see your sales increase.
    What else would you like to see on this page?
    	Apa yang kamu tawarkan ? Apa produknya simple dan memenuhi kebutuhan standar saya ?
    What’s the number 1 reason that is stopping you from buying?
    What’s your biggest concern about this product or service?
    Is there anything that is confusing on this page?
    What can we help you solve?


/*========================================================================*\
  Sales
\*========================================================================*/
	8x edukasi, 2x promosi
	Buat mereka bercerita tentang permasalah mereka
	Jangan terlihat seper jualan
	Anggap sebagai teman, bukan calon pelanggan. Karena orang lebih tertarik membeli produk yang direkomendasikan oleh teman/sodaranya.
	Kenali pembeli, Dekatin pembeli, bahkan setelah penjualan
	Basa basi
	Tindak lanjut ketika negosiasi
	Berapapun murahnya, jika selisih penurunah harga tidak signifikan, maka konsumen tidak akan puas
	Jangan Mendebat Pelanggan, meski kita benar dan kita menang, pelanggan akan lari
	BAHAGIA = EKSPEKTASI + 1
	SESUATU YANG DIBANDINGKAN, AKAN MUDAH UNTUK DIPILIH
	Buat mereka harap-harap cemas dengan ketersediaan solusi

/*========================================================================*\
  Copywriting
\*========================================================================*/
	Target Market : 10 x lebih penting dari copywriting
		Perusahaan yang siap berinvestasi demi meningkatkan produktifitas bisnisnya

	Komponen Copywritin
		Headline
		Penawaran
		Alasan
		Bonus
		Testimoni
		Garansi
		Call to Action
		N.B

	Kata perangsang dalam headline
		Anda
		Cepat
		Praktis
		Lebih
		Rahasia
		Terbatas
		Sekarang
		Penting
		Buruan
		Menarik
		Hanya
		Mudah
		Akhirnya
		Baru
		Pengumuman
		Perhatian
		Perkenalkan
		Dicari
		Spesial!
		Segera
		Cara

	Frasa perangsang Headline
		Inilah Cara…
		Ini Alasan Kenapa…
		Bagaimana Caranya…
		Bagaimana Anda Bisa…
		Kini…
		Jangan… sebelum… ini
		Siapa lagi…
		7 Alasan Kenapa Anda…

	Penawaran
		Apa frustasi terbesar mereka
		Apa impian terbesar mereka

	Frasa perangsang Alasan
		7 Alasan Kenapa Anda HARUS beli …
		Mengapa Produk ini WAJIB Anda miliki…
		Kenapa ini PENTING untuk Anda coba…
		Ini Alasan Kenapa Anda HARUS Bergabung
		Menjadi…
		Selain Alasan ini, Alasan Apa Lagi yang Membuat
		Anda Tidak MEMBELI Produk ini…

	Bonus
		Relevan
		Berkualitas
		Ada Harganya
		Cantumkan Benefitnya

	Tips Testimoni
		Semakin spesifik, semakin orang akan merasa
		bahwa ‘INI BENAR’
		Tunjukkan dalam Bentuk Angka
		Jangan gunakan angka genap, 5, 10 atau
		kelipatannya
		Jangan semuanya ditunjukkan
		Tunjukkan Fotonya

	3 Fungsi Garansi
		Mengunci risiko mereka
		Mengatasi rasa frustasi mereka
		Alasan terkuat mereka bertransaksi dengan Anda

	Tips Memberikan GARANSI
		Hindari kata-kata Garansi yang sering digunakan oleh banyak orang
		Garansi harus menyentuh rasa frustasi terbesar yang dialami mereka
		Beriikan Garansi yang sekiranya tidak memberatkan Anda

	Contoh Kalimat CALL TO ACTION
		Apa lagi yang Anda tunggu? Telepon sekarang juga!
		Dapatkan katalog GRATIS kami. Hubungi …
		Bergabunglah dengan seribu konsumen yang puas lainnya hari ini juga!
		Untuk informasi lebih lanjut, hubungi….
		Action sekarang juga!
		Putuskan sendiri!
		Lakukan hari ini
		Jangan ragu-ragu!
		Berminat?
		Ini adalah keputusan yang menguntungkan untuk diambil
		Sekarang adalah saat yang terbaik!
		Daripada hanya membacanya, mengapa Anda tidak……
		Buktikan sendiri!
		Jadi, tunggu apa lagi?
		Waktu hampir habis!
		Mengapa tunggu sampai besok?
		Anda harus mengambil keputusan penting
		Anda sudah menunggu cukup lama

/*========================================================================*\
  Closing
\*========================================================================*/

	1. Mengapa Orang lain HARUS BELI produk Anda?
		Lebih Mudah… ?
		Lebih Murah… ?
		Lebih Eksklusif… ?
		Lebih VariaIf… ?
		Lebih Cepat… ?
		Lebih Enak… ?
		Lebih Tahan Lama… ?
		Lebih … ?
		(Silakan Pikirkan!)

	2. Kenapa Mereka HARUS PERCAYA kepada Anda?
		Sudah terbukI… ?
		TesImoni… ?
		Endorsement… ?
		Bukti… ?
		TerserIfikasi… ?
		Kedekatan… ?
		Direkomendasikan… ?
		(Silakan Pikirkan!)

	3. Apa Alasan Mereka HARUS BELI sekarang?
		Eksklusif… ?
		Besok harga naik… ?
		Promo hanya berlaku hari ini… ?
		Stok terbatas… ?
		Besok produk Idak dijamin ada… ?
		Diskon khusus hari ini saja… ?
		Bonus untuk yang beli hari ini… ?
		(Silakan Pikirkan!)

	4. Kenapa Orang lain TIDAK BELI produk Anda?
		Sulit didapat… ?
		Harganya mahal… ?
		Kualitasnya jelek… ?
		Servicenya buruk… ?
		Rasanya nggak enak… ?
		Lama… ?
		Tidak tahu… ?
		(Silakan Pikirkan!)

	5. Bagaimana Caranya agar Mereka Bisa Langsung Merespon Penawaran Anda?
		DISKON… ?
		GRATIS… ?
		BONUS… ?
		HADIAH… ?
		(Silakan Pikirkan!)

	Sebelum kita lanjutkan, sadarkah Anda? Hampir 80% keberatan yang diajukan oleh calon customer Anda adalah SAMA. inilah keberatan mereka….
		Nggak butuh!
		Nggak tertarik!
		Nggak ada waktu!
		Nggak ada uang!
		Harganya mahal!
		Produknya jelek!
		Produk kompeItor Lebih Bagus
		Saya sudah punya Langganan
		Saya tanya istri/suami dulu
		Saya pikir‐pikir dulu
		dll

	Ciri-ciri closing
		ANTUSIAS
		BANYAK NANYA
		MENGHUBUNGI ANDA BERKALI­‐KALI

	Teknik Closing
		1. Buat Calon Customer berkata “YA” sebanyakbanyaknya
		2. Jangan Menghentikan Percakapan Berakhir di Anda
		3. Berikan 3 Keuntungan secara Ringkas dalam Satu Penawaran
		4. Mereka Harus Memutuskan Saat Itu Juga, Kalau Tidak, Dia Akan Menyesal
		5. Beri Kejutan yang Menyenangkan Customer Anda
		6. Beri 3 Penawaran dan Simpan yang Ingin Dijual Diurutan ke-2
		7. Tunjukkan Harga Asli dan Harga Penawaran Terbaru. Biarkan Dia Menghiting Sendiri
		8. Katakan pada Customer bahwa DIA HEBAT. Anda KAGUM dan TERKESAN dengannya
		9. Jika dibanding dengan pesaing, sadarkan bahwa mereka People buy value
		10. Pecah Biaya Yang Dikeluarkan Customer Agar TERLIHAT LEBIH KECIL
		11. Hadirkan RASA MALU Jika Tidak Membeli : entertain
		12. Tunjukan Anda Bisa MERASAKAN Apa Yang Mereka Rasakan
		13. Tidak Semua Orang Bisa Mendapatkan Produk Ini, TERBATAS !
		14. Arahkan Customer Secara Tidak Langsung. Biarkan Mereka MEMILIH SENDIRI
		15. Buat Orang TERTAWA Atau TERSENYUM Dengan Penawaran / Kata-Kata Anda
		16. Apa KERUGIAN MEREKA Jika Tidak Memilih Produk Ini
		17. Kasih TESTIMONI Atau PENGALAMAN ORANG LAIN

/*========================================================================*\
  Persiapan
\*========================================================================*/
	Mental yang perlu dibuang sebelum berbisnis
		Gengsi melakukan pekerjaan yang bukan kantoran
		Takut gagal
		Niat dan alasan yang tidak tepat memilih bisnis
		Tidak mau tangan kotor

/*========================================================================*\
  Other
\*========================================================================*/
	Hedonic treadmill
		nafsu membeli barang mewah yang terus meningkat sejalan dengan peningkatan income.
		Seperti berjalan di atas treadmill, kebahagiaan yang tidak maju-maju!

	Tinggi ada karena rendah, syukur & bahagia tidak perlu materi

	Yang dimiliki dalam hidup
		1. Ikhlas ketika kehilangan sesuatu / ketika sesuatu yang dimiliki rusak
		2. Kebutuhan : sandang, pangan, papan, kesehatan, pendidikan anak
		3. Ikhlas berbagi harta dan ilmu.
		4. Ikhlas & tanpa beban ketika menghabiskan uang.
		5. Tabungan & Investasi : hari tua