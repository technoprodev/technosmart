Yii::$app is an application instance, a globally accessible singleton, a service locator that provides components such as request, response, db, etc. to support specific functionality

controllers should NOT process the request data - this should be done in models;

models should NOT directly access request, session, or any other environmental data. These data should be injected by controllers into models; avoid having too many scenarios in a single model.

security, When creating views that generate HTML pages, it is important that you encode and/or filter the data coming from end users before presenting them. Otherwise, your application may be subject to cross-site scripting attacks.


    $this->member for non-static members
    Self::$member for static members

    $array = array_filter($array, function(&$val) {
        return $val != ''; //hapus elemen jika ''
    });

    $array = array_walk($array, function(&$val, $key) {
        $val = 'new val';
    });

    ////

    $emptyRemoved = remove_empty($array);

    function remove_empty($array) {
      return array_filter($array, '_remove_empty_internal');
    }

    function _remove_empty_internal($value) {
      return !empty($value) || $value === 0;
    }

    ////

    $array = [
        ['id' => '123', 'name' => 'aaa', 'class' => 'x'],
        ['id' => '124', 'name' => 'bbb', 'class' => 'x'],
        ['id' => '345', 'name' => 'ccc', 'class' => 'y'],
    );
    $result = ArrayHelper::map($array, 'id', 'name');
    /* the result is:
    [
        '123' => 'aaa',
        '124' => 'bbb',
        '345' => 'ccc',
    ]
    */
        
    $result = ArrayHelper::map($array, 'id', 'name', 'class');
    /* the result is:
    [
        'x' => [
            '123' => 'aaa',
            '124' => 'bbb',
        ],
        'y' => [
            '345' => 'ccc',
        ],
    ]
    */

    ////
    
    HtmlPurifier::process($post->text) for filtering harmful text, it is not fast, consider using caching