var vm = new Vue({
    el: '#app',
    data: {
        a: null,
        b: 'Bar'
    },
    computed: {
        computedA: function () {
            // `this` points to the vm instance
            return this.a.split('').reverse().join('')
        },
        fullName: {
            // getter
            get: function () {
                return this.a + ' ' + this.b
            },
            // setter
            set: function (newValue) {
                var names = newValue.split(' ')
                this.a = names[0]
                this.b = names[names.length - 1]
            }
        }
    },
    methods: {
        getA: function (arg1, arg2) {
            /*this.$http.get('https://yesno.wtf/api').then( function(response) {
                this.a = response.data.a
            }, function (error) {
                this.a = 'Error! Could not reach the API. ' + error
            })*/
        }
    },
    watch: {
        a: function(newVal, oldVal){
            this.a = !this.a;
        }
    },
});