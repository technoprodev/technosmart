-- baru ini
ALTER TABLE `user`
CHANGE `username` `username` varchar(16) COLLATE 'latin1_swedish_ci' NULL AFTER `id`,
CHANGE `name` `name` varchar(64) COLLATE 'latin1_swedish_ci' NULL AFTER `username`,
CHANGE `email` `email` varchar(64) COLLATE 'latin1_swedish_ci' NULL AFTER `name`;

-----------------

ALTER TABLE `post`
ENGINE='InnoDB';

ALTER TABLE `post`
ADD FOREIGN KEY (`created_by`) REFERENCES `user` (`id`);

ALTER TABLE `post`
ADD FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`);

ALTER TABLE `post`
ADD FOREIGN KEY (`id_post_category`) REFERENCES `post_category` (`id`);

ALTER TABLE `post`
ADD FOREIGN KEY (`id_post_type`) REFERENCES `post_type` (`id`);

ALTER TABLE `post`
CHANGE `id_post_category` `id_post_category` int(11) NULL AFTER `status_comment`,
CHANGE `id_post_type` `id_post_type` int(11) NULL AFTER `id_post_category`;

----pending dr ta nanda
ALTER TABLE `faq`
CHANGE `question` `question` text COLLATE 'latin1_swedish_ci' NOT NULL AFTER `id`,
CHANGE `answer` `answer` text COLLATE 'latin1_swedish_ci' NOT NULL AFTER `question`;

ALTER TABLE `post`
CHANGE `updated_by` `updated_by` int(11) NULL AFTER `created_by`; --di starter-technosmart udah ya