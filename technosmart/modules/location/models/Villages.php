<?php
namespace technosmart\modules\location\models;

use Yii;

/**
 * This is the model class for table "villages".
 *
 * @property string $id
 * @property string $district_id
 * @property string $name
 *
 * @property Districts $district
 */
class Villages extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'villages';
    }

    public function rules()
    {
        return [
            //id
            [['id'], 'required'],
            [['id'], 'string', 'max' => 10],

            //district_id
            [['district_id'], 'required'],
            [['district_id'], 'string', 'max' => 7],
            [['district_id'], 'exist', 'skipOnError' => true, 'targetClass' => Districts::className(), 'targetAttribute' => ['district_id' => 'id']],

            //name
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'district_id' => 'District ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistrict()
    {
        return $this->hasOne(Districts::className(), ['id' => 'district_id']);
    }
}
