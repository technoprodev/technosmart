<?php

use yii\helpers\Html;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

\technosmart\modules\location\assets_manager\villages\ListAsset::register($this);
?>

<table class="datatables display nowrap table table-striped table-hover table-condensed">
    <thead>
        <tr>
            <th class="text-dark f-default" style="border-bottom: 1px">Action</th>
            <th class="text-dark f-default" style="border-bottom: 1px">District ID</th>
            <th class="text-dark f-default" style="border-bottom: 1px">Name</th>
        </tr>
        <tr class="dt-search">
            <th class="padding-0"></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search district id" class="form-control border-none f-default padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search name" class="form-control border-none f-default padding-x-5"/></th>
        </tr>
    </thead>
</table>

<?php if ($this->context->can('create')): ?>
    <hr class="margin-y-15">
    <div>
        <?= Html::a('Create Data', ['create'], ['class' => 'btn btn-sm btn-default bg-azure rounded-xs border-azure']) ?>
    </div>
<?php endif; ?>