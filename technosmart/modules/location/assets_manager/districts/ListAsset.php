<?php
namespace technosmart\modules\location\assets_manager\districts;

use yii\web\AssetBundle;

class ListAsset extends AssetBundle
{
	public $sourcePath = '@technosmart/modules/location/assets/districts';

    public $css = [];

    public $js = [
        'list.js',
    ];

    public $depends = [
        'technosmart\assets_manager\DatatablesAsset',
    ];
}