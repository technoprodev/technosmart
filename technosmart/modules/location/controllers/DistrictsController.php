<?php
namespace technosmart\modules\location\controllers;

use Yii;
use technosmart\modules\location\models\Districts;
use technosmart\yii\web\Controller;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

/**
 * DistrictsController implements highly advanced CRUD actions for Districts model.
 */
class DistrictsController extends Controller
{
    /*public static $permissions = [
        ['view', 'View Districts'], ['create', 'Create Districts'], ['update', 'Update Districts'], ['delete', 'Delete Districts'],
    ];

    public function behaviors()
    {
        return [
            'access' => $this->access([
                [['index'], 'view'],
                [['index', 'create'], 'create'],
                [['index', 'update'], 'update'],
                [['index', 'delete'], 'delete', null, ['POST']],
            ]),
        ];
    }*/

    public function actionDatatables()
    {
        $db = Districts::getDb();
        $post = Yii::$app->request->post();

        // serve data for datatables
        if (isset($post['draw'])) {
            $query = new \yii\db\Query();
            $query
                ->select('count(*)')
                ->from('districts');
            $countWhere = count($query->where);

            $total = $query->scalar($db);
            $return['recordsTotal'] = $total;
            $return['recordsFiltered'] = $total;

            $allWhere = ['or'];
            $allSearch = $post['search']['value'];
            foreach ($post['columns'] as $key => $value) {
                if ($value['searchable'] == 'true') {
                    $column = $value['data'];
                    if (is_array($column)) {
                        if ( isset($column['filter']) )
                            $column = $column['filter'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['search']['regex'] == 'false') {
                        $query->andFilterWhere(['like', $column, $value['search']['value']]);
                    } else if ($value['search']['regex'] == 'true') {
                        $query->andFilterWhere(['regexp', $column, $value['search']['value']]);
                    }

                    if ($allSearch) {
                        if ($post['search']['regex'] == 'false') {
                            $allWhere[] = ['like', $column, $allSearch];
                        } else if ($post['search']['regex'] == 'true') {
                            $allWhere[] = ['regexp', $column, $allSearch];
                        }
                    }
                }
            }
            if (count($allWhere) > 1)
                $query->andFilterWhere($allWhere);
            if (count($query->where) > $countWhere)
                $return['recordsFiltered'] = $query->scalar($db);

            $query->select([
                'id',
                'regency_id',
                'name',
            ]);

            $order = [];
            if (isset($post['order'])) {
                foreach ($post['order'] as $key => $value) {
                    $column = $post['columns'][$value['column']]['data'];
                    if ($post['columns'][$value['column']]['orderable'] == 'false') {
                        continue;
                    }
                    if (is_array($column)) {
                        if ( isset($column['sort']) )
                            $column = $column['sort'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['dir'] == 'asc')
                        $order[$column] = SORT_ASC;
                    else if ($value['dir'] == 'desc')
                        $order[$column] = SORT_DESC;
                }
            }
            count($order) ? $query->orderBy($order) : 0;

            if (isset($post['length']))
                $query->limit(intval($post['length']));

            if (isset($post['start']))
                $query->offset(intval($post['start']));

            $return['draw'] = intval($post['draw']);
            $return['data'] = $query->all($db);
            return $this->json($return);
        }
    }

    /**
     * If param(s) is null, display all datas from models.
     * If all param(s) is not null, display a data from model.
     * @param string $id
     * @return mixed
     */
    public function actionIndex($id = null)
    {
        // view all data
        if (!$id) {
            return $this->render('list', [
                'title' => 'List of Districts',
            ]);
        }
        
        // view single data
        $model['districts'] = $this->findModel($id);
        return $this->render('one', [
            'model' => $model,
            'title' => 'Detail of Districts ' . $model['districts']->id,
        ]);
    }

    /**
     * Creates new data(s) from model(s).
     * If submission is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionCreate()
    {
        $render = false;

        $model['districts'] = isset($id) ? $this->findModel($id) : new Districts();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['districts']->load($post);

            $transaction['districts'] = Districts::getDb()->beginTransaction();

            try {
                if (!$model['districts']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved. Please try again.');
                }
                
                $transaction['districts']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['districts']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['districts']->rollBack();
            }
        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Add New Districts',
            ]);
        else
            return $this->redirect(['index', 'id' => $model['districts']->id]);
    }

    /**
     * Updates existing data(s) from model(s).
     * If submission is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id = null)
    {
        $render = false;

        $model['districts'] = isset($id) ? $this->findModel($id) : new Districts();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['districts']->load($post);

            $transaction['districts'] = Districts::getDb()->beginTransaction();

            try {
                if (!$model['districts']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved. Please try again.');
                }
                
                $transaction['districts']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['districts']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['districts']->rollBack();
            }
        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Update Districts ' . $model['districts']->id,
            ]);
        else
            return $this->redirect(['index', 'id' => $model['districts']->id]);
    }

    /**
     * Deletes an existing Districts model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Districts model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Districts the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Districts::findOne($id)) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }
}
