<?php

use yii\db\Migration;

class m171215_093440_location extends Migration
{
    public function safeUp()
    {
        $sql = file_get_contents('location.sql', true);
        $this->execute($sql);
    }

    public function safeDown()
    {
        echo "m171215_093440_location cannot be reverted.\n";

        return false;
    }
}
