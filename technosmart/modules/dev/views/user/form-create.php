<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

technosmart\assets_manager\Select2Asset::register($this);

$error = false;
$errorMessage = '';
if ($model['user']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['user'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-left-30 m-margin-left-0">
    <div class="box-8">
<?php endif; ?>
    
<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model['user'], 'name')->begin(); ?>
        <?= Html::activeLabel($model['user'], 'name', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['user'], 'name', ['class' => 'form-control', 'maxlength' => true]); ?>
        <?= Html::error($model['user'], 'name', ['class' => 'help-block']); ?>
    <?= $form->field($model['user'], 'name')->end(); ?>

    <?= $form->field($model['user'], 'username')->begin(); ?>
        <?= Html::activeLabel($model['user'], 'username', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['user'], 'username', ['class' => 'form-control', 'maxlength' => true]); ?>
        <?= Html::error($model['user'], 'username', ['class' => 'help-block']); ?>
    <?= $form->field($model['user'], 'username')->end(); ?>

    <?= $form->field($model['user'], 'email')->begin(); ?>
        <?= Html::activeLabel($model['user'], 'email', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['user'], 'email', ['class' => 'form-control', 'maxlength' => true]); ?>
        <?= Html::error($model['user'], 'email', ['class' => 'help-block']); ?>
    <?= $form->field($model['user'], 'email')->end(); ?>

    <?= $form->field($model['user'], 'phone')->begin(); ?>
        <?= Html::activeLabel($model['user'], 'phone', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['user'], 'phone', ['class' => 'form-control', 'maxlength' => true]); ?>
        <?= Html::error($model['user'], 'phone', ['class' => 'help-block']); ?>
    <?= $form->field($model['user'], 'phone')->end(); ?>

    <?= $form->field($model['user'], 'password')->begin(); ?>
        <?= Html::activeLabel($model['user'], 'password', ['class' => 'control-label']); ?>
        <?= Html::activePasswordInput($model['user'], 'password', ['class' => 'form-control', 'maxlength' => true]); ?>
        <?= Html::error($model['user'], 'password', ['class' => 'help-block']); ?>
    <?= $form->field($model['user'], 'password')->end(); ?>

    <?= $form->field($model['user'], 'password_repeat')->begin(); ?>
        <?= Html::activeLabel($model['user'], 'password_repeat', ['class' => 'control-label']); ?>
        <?= Html::activePasswordInput($model['user'], 'password_repeat', ['class' => 'form-control', 'maxlength' => true]); ?>
        <?= Html::error($model['user'], 'password_repeat', ['class' => 'help-block']); ?>
    <?= $form->field($model['user'], 'password_repeat')->end(); ?>

    <div class="form-group field-roles-assignments">
        <label class="control-label">Roles</label>
        <?= Html::checkboxList(
            'assignments[]',
            $model['assignments'],
            $roles
        ) ?>
    </div>

    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['user']->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>