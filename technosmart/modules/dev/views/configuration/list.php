<?php

use yii\helpers\Html;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

\technosmart\modules\dev\assets_manager\configuration\ListAsset::register($this);
?>

<table class="datatables display nowrap table table-striped table-hover table-condensed">
    <thead>
        <tr>
            <th class="text-dark f-default" style="border-bottom: 1px">Action</th>
            <th class="text-dark f-default" style="border-bottom: 1px">Name</th>
            <th class="text-dark f-default" style="border-bottom: 1px">Type</th>
            <th class="text-dark f-default" style="border-bottom: 1px">Value</th>
        </tr>
        <tr class="dt-search">
            <th class="padding-0"></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search name" class="form-control border-none f-default padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search type" class="form-control border-none f-default padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search value" class="form-control border-none f-default padding-x-5"/></th>
        </tr>
    </thead>
</table>