<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['configuration']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['configuration'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-left-30 m-margin-left-0">
    <div class="box-8">
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <div class="margin-top-20">
        <div class="box box-break-sm margin-bottom-10">
            <div class="box-1 padding-x-0 m-text-left"><?= $model['configuration']->attributeLabels()['name'] ?></div>
            <div class="box-11 m-padding-x-0 text-dark"><?= $model['configuration']->name ? $model['configuration']->name : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
        </div>
            
        <div class="box box-break-sm margin-bottom-10">
            <div class="box-1 padding-x-0 m-text-left"><?= $model['configuration']->attributeLabels()['type'] ?></div>
            <div class="box-11 m-padding-x-0 text-dark"><?= $model['configuration']->type ? $model['configuration']->type : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
        </div>
    </div>

    <?= $form->field($model['configuration'], 'value')->begin(); ?>
        <?= Html::activeLabel($model['configuration'], 'value', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['configuration'], 'value', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['configuration'], 'value', ['class' => 'help-block']); ?>
    <?= $form->field($model['configuration'], 'value')->end(); ?>


    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['configuration']->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>