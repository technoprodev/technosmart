<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['role']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['role'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-left-30 m-margin-left-0">
    <div class="box-8">
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model['role'], 'name')->begin(); ?>
        <?= Html::activeLabel($model['role'], 'name', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['role'], 'name', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['role'], 'name', ['class' => 'help-block']); ?>
    <?= $form->field($model['role'], 'name')->end(); ?>

    <?= $form->field($model['role'], 'type')->begin(); ?>
        <?= Html::activeLabel($model['role'], 'type', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['role'], 'type', ['class' => 'form-control']) ?>
        <?= Html::error($model['role'], 'type', ['class' => 'help-block']); ?>
    <?= $form->field($model['role'], 'type')->end(); ?>

    <?= $form->field($model['role'], 'description')->begin(); ?>
        <?= Html::activeLabel($model['role'], 'description', ['class' => 'control-label']); ?>
        <?= Html::activeTextArea($model['role'], 'description', ['class' => 'form-control', 'rows' => 6]) ?>
        <?= Html::error($model['role'], 'description', ['class' => 'help-block']); ?>
    <?= $form->field($model['role'], 'description')->end(); ?>

    <?= $form->field($model['role'], 'rule_name')->begin(); ?>
        <?= Html::activeLabel($model['role'], 'rule_name', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['role'], 'rule_name', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['role'], 'rule_name', ['class' => 'help-block']); ?>
    <?= $form->field($model['role'], 'rule_name')->end(); ?>

    <?= $form->field($model['role'], 'data')->begin(); ?>
        <?= Html::activeLabel($model['role'], 'data', ['class' => 'control-label']); ?>
        <?= Html::activeTextArea($model['role'], 'data', ['class' => 'form-control', 'rows' => 6]) ?>
        <?= Html::error($model['role'], 'data', ['class' => 'help-block']); ?>
    <?= $form->field($model['role'], 'data')->end(); ?>

    <?= $form->field($model['role'], 'created_at')->begin(); ?>
        <?= Html::activeLabel($model['role'], 'created_at', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['role'], 'created_at', ['class' => 'form-control']) ?>
        <?= Html::error($model['role'], 'created_at', ['class' => 'help-block']); ?>
    <?= $form->field($model['role'], 'created_at')->end(); ?>

    <?= $form->field($model['role'], 'updated_at')->begin(); ?>
        <?= Html::activeLabel($model['role'], 'updated_at', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['role'], 'updated_at', ['class' => 'form-control']) ?>
        <?= Html::error($model['role'], 'updated_at', ['class' => 'help-block']); ?>
    <?= $form->field($model['role'], 'updated_at')->end(); ?>


    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['role']->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>