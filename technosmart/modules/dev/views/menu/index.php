<?php

use yii\helpers\Html;
use technosmart\models\Menu;
use technosmart\yii\widgets\Menu as MenuWidget;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

\technosmart\modules\dev\assets_manager\menu\IndexAsset::register($this);

function generateMenuHtml($list) {
    $html = null;
    foreach ($list as $menu) {
        $addItem = Html::a('Add Item', ['create', 'parent' => $menu['id']], ['class' => 'margin-left-15 pull-right']);
        $edit = Html::a('Edit', ['update', 'id' => $menu['id']], ['class' => 'margin-left-15 pull-right']);
        $delete = Html::a('Delete', ['delete', 'id' => $menu['id']], ['class' => 'margin-left-15 pull-right', 'data-confirm' => 'Are you sure you want to delete this item ?', 'data-method' => 'post']);
        $code = "<span class='margin-x-15 pull-right'>code: $menu[code]</span>";
        $html .=
        "<li style='position: relative;' class='dd-item " . ($menu['enable'] ? null : "text-red") . "' id='$menu[id]' data-id='$menu[id]' data-order='$menu[order]' data-parent='$menu[parent]'>" .
            "<div class='dd-handle border-light text-azure bg-lightest hover-bg-light-azure padding-y-5 padding-x-10 margin-y-5 clearfix'>
                $menu[title]
            </div>
            <div style='position: absolute; right: 7px; top: 7px;'>$addItem $edit $delete $code</div>" .
            (!empty($menu['items']) ? generateMenuHtml($menu['items']) : null) .
        "</li>";
    }

    return "<ol class='dd-list'>$html</ol>";
}

$menuFromDatabase = technosmart\models\Menu::find()/*->where(['code' => 'sidebar'])*/->asArray()->all();
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm m-margin-left-0">
    <div class="box-9">
<?php endif; ?>

<div class="margin-y-15">
    <?= Html::a('Add New Menu', ['create'], ['class' => 'btn fs-12 border-azure text-azure hover-bg-azure']) ?>
</div>

<div class="dd" id="nestable">
    <?php if ($menuFromDatabase) : ?>
        <?= generateMenuHtml(Menu::menuTreeSortOnly($menuFromDatabase)); ?>
    <?php else : ?>
        <?= 'There is no any menu yet.' ?>
    <?php endif; ?>
</div>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>