<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['menu']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['menu'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-left-30 m-margin-left-0">
    <div class="box-8">
<?php endif; ?>

<?php $form = ActiveForm::begin(['options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <div class="box box-break-sm">
        <div class="box-6 padding-x-0">
            <?= $form->field($model['menu'], 'code', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
                <?= Html::activeLabel($model['menu'], 'code', ['class' => 'control-label fs-12']); ?>
                <?= Html::activeTextInput($model['menu'], 'code', ['class' => 'form-control', 'maxlength' => true]) ?>
                <?= Html::error($model['menu'], 'code', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
            <?= $form->field($model['menu'], 'code')->end(); ?>
        </div>
        <div class="box-6 padding-x-0">
            <?php if ($this->context->action->id == 'create') : ?>
            <?= $form->field($model['menu'], 'parent', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
                <?= Html::activeLabel($model['menu'], 'parent', ['class' => 'control-label fs-12']); ?>
                <?= Html::activeDropDownList($model['menu'], 'parent', ArrayHelper::map(\technosmart\models\Menu::find()->asArray()->all(), 'id', 'title', 'code'), ['prompt' => 'Choose one please', 'class' => 'form-control']); ?>
                <?= Html::error($model['menu'], 'parent', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
            <?= $form->field($model['menu'], 'parent')->end(); ?>
            <?php endif; ?>
        </div>
    </div>

    <div class="box box-break-sm">
        <div class="box-12 padding-x-0">
            <?= $form->field($model['menu'], 'title', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
                <?= Html::activeLabel($model['menu'], 'title', ['class' => 'control-label fs-12']); ?>
                <?= Html::activeTextInput($model['menu'], 'title', ['class' => 'form-control', 'maxlength' => true]) ?>
                <?= Html::error($model['menu'], 'title', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
            <?= $form->field($model['menu'], 'title')->end(); ?>
        </div>
    </div>

    <div class="box box-break-sm">
        <div class="box-6 padding-x-0">
            <?= $form->field($model['menu'], 'icon', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
                <?= Html::activeLabel($model['menu'], 'icon', ['class' => 'control-label fs-12']); ?>
                <?= Html::activeTextInput($model['menu'], 'icon', ['class' => 'form-control', 'maxlength' => true]) ?>
                <?= Html::error($model['menu'], 'icon', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
            <?= $form->field($model['menu'], 'icon')->end(); ?>
        </div>
        <div class="box-6 padding-x-0">
            <?= $form->field($model['menu'], 'url', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
                <?= Html::activeLabel($model['menu'], 'url', ['class' => 'control-label fs-12']); ?>
                <?= Html::activeTextInput($model['menu'], 'url', ['class' => 'form-control', 'maxlength' => true]) ?>
                <?= Html::error($model['menu'], 'url', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
            <?= $form->field($model['menu'], 'url')->end(); ?>
        </div>
    </div>

    <div class="box box-break-sm">
        <div class="box-6 padding-x-0">
            <?= $form->field($model['menu'], 'url_controller', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
                <?= Html::activeLabel($model['menu'], 'url_controller', ['class' => 'control-label fs-12']); ?>
                <?= Html::activeTextInput($model['menu'], 'url_controller', ['class' => 'form-control', 'maxlength' => true]) ?>
                <?= Html::error($model['menu'], 'url_controller', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
            <?= $form->field($model['menu'], 'url_controller')->end(); ?>
        </div>
        <div class="box-6 padding-x-0">
            <?= $form->field($model['menu'], 'url_action', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
                <?= Html::activeLabel($model['menu'], 'url_action', ['class' => 'control-label fs-12']); ?>
                <?= Html::activeTextInput($model['menu'], 'url_action', ['class' => 'form-control', 'maxlength' => true]) ?>
                <?= Html::error($model['menu'], 'url_action', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
            <?= $form->field($model['menu'], 'url_action')->end(); ?>
        </div>
    </div>

    <div class="box box-break-sm">
        <div class="box-6 padding-x-0">
            <?= $form->field($model['menu'], 'param_key_1', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
                <?= Html::activeLabel($model['menu'], 'param_key_1', ['class' => 'control-label fs-12']); ?>
                <?= Html::activeTextInput($model['menu'], 'param_key_1', ['class' => 'form-control', 'maxlength' => true]) ?>
                <?= Html::error($model['menu'], 'param_key_1', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
            <?= $form->field($model['menu'], 'param_key_1')->end(); ?>
        </div>
        <div class="box-6 padding-x-0">
            <?= $form->field($model['menu'], 'param_value_1', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
                <?= Html::activeLabel($model['menu'], 'param_value_1', ['class' => 'control-label fs-12']); ?>
                <?= Html::activeTextInput($model['menu'], 'param_value_1', ['class' => 'form-control', 'maxlength' => true]) ?>
                <?= Html::error($model['menu'], 'param_value_1', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
            <?= $form->field($model['menu'], 'param_value_1')->end(); ?>
        </div>
    </div>

    <div class="box box-break-sm">
        <div class="box-6 padding-x-0">
            <?= $form->field($model['menu'], 'param_key_2', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
                <?= Html::activeLabel($model['menu'], 'param_key_2', ['class' => 'control-label fs-12']); ?>
                <?= Html::activeTextInput($model['menu'], 'param_key_2', ['class' => 'form-control', 'maxlength' => true]) ?>
                <?= Html::error($model['menu'], 'param_key_2', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
            <?= $form->field($model['menu'], 'param_key_2')->end(); ?>
        </div>
        <div class="box-6 padding-x-0">
            <?= $form->field($model['menu'], 'param_value_2', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
                <?= Html::activeLabel($model['menu'], 'param_value_2', ['class' => 'control-label fs-12']); ?>
                <?= Html::activeTextInput($model['menu'], 'param_value_2', ['class' => 'form-control', 'maxlength' => true]) ?>
                <?= Html::error($model['menu'], 'param_value_2', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
            <?= $form->field($model['menu'], 'param_value_2')->end(); ?>
        </div>
    </div>

    <div class="box box-break-sm">
        <div class="box-6 padding-x-0">
            <?= $form->field($model['menu'], 'param_key_3', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
                <?= Html::activeLabel($model['menu'], 'param_key_3', ['class' => 'control-label fs-12']); ?>
                <?= Html::activeTextInput($model['menu'], 'param_key_3', ['class' => 'form-control', 'maxlength' => true]) ?>
                <?= Html::error($model['menu'], 'param_key_3', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
            <?= $form->field($model['menu'], 'param_key_3')->end(); ?>
        </div>
        <div class="box-6 padding-x-0">
            <?= $form->field($model['menu'], 'param_value_3', ['options' => ['class' => 'form-group margin-bottom-10 form-group-sm']])->begin(); ?>
                <?= Html::activeLabel($model['menu'], 'param_value_3', ['class' => 'control-label fs-12']); ?>
                <?= Html::activeTextInput($model['menu'], 'param_value_3', ['class' => 'form-control', 'maxlength' => true]) ?>
                <?= Html::error($model['menu'], 'param_value_3', ['class' => 'help-block fs-11 margin-bottom-0']); ?>
            <?= $form->field($model['menu'], 'param_value_3')->end(); ?>
        </div>
    </div>

    <hr>

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['menu']->isNewRecord ? 'Create' : 'Update', ['class' => 'btn fs-12 border-azure bg-azure text-white hover-bg-light-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn fs-12 border-azure bg-lightest text-azure hover-bg-azure']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'btn fs-12 border-azure bg-lightest text-azure hover-bg-azure pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>
