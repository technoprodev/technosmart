<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['sequence']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['sequence'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-left-30 m-margin-left-0">
    <div class="box-8">
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <div class="margin-top-20">
        <div class="box box-break-sm margin-bottom-10">
            <div class="box-1 padding-x-0 text-right m-text-left"><?= $model['sequence']->attributeLabels()['target'] ?></div>
            <div class="box-11 m-padding-x-0 text-dark"><?= $model['sequence']->target ? $model['sequence']->target : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
        </div>
    </div>

    <?= $form->field($model['sequence'], 'last_sequence')->begin(); ?>
        <?= Html::activeLabel($model['sequence'], 'last_sequence', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['sequence'], 'last_sequence', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['sequence'], 'last_sequence', ['class' => 'help-block']); ?>
    <?= $form->field($model['sequence'], 'last_sequence')->end(); ?>

    <?= $form->field($model['sequence'], 'first')->begin(); ?>
        <?= Html::activeLabel($model['sequence'], 'first', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['sequence'], 'first', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['sequence'], 'first', ['class' => 'help-block']); ?>
    <?= $form->field($model['sequence'], 'first')->end(); ?>

    <?= $form->field($model['sequence'], 'second')->begin(); ?>
        <?= Html::activeLabel($model['sequence'], 'second', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['sequence'], 'second', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['sequence'], 'second', ['class' => 'help-block']); ?>
    <?= $form->field($model['sequence'], 'second')->end(); ?>

    <?= $form->field($model['sequence'], 'third')->begin(); ?>
        <?= Html::activeLabel($model['sequence'], 'third', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['sequence'], 'third', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['sequence'], 'third', ['class' => 'help-block']); ?>
    <?= $form->field($model['sequence'], 'third')->end(); ?>


    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['sequence']->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>