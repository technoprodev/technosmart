<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-top-15">
    <div class="box-6">
<?php endif; ?>

<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['sequence']->attributeLabels()['target'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['sequence']->target ? $model['sequence']->target : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['sequence']->attributeLabels()['last_sequence'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['sequence']->last_sequence ? $model['sequence']->last_sequence : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['sequence']->attributeLabels()['first'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['sequence']->first ? $model['sequence']->first : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['sequence']->attributeLabels()['second'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['sequence']->second ? $model['sequence']->second : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['sequence']->attributeLabels()['third'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['sequence']->third ? $model['sequence']->third : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<?php if (!Yii::$app->request->isAjax) : ?>
        <hr class="margin-y-15">
        <div class="form-group clearfix">
            <?= Html::a('Update', ['update', 'id' => $model['sequence']->target], ['class' => 'btn btn-sm btn-default bg-azure rounded-xs border-azure']) ?>&nbsp;
            <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-sm btn-default bg-lighter rounded-xs pull-right']) ?>
        </div>
    </div>
</div>
<?php endif; ?>