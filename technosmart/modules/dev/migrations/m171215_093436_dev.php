<?php

use yii\db\Migration;

class m171215_093436_dev extends Migration
{
    public function safeUp()
    {
        $sql = file_get_contents('dev.sql', true);
        $this->execute($sql);
    }

    public function safeDown()
    {
        $sql = file_get_contents('dev-down.sql', true);
        $this->execute($sql);
    }
}
