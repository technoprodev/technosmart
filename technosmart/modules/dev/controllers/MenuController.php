<?php
namespace technosmart\modules\dev\controllers;

use Yii;
use technosmart\yii\web\Controller;
use technosmart\models\Menu;
use yii\filters\VerbFilter;
use yii\db\Query;

class MenuController extends Controller
{
    public function actionIndex()
    {
        $model['menu'] = Menu::find()->asArray()->all();

        return $this->render('index', [
            'model' => $model,
            'title' => 'Menus',
        ]);
    }

    public function actionMove($id, $order, $idSourceParent, $idDestinationParent, $orderDestinationPrevSibling)
    {
        if (Menu::move($id, $order, $idSourceParent, $idDestinationParent, $orderDestinationPrevSibling)) {
            return $this->json(true);
        } else {
            return $this->json(false);
        }
    }

    protected function findModel($id)
    {
        if (($model = Menu::findOne($id)) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    public function actionCreate($parent = null)
    {
        $render = false;

        $model['menu'] = isset($id) ? $this->findModel($id) : new Menu();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['menu']->load($post);

            $transaction['menu'] = Menu::getDb()->beginTransaction();

            try {
                if ($model['menu']->isNewRecord) {
                    if (!is_null($parent)) {
                        $order = Yii::$app->db->createCommand('SELECT MAX(`order`) FROM `menu` WHERE parent = :parent')->bindValue(':parent', (int) $parent)->queryScalar();
                    } else {
                        $order = Yii::$app->db->createCommand('SELECT MAX(`order`) FROM `menu` WHERE parent IS NULL')->queryScalar();
                    }
                    if ($order) {
                        $model['menu']->order = $order;
                    } else {
                        $model['menu']->order = 0;
                    }
                }
                if (!$model['menu']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved. Please try again.');
                }
                
                $transaction['menu']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
                Yii::$app->cache->flush();
            } catch (\Exception $e) {
                $render = true;
                $transaction['menu']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['menu']->rollBack();
            }
        } else {
            if ($model['menu']->isNewRecord) {
                if (!is_null($parent)) {
                    $model['menu']->parent = $parent;
                }
            }
            $render = true;
        }

        if ($render)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Add New Menu',
            ]);
        else
            return $this->redirect(['index', 'id' => $model['menu']->id]);
    }

    public function actionUpdate($id = null)
    {
        $render = false;

        $model['menu'] = isset($id) ? $this->findModel($id) : new Menu();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['menu']->load($post);

            $transaction['menu'] = Menu::getDb()->beginTransaction();

            try {
                if ($model['menu']->isNewRecord) {
                    $order = Yii::$app->db->createCommand('SELECT MAX(`order`) FROM `menu` WHERE parent = :parent')->bindValue(':parent', (int) $parent)->queryScalar();
                    if ($order) {
                        $model['menu']->order = $order;
                    } else {
                        $model['menu']->order = 0;
                    }
                }
                if (!$model['menu']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved. Please try again.');
                }
                
                $transaction['menu']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
                Yii::$app->cache->flush();
            } catch (\Exception $e) {
                $render = true;
                $transaction['menu']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['menu']->rollBack();
            }
        } else {
            if ($model['menu']->isNewRecord) {
                if (!is_null($parent)) {
                    $model['menu']->parent = $parent;
                }
            }
            $render = true;
        }

        if ($render)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Update Menu ' . $model['menu']->id,
            ]);
        else
            return $this->redirect(['index', 'id' => $model['menu']->id]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->cache->flush();

        return $this->redirect(['index']);
    }
}
