<?php
namespace technosmart\modules\dev\controllers;

use Yii;
use technosmart\models\ConfigurationFile;
use technosmart\yii\web\Controller;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

/**
 * ConfigurationFileController implements highly advanced CRUD actions for ConfigurationFile model.
 */
class ConfigurationFileController extends Controller
{
    public function actionDatatables()
    {
        $db = ConfigurationFile::getDb();
        $post = Yii::$app->request->post();

        // serve data for datatables
        if (isset($post['draw'])) {
            $query = new \yii\db\Query();
            $query
                ->select('count(*)')
                ->from('configuration_file');
            $countWhere = count($query->where);

            $total = $query->scalar($db);
            $return['recordsTotal'] = $total;
            $return['recordsFiltered'] = $total;

            $allWhere = ['or'];
            $allSearch = $post['search']['value'];
            foreach ($post['columns'] as $key => $value) {
                if ($value['searchable'] == 'true') {
                    $column = $value['data'];
                    if (is_array($column)) {
                        if ( isset($column['filter']) )
                            $column = $column['filter'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['search']['regex'] == 'false') {
                        $query->andFilterWhere(['like', $column, $value['search']['value']]);
                    } else if ($value['search']['regex'] == 'true') {
                        $query->andFilterWhere(['regexp', $column, $value['search']['value']]);
                    }

                    if ($allSearch) {
                        if ($post['search']['regex'] == 'false') {
                            $allWhere[] = ['like', $column, $allSearch];
                        } else if ($post['search']['regex'] == 'true') {
                            $allWhere[] = ['regexp', $column, $allSearch];
                        }
                    }
                }
            }
            if (count($allWhere) > 1)
                $query->andFilterWhere($allWhere);
            if (count($query->where) > $countWhere)
                $return['recordsFiltered'] = $query->scalar($db);

            $query->select([
                'target',
                'alias_upload_root',
                'alias_download_base_url',
            ]);

            $order = [];
            if (isset($post['order'])) {
                foreach ($post['order'] as $key => $value) {
                    $column = $post['columns'][$value['column']]['data'];
                    if ($post['columns'][$value['column']]['orderable'] == 'false') {
                        continue;
                    }
                    if (is_array($column)) {
                        if ( isset($column['sort']) )
                            $column = $column['sort'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['dir'] == 'asc')
                        $order[$column] = SORT_ASC;
                    else if ($value['dir'] == 'desc')
                        $order[$column] = SORT_DESC;
                }
            }
            count($order) ? $query->orderBy($order) : 0;

            if (isset($post['length']))
                $query->limit(intval($post['length']));

            if (isset($post['start']))
                $query->offset(intval($post['start']));

            $return['draw'] = intval($post['draw']);
            $return['data'] = $query->all($db);
            return $this->json($return);
        }
    }

    public function actionIndex($id = null)
    {
        // view all data
        if (!$id) {
            return $this->render('list', [
                'title' => 'List of Configuration files',
            ]);
        }
        
        // view single data
        $model['configuration-file'] = $this->findModel($id);
        return $this->render('one', [
            'model' => $model,
            'title' => 'Detail of Configuration file ' . $model['configuration-file']->target,
        ]);
    }

    public function actionUpdate($id = null)
    {
        $render = false;

        $model['configuration-file'] = isset($id) ? $this->findModel($id) : new ConfigurationFile();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['configuration-file']->load($post);

            if (Yii::$app->request->isAjax && isset($post['ajax'])) {
                $result = array_merge(
                    ActiveForm::validate($model['configuration-file'])
                );
                return $this->json($result);
            }

            $transaction['configuration-file'] = ConfigurationFile::getDb()->beginTransaction();

            try {
                if (!$model['configuration-file']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved. Please try again.');
                }
                
                $transaction['configuration-file']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['configuration-file']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['configuration-file']->rollBack();
            }
        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Update Configuration file ' . $model['configuration-file']->target,
            ]);
        else
            return $this->redirect(['index', 'id' => $model['configuration-file']->target]);
    }

    protected function findModel($id)
    {
        if (($model = ConfigurationFile::findOne($id)) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }
}
