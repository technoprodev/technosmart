<?php
namespace technosmart\modules\dev\controllers;

use Yii;
use technosmart\models\Role;
use technosmart\yii\web\Controller;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

/**
 * RoleController implements highly advanced CRUD actions for Role model.
 */
class RoleController extends Controller
{
    /*public static $permissions = [
        ['view', 'View Role'], ['create', 'Create Role'], ['update', 'Update Role'], ['delete', 'Delete Role'],
    ];

    public function behaviors()
    {
        return [
            'access' => $this->access([
                [['index'], 'view'],
                [['index', 'create'], 'create'],
                [['index', 'update'], 'update'],
                [['index', 'delete'], 'delete', null, ['POST']],
            ]),
        ];
    }*/

    public function actionDatatables()
    {
        $db = Role::getDb();
        $post = Yii::$app->request->post();

        // serve data for datatables
        if (isset($post['draw'])) {
            $query = new \yii\db\Query();
            $query
                ->select('count(*)')
                ->from('auth_item');
            $countWhere = count($query->where);

            $total = $query->scalar($db);
            $return['recordsTotal'] = $total;
            $return['recordsFiltered'] = $total;

            $allWhere = ['or'];
            $allSearch = $post['search']['value'];
            foreach ($post['columns'] as $key => $value) {
                if ($value['searchable'] == 'true') {
                    $column = $value['data'];
                    if (is_array($column)) {
                        if ( isset($column['filter']) )
                            $column = $column['filter'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['search']['regex'] == 'false') {
                        $query->andFilterWhere(['like', $column, $value['search']['value']]);
                    } else if ($value['search']['regex'] == 'true') {
                        $query->andFilterWhere(['regexp', $column, $value['search']['value']]);
                    }

                    if ($allSearch) {
                        if ($post['search']['regex'] == 'false') {
                            $allWhere[] = ['like', $column, $allSearch];
                        } else if ($post['search']['regex'] == 'true') {
                            $allWhere[] = ['regexp', $column, $allSearch];
                        }
                    }
                }
            }
            if (count($allWhere) > 1)
                $query->andFilterWhere($allWhere);
            if (count($query->where) > $countWhere)
                $return['recordsFiltered'] = $query->scalar($db);

            $query->select([
                'name',
                'type',
                'description',
                'rule_name',
                'data',
                'created_at',
                'updated_at',
            ]);

            $order = [];
            if (isset($post['order'])) {
                foreach ($post['order'] as $key => $value) {
                    $column = $post['columns'][$value['column']]['data'];
                    if ($post['columns'][$value['column']]['orderable'] == 'false') {
                        continue;
                    }
                    if (is_array($column)) {
                        if ( isset($column['sort']) )
                            $column = $column['sort'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['dir'] == 'asc')
                        $order[$column] = SORT_ASC;
                    else if ($value['dir'] == 'desc')
                        $order[$column] = SORT_DESC;
                }
            }
            count($order) ? $query->orderBy($order) : 0;

            if (isset($post['length']))
                $query->limit(intval($post['length']));

            if (isset($post['start']))
                $query->offset(intval($post['start']));

            $return['draw'] = intval($post['draw']);
            $return['data'] = $query->all($db);
            return $this->json($return);
        }
    }

    /**
     * If param(s) is null, display all datas from models.
     * If all param(s) is not null, display a data from model.
     * @param string $id
     * @return mixed
     */
    public function actionIndex($id = null)
    {
        // view all data
        if (!$id) {
            return $this->render('list', [
                'title' => 'List of Roles',
            ]);
        }
        
        // view single data
        $model['role'] = $this->findModel($id);
        return $this->render('one', [
            'model' => $model,
            'title' => 'Detail of Role ' . $model['role']->id,
        ]);
    }

    /**
     * Creates new data(s) from model(s).
     * If submission is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionCreate()
    {
        $render = false;

        $model['role'] = isset($id) ? $this->findModel($id) : new Role();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['role']->load($post);

            $transaction['role'] = Role::getDb()->beginTransaction();

            try {
                if (!$model['role']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved. Please try again.');
                }
                
                $transaction['role']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['role']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['role']->rollBack();
            }
        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Add New Role',
            ]);
        else
            return $this->redirect(['index', 'id' => $model['role']->id]);
    }

    /**
     * Updates existing data(s) from model(s).
     * If submission is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id = null)
    {
        $render = false;

        $model['role'] = isset($id) ? $this->findModel($id) : new Role();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['role']->load($post);

            $transaction['role'] = Role::getDb()->beginTransaction();

            try {
                if (!$model['role']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved. Please try again.');
                }
                
                $transaction['role']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['role']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['role']->rollBack();
            }
        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Update Role ' . $model['role']->id,
            ]);
        else
            return $this->redirect(['index', 'id' => $model['role']->id]);
    }

    /**
     * Deletes an existing Role model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Role model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Role the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Role::findOne($id)) !== null) {
            return $model;
        } else {
            throw new \yii\web\HttpException(404, 'The requested page does not exist.');
        }
    }

    /**
     * role & permission configuration page
     * @param  string|array $role
     * @return
     */
    public function actionPermission($role = null)
    {
        $post = Yii::$app->request->post();

        // init all permissions
        \app_ptsa\models\Permission::initAllController([
            "app_ptsa\controllers" => "@backend/controllers",
        ]);

        /*Form submission process*/
        if (count($post) > 0) {
            // recreate role-permissions
            \Yii::$app->db->createCommand()->delete("auth_item_child")->query();
            $data = [];

            foreach ($post['perm'] as $perm => $roles) {
                foreach ($roles as $role)
                    $data[] = [$role, $perm];
            }

            \Yii::$app->db->createCommand()->batchInsert("auth_item_child", ["parent", "child"], $data)->query();
            return $this->redirect(["permissions"]);
        }

        /*Render view*/
        $roles = (new \yii\db\Query)->select("name, description")->from("auth_item")->orderBy("name");

        if ($role!=null)
            $roles->where(["in", "name", is_array($role) ? [$role] : $role]);

        # permissions
        $perm = clone $roles;
        $perm->where("type=2");
        $permissions = [];
        $other = [];

        foreach ($perm->each() as $val) {
            // getting controller name
            preg_match_all("/^([\w\-\\\\]+)\:\w+/", $val['name'], $match);

            if (isset($match[1][0]))
                $permissions[$match[1][0]][] = $val;
            else
                $other[] = $val;
        }

        // put "other" permission category to the last
        if (count($other) > 0)
            $permissions['other'] = $other;

        # role permission data
        $query = (new \yii\db\Query)->from("auth_item_child");
        $data = [];

        foreach ($query->each() as $val)
            $data[$val["child"]][] = $val['parent'];

        return $this->render("permissions", [
            "roles" => $roles->andwhere("type=1"),
            "permissions" => $permissions,
            "data" => $data,
        ]);
    }
}
