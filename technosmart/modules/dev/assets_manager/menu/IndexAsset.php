<?php
namespace technosmart\modules\dev\assets_manager\menu;

use yii\web\AssetBundle;

class IndexAsset extends AssetBundle
{
	public $sourcePath = '@technosmart/modules/dev/assets/menu';

    public $css = [];

    public $js = [
        'index.js',
    ];

    public $depends = [
        'technosmart\assets_manager\NestableAsset',
    ];
}