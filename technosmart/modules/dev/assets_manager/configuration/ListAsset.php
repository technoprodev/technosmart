<?php
namespace technosmart\modules\dev\assets_manager\configuration;

use yii\web\AssetBundle;

class ListAsset extends AssetBundle
{
	public $sourcePath = '@technosmart/modules/dev/assets/configuration';

    public $css = [];

    public $js = [
        'list.js',
    ];

    public $depends = [
        'technosmart\assets_manager\DatatablesAsset',
    ];
}