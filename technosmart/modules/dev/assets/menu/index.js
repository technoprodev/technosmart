$(document).ready(function() {
    $('#nestable').nestable({
        expandBtnHTML   : '<button class="text-rose padding-y-5 padding-left-10 f-bold fs-14" data-action="expand"></button>',
        collapseBtnHTML : '<button class="text-rose padding-y-5 padding-left-10 f-bold fs-14" data-action="collapse"></button>',
    }).on('dragEnd', function(event, item, source, destination, position) {
        var id = $(item).attr('data-id');
        var order = $(item).attr('data-order');
        var idSourceParent = $(item).attr('data-parent');
        var idDestinationParent = $('#'+id).parent().parent().attr('data-id');
        var orderDestinationPrevSibling = -1;
        if ($('#'+id).prev().length >= 1) {
            orderDestinationPrevSibling = position-1;
        }

        if (idSourceParent == '') {
            idSourceParent = null;
        }
        if (typeof(idDestinationParent) == 'undefined') {
            idDestinationParent = null;
        }

        $.ajax({
            method: 'POST',
            url: fn.urlTo('dev/menu/move', {
                id: id,
                order: order,
                idSourceParent: idSourceParent,
                idDestinationParent: idDestinationParent,
                orderDestinationPrevSibling: orderDestinationPrevSibling
            }),
            success: function(res) {
                console.log(res);
            }
        });
    });
});