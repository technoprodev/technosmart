<?php
namespace technosmart\modules\faq\assets_manager\faq;

use yii\web\AssetBundle;

class ListAsset extends AssetBundle
{
	public $sourcePath = '@technosmart/modules/faq/assets/faq';

    public $css = [];

    public $js = [
        'list.js',
    ];

    public $depends = [
        'technosmart\assets_manager\DatatablesAsset',
    ];
}