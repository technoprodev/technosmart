<?php
namespace technosmart\modules\faq\models;

use Yii;

/**
 * This is the model class for table "faq".
 *
 * @property integer $id
 * @property string $question
 * @property string $answer
 */
class Faq extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'faq';
    }

    public function rules()
    {
        return [
            //id

            //question
            [['question'], 'required'],
            [['question'], 'string'],

            //answer
            [['answer'], 'required'],
            [['answer'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'question' => 'Question',
            'answer' => 'Answer',
        ];
    }
}
