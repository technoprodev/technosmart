<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['faq']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['faq'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-left-30 m-margin-left-0">
    <div class="box-8">
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model['faq'], 'question')->begin(); ?>
        <?= Html::activeLabel($model['faq'], 'question', ['class' => 'control-label']); ?>
        <?= Html::activeTextArea($model['faq'], 'question', ['class' => 'form-control', 'rows' => 6]) ?>
        <?= Html::error($model['faq'], 'question', ['class' => 'help-block']); ?>
    <?= $form->field($model['faq'], 'question')->end(); ?>

    <?= $form->field($model['faq'], 'answer')->begin(); ?>
        <?= Html::activeLabel($model['faq'], 'answer', ['class' => 'control-label']); ?>
        <?= Html::activeTextArea($model['faq'], 'answer', ['class' => 'form-control', 'rows' => 6]) ?>
        <?= Html::error($model['faq'], 'answer', ['class' => 'help-block']); ?>
    <?= $form->field($model['faq'], 'answer')->end(); ?>


    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['faq']->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>