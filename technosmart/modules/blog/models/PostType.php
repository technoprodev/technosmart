<?php
namespace technosmart\modules\blog\models;

use Yii;

/**
 * This is the model class for table "post_type".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Post[] $posts
 */
class PostType extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'post_type';
    }

    public function rules()
    {
        return [
            //id

            //name
            [['name'], 'required'],
            [['name'], 'string', 'max' => 64],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosts()
    {
        return $this->hasMany(Post::className(), ['id_post_type' => 'id']);
    }
}
