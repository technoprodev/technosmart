<?php
namespace technosmart\modules\blog\models;

use Yii;

/**
 * This is the model class for table "post_category".
 *
 * @property integer $id
 * @property string $name
 * @property integer $parent
 *
 * @property Post[] $posts
 * @property PostCategory $parent0
 * @property PostCategory[] $postCategories
 */
class PostCategory extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'post_category';
    }

    public function rules()
    {
        return [
            //id

            //name
            [['name'], 'required'],
            [['name'], 'string', 'max' => 64],

            //parent
            [['parent'], 'integer'],
            [['parent'], 'exist', 'skipOnError' => true, 'targetClass' => PostCategory::className(), 'targetAttribute' => ['parent' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'parent' => 'Parent',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosts()
    {
        return $this->hasMany(Post::className(), ['id_post_category' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent0()
    {
        return $this->hasOne(PostCategory::className(), ['id' => 'parent']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostCategories()
    {
        return $this->hasMany(PostCategory::className(), ['parent' => 'id']);
    }
}
