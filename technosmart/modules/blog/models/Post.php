<?php
namespace technosmart\modules\blog\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "post".
 *
 * @property integer $id
 * @property integer $created_by
 * @property integer $updated_by
 * @property string $created_at
 * @property string $updated_at
 * @property string $title
 * @property string $slug
 * @property string $excerpt
 * @property string $content
 * @property string $featured_image
 * @property string $status
 * @property string $status_comment
 * @property integer $id_post_category
 * @property integer $id_post_type
 *
 * @property User $createdBy
 * @property User $updatedBy
 * @property PostCategory $postCategory
 * @property PostType $postType
 */
class Post extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'post';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression("now()"),
            ],
        ];
    }

    public function rules()
    {
        return [
            //id

            //created_by
            [['created_by'], 'required'],
            [['created_by'], 'integer'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],

            //updated_by
            [['updated_by'], 'integer'],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],

            //created_at
            [['created_at'], 'safe'],

            //updated_at
            [['updated_at'], 'safe'],

            //title
            [['title'], 'required'],
            [['title'], 'string', 'max' => 128],

            //slug
            [['slug'], 'required'],
            [['slug'], 'string', 'max' => 128],

            //excerpt
            [['excerpt'], 'string'],

            //content
            [['content'], 'string'],

            //featured_image
            [['featured_image'], 'string', 'max' => 64],

            //status
            [['status'], 'string'],

            //status_comment
            [['status_comment'], 'string'],

            //id_post_category
            [['id_post_category'], 'integer'],
            [['id_post_category'], 'exist', 'skipOnError' => true, 'targetClass' => PostCategory::className(), 'targetAttribute' => ['id_post_category' => 'id']],

            //id_post_type
            [['id_post_type'], 'integer'],
            [['id_post_type'], 'exist', 'skipOnError' => true, 'targetClass' => PostType::className(), 'targetAttribute' => ['id_post_type' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'title' => 'Title',
            'slug' => 'Slug',
            'excerpt' => 'Excerpt',
            'content' => 'Content',
            'featured_image' => 'Featured Image',
            'status' => 'Status',
            'status_comment' => 'Status Comment',
            'id_post_category' => 'Id Post Category',
            'id_post_type' => 'Id Post Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostCategory()
    {
        return $this->hasOne(PostCategory::className(), ['id' => 'id_post_category']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostType()
    {
        return $this->hasOne(PostType::className(), ['id' => 'id_post_type']);
    }
}
