<?php

use yii\db\Migration;

class m171215_093437_blog extends Migration
{
    public function safeUp()
    {
        $sql = file_get_contents('blog.sql', true);
        $this->execute($sql);
    }

    public function safeDown()
    {
        echo "m171215_093437_blog cannot be reverted.\n";

        return false;
    }
}
