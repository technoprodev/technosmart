<?php
namespace technosmart\modules\blog\assets_manager\post_type;

use yii\web\AssetBundle;

class ListAsset extends AssetBundle
{
	public $sourcePath = '@technosmart/modules/blog/assets/post-type';

    public $css = [];

    public $js = [
        'list.js',
    ];

    public $depends = [
        'technosmart\assets_manager\DatatablesAsset',
    ];
}