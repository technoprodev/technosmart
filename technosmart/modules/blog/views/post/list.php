<?php

use yii\helpers\Html;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

\technosmart\modules\blog\assets_manager\post\ListAsset::register($this);
?>

<table class="datatables display nowrap table table-striped table-hover table-condensed">
    <thead>
        <tr>
            <th class="text-dark f-default" style="border-bottom: 1px">Action</th>
            <th class="text-dark f-default" style="border-bottom: 1px">Created By</th>
            <th class="text-dark f-default" style="border-bottom: 1px">Updated By</th>
            <th class="text-dark f-default" style="border-bottom: 1px">Created At</th>
            <th class="text-dark f-default" style="border-bottom: 1px">Updated At</th>
            <th class="text-dark f-default" style="border-bottom: 1px">Title</th>
            <th class="text-dark f-default" style="border-bottom: 1px">Slug</th>
            <th class="text-dark f-default" style="border-bottom: 1px">Excerpt</th>
            <th class="text-dark f-default" style="border-bottom: 1px">Content</th>
            <th class="text-dark f-default" style="border-bottom: 1px">Featured Image</th>
            <th class="text-dark f-default" style="border-bottom: 1px">Status</th>
            <th class="text-dark f-default" style="border-bottom: 1px">Status Comment</th>
            <th class="text-dark f-default" style="border-bottom: 1px">Id Post Category</th>
            <th class="text-dark f-default" style="border-bottom: 1px">Id Post Type</th>
        </tr>
        <tr class="dt-search">
            <th class="padding-0"></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search created by" class="form-control border-none f-default padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search updated by" class="form-control border-none f-default padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search created at" class="form-control border-none f-default padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search updated at" class="form-control border-none f-default padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search title" class="form-control border-none f-default padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search slug" class="form-control border-none f-default padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search excerpt" class="form-control border-none f-default padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search content" class="form-control border-none f-default padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search featured image" class="form-control border-none f-default padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search status" class="form-control border-none f-default padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search status comment" class="form-control border-none f-default padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search id post category" class="form-control border-none f-default padding-x-5"/></th>
            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search id post type" class="form-control border-none f-default padding-x-5"/></th>
        </tr>
    </thead>
</table>

<?php if ($this->context->can('create')): ?>
    <hr class="margin-y-15">
    <div>
        <?= Html::a('Create Data', ['create'], ['class' => 'btn btn-sm btn-default bg-azure rounded-xs border-azure']) ?>
    </div>
<?php endif; ?>