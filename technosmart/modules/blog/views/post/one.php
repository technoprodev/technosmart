<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-top-15">
    <div class="box-6">
<?php endif; ?>

<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['post']->attributeLabels()['id'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['post']->id ? $model['post']->id : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['post']->attributeLabels()['created_by'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['post']->created_by ? $model['post']->created_by : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['post']->attributeLabels()['updated_by'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['post']->updated_by ? $model['post']->updated_by : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['post']->attributeLabels()['created_at'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['post']->created_at ? $model['post']->created_at : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['post']->attributeLabels()['updated_at'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['post']->updated_at ? $model['post']->updated_at : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['post']->attributeLabels()['title'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['post']->title ? $model['post']->title : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['post']->attributeLabels()['slug'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['post']->slug ? $model['post']->slug : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['post']->attributeLabels()['excerpt'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['post']->excerpt ? $model['post']->excerpt : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['post']->attributeLabels()['content'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['post']->content ? $model['post']->content : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['post']->attributeLabels()['featured_image'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['post']->featured_image ? $model['post']->featured_image : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['post']->attributeLabels()['status'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['post']->status ? $model['post']->status : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['post']->attributeLabels()['status_comment'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['post']->status_comment ? $model['post']->status_comment : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['post']->attributeLabels()['id_post_category'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['post']->id_post_category ? $model['post']->id_post_category : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= $model['post']->attributeLabels()['id_post_type'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= $model['post']->id_post_type ? $model['post']->id_post_type : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    
<?php if (!Yii::$app->request->isAjax) : ?>
        <hr class="margin-y-15">
        <div class="form-group clearfix">
            <?= Html::a('Update', ['update', 'id' => $model['post']->id], ['class' => 'btn btn-sm btn-default bg-azure rounded-xs border-azure']) ?>&nbsp;
            <?= Html::a('Delete', ['delete', 'id' => $model['post']->id], [
                'class' => 'btn btn-sm btn-default bg-lighter rounded-xs',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item ?',
                    'method' => 'post',
                ],
            ]) ?>
            <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-sm btn-default bg-lighter rounded-xs pull-right']) ?>
        </div>
    </div>
</div>
<?php endif; ?>