<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['post']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['post'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-left-30 m-margin-left-0">
    <div class="box-8">
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model['post'], 'created_by')->begin(); ?>
        <?= Html::activeLabel($model['post'], 'created_by', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['post'], 'created_by', ['class' => 'form-control']) ?>
        <?= Html::error($model['post'], 'created_by', ['class' => 'help-block']); ?>
    <?= $form->field($model['post'], 'created_by')->end(); ?>

    <?= $form->field($model['post'], 'updated_by')->begin(); ?>
        <?= Html::activeLabel($model['post'], 'updated_by', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['post'], 'updated_by', ['class' => 'form-control']) ?>
        <?= Html::error($model['post'], 'updated_by', ['class' => 'help-block']); ?>
    <?= $form->field($model['post'], 'updated_by')->end(); ?>

    <?= $form->field($model['post'], 'created_at')->begin(); ?>
        <?= Html::activeLabel($model['post'], 'created_at', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['post'], 'created_at', ['class' => 'form-control']) ?>
        <?= Html::error($model['post'], 'created_at', ['class' => 'help-block']); ?>
    <?= $form->field($model['post'], 'created_at')->end(); ?>

    <?= $form->field($model['post'], 'updated_at')->begin(); ?>
        <?= Html::activeLabel($model['post'], 'updated_at', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['post'], 'updated_at', ['class' => 'form-control']) ?>
        <?= Html::error($model['post'], 'updated_at', ['class' => 'help-block']); ?>
    <?= $form->field($model['post'], 'updated_at')->end(); ?>

    <?= $form->field($model['post'], 'title')->begin(); ?>
        <?= Html::activeLabel($model['post'], 'title', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['post'], 'title', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['post'], 'title', ['class' => 'help-block']); ?>
    <?= $form->field($model['post'], 'title')->end(); ?>

    <?= $form->field($model['post'], 'slug')->begin(); ?>
        <?= Html::activeLabel($model['post'], 'slug', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['post'], 'slug', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['post'], 'slug', ['class' => 'help-block']); ?>
    <?= $form->field($model['post'], 'slug')->end(); ?>

    <?= $form->field($model['post'], 'excerpt')->begin(); ?>
        <?= Html::activeLabel($model['post'], 'excerpt', ['class' => 'control-label']); ?>
        <?= Html::activeTextArea($model['post'], 'excerpt', ['class' => 'form-control', 'rows' => 6]) ?>
        <?= Html::error($model['post'], 'excerpt', ['class' => 'help-block']); ?>
    <?= $form->field($model['post'], 'excerpt')->end(); ?>

    <?= $form->field($model['post'], 'content')->begin(); ?>
        <?= Html::activeLabel($model['post'], 'content', ['class' => 'control-label']); ?>
        <?= Html::activeTextArea($model['post'], 'content', ['class' => 'form-control', 'rows' => 6]) ?>
        <?= Html::error($model['post'], 'content', ['class' => 'help-block']); ?>
    <?= $form->field($model['post'], 'content')->end(); ?>

    <?= $form->field($model['post'], 'featured_image')->begin(); ?>
        <?= Html::activeLabel($model['post'], 'featured_image', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['post'], 'featured_image', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['post'], 'featured_image', ['class' => 'help-block']); ?>
    <?= $form->field($model['post'], 'featured_image')->end(); ?>

    <?= $form->field($model['post'], 'status')->begin(); ?>
        <?= Html::activeLabel($model['post'], 'status', ['class' => 'control-label']); ?>
        <?= Html::activeDropDownList($model['post'], 'status', [ -1 => '-1', 0 => '0', 1 => '1', 2 => '2', ], ['prompt' => 'Choose one please', 'class' => 'form-control']) ?>
        <?= Html::error($model['post'], 'status', ['class' => 'help-block']); ?>
    <?= $form->field($model['post'], 'status')->end(); ?>

    <?= $form->field($model['post'], 'status_comment')->begin(); ?>
        <?= Html::activeLabel($model['post'], 'status_comment', ['class' => 'control-label']); ?>
        <?= Html::activeDropDownList($model['post'], 'status_comment', [ '0', '1', ], ['prompt' => 'Choose one please', 'class' => 'form-control']) ?>
        <?= Html::error($model['post'], 'status_comment', ['class' => 'help-block']); ?>
    <?= $form->field($model['post'], 'status_comment')->end(); ?>

    <?= $form->field($model['post'], 'id_post_category')->begin(); ?>
        <?= Html::activeLabel($model['post'], 'id_post_category', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['post'], 'id_post_category', ['class' => 'form-control']) ?>
        <?= Html::error($model['post'], 'id_post_category', ['class' => 'help-block']); ?>
    <?= $form->field($model['post'], 'id_post_category')->end(); ?>

    <?= $form->field($model['post'], 'id_post_type')->begin(); ?>
        <?= Html::activeLabel($model['post'], 'id_post_type', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['post'], 'id_post_type', ['class' => 'form-control']) ?>
        <?= Html::error($model['post'], 'id_post_type', ['class' => 'help-block']); ?>
    <?= $form->field($model['post'], 'id_post_type')->end(); ?>


    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['post']->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>