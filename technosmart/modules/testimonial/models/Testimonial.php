<?php
namespace technosmart\modules\testimonial\models;

use Yii;

/**
 * This is the model class for table "testimonial".
 *
 * @property integer $id
 * @property string $name
 * @property string $position
 * @property string $testimonial
 */
class Testimonial extends \technosmart\yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'testimonial';
    }

    public function rules()
    {
        return [
            //id

            //name
            [['name'], 'string', 'max' => 64],

            //position
            [['position'], 'string', 'max' => 64],

            //testimonial
            [['testimonial'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'position' => 'Position',
            'testimonial' => 'Testimonial',
        ];
    }
}
