<?php
namespace technosmart\modules\testimonial\assets_manager\testimonial;

use yii\web\AssetBundle;

class ListAsset extends AssetBundle
{
	public $sourcePath = '@technosmart/modules/testimonial/assets/testimonial';

    public $css = [];

    public $js = [
        'list.js',
    ];

    public $depends = [
        'technosmart\assets_manager\DatatablesAsset',
    ];
}