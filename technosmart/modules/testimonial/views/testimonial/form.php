<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['testimonial']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['testimonial'], ['class' => '']);
}
?>

<?php if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-left-30 m-margin-left-0">
    <div class="box-8">
<?php endif; ?>

<?php $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>

    <?= $form->field($model['testimonial'], 'name')->begin(); ?>
        <?= Html::activeLabel($model['testimonial'], 'name', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['testimonial'], 'name', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['testimonial'], 'name', ['class' => 'help-block']); ?>
    <?= $form->field($model['testimonial'], 'name')->end(); ?>

    <?= $form->field($model['testimonial'], 'position')->begin(); ?>
        <?= Html::activeLabel($model['testimonial'], 'position', ['class' => 'control-label']); ?>
        <?= Html::activeTextInput($model['testimonial'], 'position', ['class' => 'form-control', 'maxlength' => true]) ?>
        <?= Html::error($model['testimonial'], 'position', ['class' => 'help-block']); ?>
    <?= $form->field($model['testimonial'], 'position')->end(); ?>

    <?= $form->field($model['testimonial'], 'testimonial')->begin(); ?>
        <?= Html::activeLabel($model['testimonial'], 'testimonial', ['class' => 'control-label']); ?>
        <?= Html::activeTextArea($model['testimonial'], 'testimonial', ['class' => 'form-control', 'rows' => 6]) ?>
        <?= Html::error($model['testimonial'], 'testimonial', ['class' => 'help-block']); ?>
    <?= $form->field($model['testimonial'], 'testimonial')->end(); ?>


    <hr class="margin-y-15">

    <?php if ($error) : ?>
        <div class="alert alert-danger">
            <?= $errorMessage ?>
        </div>
    <?php endif; ?>
    
    <div class="form-group clearfix">
        <?= Html::submitButton($model['testimonial']->isNewRecord ? 'Create' : 'Update', ['class' => 'btn btn-default bg-azure rounded-xs border-azure']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default bg-lighter rounded-xs']); ?> 
        <?= Html::a('Back to list', ['index'], ['class' => 'btn btn-default bg-lightest rounded-xs pull-right']) ?>
    </div>
    
<?php ActiveForm::end(); ?>

<?php if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?php endif; ?>