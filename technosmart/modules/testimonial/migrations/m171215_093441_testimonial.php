<?php

use yii\db\Migration;

class m171215_093441_testimonial extends Migration
{
    public function safeUp()
    {
        $sql = file_get_contents('testimonial.sql', true);
        $this->execute($sql);
    }

    public function safeDown()
    {
        echo "m171215_093441_testimonial cannot be reverted.\n";

        return false;
    }
}
