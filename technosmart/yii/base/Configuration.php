<?php
namespace technosmart\yii\base;

use Yii;
use yii\base\BootstrapInterface;

class Configuration implements BootstrapInterface
{
    /**
    * Bootstrap method to be called during application bootstrap stage.
    * Loads all the settings into the Yii::$app->params array
    *
    * @param Application $app the application currently running
    */
    public function bootstrap($app)
    {
        $cache = Yii::$app->cache;
        $key = 'configurations';
        $cacheData;
        if ($cache->exists($key)) {
            $cacheData = $cache->get($key);
        } else {
            $configurations = Yii::$app->db->createCommand('SELECT name, type, value FROM configuration')->queryAll();
            foreach ($configurations as $val) {
                $type = $val['type'];
                switch ($type) {
                    case 'int':
                        $val['value'] = (int)$val['value'];
                        break;
                    case 'float':
                        $val['value'] = (float)$val['value'];
                        break;
                    case 'bool':
                        $val['value'] = $val['value'] === 'true'? true: false;
                        break;
                    default:
                        break;
                }
                $cacheData[$val['name']] = $val['value'];
            }
            $cache->set($key, $cacheData);
        }

        // cek profiling
        foreach ($cacheData as $key => $val) {
            Yii::$app->params[$key] = $val;
        }

        $key = 'configurations_file';
        if ($cache->exists($key)) {
            $configurations_file = $cache->get($key);
        } else {
            $configurations_file = Yii::$app->db->createCommand('SELECT target, alias_upload_root, alias_download_base_url FROM configuration_file')->queryAll();
            $cache->set($key, $configurations_file);
        }

        foreach ($configurations_file as $key => $val) {
            Yii::$app->params['configurations_file'][$val['target']]['alias_upload_root'] = $val['alias_upload_root'];
            Yii::$app->params['configurations_file'][$val['target']]['alias_download_base_url'] = $val['alias_download_base_url'];
        }
    }
}
