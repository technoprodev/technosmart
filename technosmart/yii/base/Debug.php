<?php
namespace technosmart\yii\base;

use Symfony\Component\VarDumper\Dumper\CliDumper;
use Symfony\Component\VarDumper\Cloner\VarCloner;
use Symfony\Component\VarDumper\Dumper\HtmlDumper;

class Debug
{
    public function dd()
    {
        global $backtrace_index;
        $backtrace = debug_backtrace();
        echo "<hr><b>".$this->trace_format($backtrace[$backtrace_index==null ? 0 : $backtrace_index])."</b>";

        array_map(function ($value) {
            $this->dump($value);
        }, func_get_args());
    }

    public function ddx()
    {
        global $backtrace_index;
        $backtrace_index = 2;

        echo '<!DOCTYPE html><html lang="en"><head></head><body>';
        call_user_func_array([$this, 'dd'], func_get_args());
        echo '</body></html>';

        die(0);
    }

    public function dump($value)
    {
        if (class_exists(CliDumper::class)) {
            $dumper = (PHP_SAPI === 'cli') ? new CliDumper : new HtmlDumper;
            $dumper->dump((new VarCloner)->cloneVar($value));
        } else var_dump($value);
    }

    protected function trace_format($trace)
    {
        if (isset($trace['file']))
            return str_replace(realpath('sites/all/modules'), null, str_replace('\\', '/', $trace['file'])).' line: '.$trace['line'];
    }
}