<?php
namespace technosmart\yii\web;

use Yii;
use technosmart\models\Permission;
use technosmart\models\Sequence;
use yii\helpers\Inflector;

class Controller extends \yii\web\Controller
{
    /**
     * shortcut for \Yii::$app->user->can('')
     * 
     * @param mixed $perms
     * @return boolean
     */
    public function can($permissions = null)
    {
        switch (gettype($permissions)) {
            case 'NULL':
                return false;
                break;
            case 'boolean':
                return $permissions;
                break;
            case 'object':
                if (is_callable($permissions))
                    return $permissions();
                else
                    return false;
                break;
            case 'array':
                $can = true;
                foreach ($permissions as $permission)
                    $can = $can && $this->can($permission);
                return $can;
                break;
            case 'string':
                return \Yii::$app->user->can($this->id . ":" . $permissions);
                break;
            default: return (bool) $permissions;
        }
    }

    // cek profiling
    public function canAccess($actionId)
    {
        $action = $this->createAction($actionId);
        if (!isset($action))
            return false;

        if(isset($this->getBehaviors()['access']))
            $behaviours = $this->getBehaviors()['access'];
        else return true;

        $user = $behaviours->user;
        $request = Yii::$app->getRequest();
        /* @var $rule AccessRule */
        foreach ($behaviours->rules as $rule) {
            if ($allow = $rule->allows($action, $user, $request)) {
                return true;
            } elseif ($allow === false) {
                return false;
            }
        }
        return true;
    }
    // $this->context->can('create'); call it on view
    // Yii::$app->controller->canAccess('devvv'); call it everywhere
    // Yii::$app->createControllerByID('applicant')->canAccess('view'); call it everywhere

    /**
     * return controller id with static call
     *
     * @return string
     */
    public static function id()
    {
        $class = get_called_class();
        $mark = "controllers";
        $classname = preg_replace("/Controller$/", "", substr($class, strpos($class, $mark)+strlen($mark)+1));
        $pos = strrpos($classname, "\\");

        return ($pos > 0 ? substr($classname, 0, $pos) . "/" : '') . Inflector::camel2id(substr($classname, $pos !== false ? $pos + 1 : 0));
    }

    /**
     * template access control
     * 
     * @param array $permissions see rule method
     * @param closure $denyCallback
     * @return array
     */
    public function access(array $permissions, $denyCallback = null, $user = 'user')
    {
        $rules = $only = array();

        try {
            foreach ($permissions as $permission) {
                if (!is_array($permission))
                    throw new \yii\web\HttpException(400, "wrong call. permitted param: ([
                        [['action'], true, ['role name'], ['post', 'get'], function() {return 'denycallback'}],
                        [['action1', 'action2'], 'permission', ['@'], ['post', 'get'], function() {return 'denycallback'}]
                    ])");

                $rules[] = call_user_func_array([$this, 'rule'], $permission);

                if (isset($permission[0])) {
                    if (is_array($permission[0]))
                        $only = array_merge($only, $permission[0]);
                    else
                        $only[] = $permission[0];
                }
            }
        } catch(Exception $e) {}

        return [
            'class' => \yii\filters\AccessControl::className(),
            'only' => $only,
            'denyCallback' => is_callable($denyCallback) ? $denyCallback : function($rule, $action) {
                throw new \yii\web\HttpException(403, 'You are forbidden from accessing this page.');
            },
            'user' => $user,
            'rules' => $rules,
        ];
    }

    /**
     * template access rule for access conrol
     * 
     * @param array $actions
     * @param mixed $permission
     * @param array $roles
     * @param array $verbs allowed request method
     * @param closure $denyCallback
     * @return array
     */
    protected function rule($actions = '*', $permission = true, $roles = null, $verbs = array('POST', 'GET'), $denyCallback = null)
    {
        return [
            'actions' => $actions == '*' ? null : $actions,
            'allow' => $this->can($permission),
            'roles' => $roles,
            'verbs' => $verbs,
            'denyCallback' => is_callable($denyCallback) ? $denyCallback : null,
        ];
    }

    public function render($view, $params=[])
    {
        if (\Yii::$app->request->isAjax)
            return $this->renderAjax($view, $params, $this);
        else
            return parent::render($view, $params, $this);
    }

    public function json($data)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $data;
    }

    public function sequence($target = null)
    {
        if(!$target) return false;

        $sequence = Sequence::findOne($target);
        $val = $sequence->last_sequence;
        $first = $sequence->first;
        $second = $sequence->second;
        $third = $sequence->third;
        $this->increment($val, [$first, $second, $third]);
        $sequence->last_sequence = $val;
        $sequence->save();
        return $val;
    }
    // $this->sequence('dev-id_combination')

    protected function increment(&$string, $sequence)
    {
        $sequence = array_filter($sequence, function ($value) {
            return !empty($value) || $value === '0';
        });

        $last_char=substr($string, -1);
        $rest=substr($string, 0, -1);

        switch ($last_char) {
        case '':
            $next = $sequence[0];
            break;
        case $this->lastSequence($sequence[count($sequence)-1]):
            $this->increment($rest, $sequence);
            $next = $sequence[0];
            break;
        case $this->lastSequence($sequence[0]):
            $next = isset($sequence[1]) ? $sequence[1] : $sequence[0];
            break;
        case $this->lastSequence(isset($sequence[1]) ? $sequence[1] : null):
            $next = isset($sequence[2]) ? $sequence[2] : $sequence[1];
            break;
        default:
            $next = ++$last_char;
        }
        $string = $rest . $next;
    }

    protected function lastSequence($begin)
    {
        switch ($begin) {
            case 'a':
                return 'z';
                break;
            case 'A':
                return 'Z';
                break;
            case '0':
                return '9';
                break;
            default:
                throw new \yii\web\HttpException(400, 'wrong call. permitted param: ("a"), ("A") or ("0")');
                break;
        }
    }

    //

    public function datatables($query, $post, $db)
    {
        if (isset($post['draw'])) {
            $select = $query->select;

            $query->select('count(*)');
            $countWhere = $query->where === null ? 0 : count($query->where);

            $total = $query->scalar($db);
            $return['recordsTotal'] = $total;
            $return['recordsFiltered'] = $total;

            $allWhere = ['or'];
            $allSearch = $post['search']['value'];
            foreach ($post['columns'] as $key => $value) {
                if ($value['searchable'] == 'true') {
                    $column = $value['data'];
                    if (is_array($column)) {
                        if ( isset($column['filter']) )
                            $column = $column['filter'];
                        else
                            $column = $column['_'];
                    }

                    if (isset($value['data']['equal']) && $value['data']['equal'] == 'true') {
                        $query->andFilterWhere([$column => $value['search']['value']]);
                    }
                    else if ($value['search']['regex'] == 'false') {
                        $query->andFilterWhere(['like', $column, $value['search']['value']]);
                    } else if ($value['search']['regex'] == 'true') {
                        $query->andFilterWhere(['regexp', $column, $value['search']['value']]);
                    }

                    if ($allSearch) {
                        if ($post['search']['regex'] == 'false') {
                            $allWhere[] = ['like', $column, $allSearch];
                        } else if ($post['search']['regex'] == 'true') {
                            $allWhere[] = ['regexp', $column, $allSearch];
                        }
                    }
                }
            }
            if (($allWhere === null ? 0 : count($allWhere)) > 1)
                $query->andFilterWhere($allWhere);
            if (($query->where === null ? 0 : count($query->where)) > $countWhere)
                $return['recordsFiltered'] = $query->scalar($db);

            $query->select($select);

            $order = [];
            if (isset($post['order'])) {
                foreach ($post['order'] as $key => $value) {
                    $column = $post['columns'][$value['column']]['data'];
                    if ($post['columns'][$value['column']]['orderable'] == 'false') {
                        continue;
                    }
                    if (is_array($column)) {
                        if ( isset($column['sort']) )
                            $column = $column['sort'];
                        else
                            $column = $column['_'];
                    }

                    if ($value['dir'] == 'asc')
                        $order[$column] = SORT_ASC;
                    else if ($value['dir'] == 'desc')
                        $order[$column] = SORT_DESC;
                }
            }
            ($order === null ? 0 : count($order)) ? $query->orderBy($order) : 0;

            if (isset($post['length']))
                $query->limit(intval($post['length']));

            if (isset($post['start']))
                $query->offset(intval($post['start']));

            $return['draw'] = intval($post['draw']);
            $return['data'] = $query->all($db);
            return $this->json($return);
        }
    }
}
