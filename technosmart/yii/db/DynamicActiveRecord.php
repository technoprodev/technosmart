<?php
namespace technosmart\yii\db;

class DynamicActiveRecord extends ActiveRecord
{
	public static $dynamicTable;
	public static $dynamicDb;

	public static function tableName()
    {
        return self::$dynamicTable;
    }

    public static function getDb()
    {
        return \Yii::$app->get(self::$dynamicDb);
    }
}