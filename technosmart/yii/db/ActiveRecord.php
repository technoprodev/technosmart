<?php
namespace technosmart\yii\db;

class ActiveRecord extends \yii\db\ActiveRecord {

    // cek profiling
    public function load($data, $formName = null)
    {
        $name = $this->formName();

        if (isset($data[$name])) {
            foreach ($data[$name] as $key => $value) {
                if ($value == '') {
                    $data[$name][$key] = null;
                }
            }
            // $data[$name] = array_filter($data[$name], function(&$val) {
            //     return $val != '';
            // });
        }

        return parent::load($data, $formName);
    }

    // cek profiling
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $cache = \Yii::$app->cache;
            $key = 'schema-column-' . $this->formName();
            if (!$cache->exists($key))
                $cache->set($key, $this->getTableSchema()->columns);
            $columns = $cache->get($key);

            $attributes = $this->getAttributes();
            foreach ($attributes as $key => $value) {
                $dbType = $columns[$key]->dbType;
                $trimLength = strpos($dbType, '(');
                if ($trimLength)
                    $type = substr($dbType, 0, $trimLength);
                else
                    $type = $dbType;

                switch ($type) {
                    case 'set':
                        if (is_array($value))
                            $this->$key = implode(',', $value);
                        break;
                    case 'date':
                        if ($value && !($value instanceof \yii\db\Expression))
                            $this->$key = date('Y-m-d', strtotime(str_replace('/', '-', $value)));
                        break;
                    case 'datetime':
                        if ($value && !($value instanceof \yii\db\Expression))
                            $this->$key = date('Y-m-d H:i:s', strtotime(str_replace('/', '-', $value)));
                        break;
                }
            }

            return true;
        } else {
            return false;
        }
    }

    // cek profiling
    public function __get($name)
    {
        // \Yii::$app->d->dd("get $name", $this->getAttribute($name));
        // return parent::__get($name);

        $cache = \Yii::$app->cache;
        $key = 'schema-column-' . $this->formName();
        if (!$cache->exists($key))
            $cache->set($key, $this->getTableSchema()->columns);
        $columns = $cache->get($key);

        if (!isset($columns[$name]->dbType))
            return parent::__get($name);

        $dbType = $columns[$name]->dbType;
        $trimLength = strpos($dbType, '(');
        if ($trimLength)
            $type = substr($dbType, 0, $trimLength);
        else
            $type = $dbType;
        
        switch ($type) {
            case 'timestamp':
                $val = parent::__get($name);
                if (!$val)
                    return $val;
                return date('d/m/Y H:i:s', strtotime($val));
                break;
            case 'date':
                $val = parent::__get($name);
                if (!$val || $val instanceof \yii\db\Expression)
                    return $val;
                return date('d/m/Y', strtotime(str_replace('/', '-', $val)));
                break;
            case 'datetime':
                $val = parent::__get($name);
                if (!$val || $val instanceof \yii\db\Expression)
                    return $val;
                return date('d/m/Y H:i:s', strtotime(str_replace('/', '-', $val)));
                break;
            case 'set':
                $val = parent::__get($name);
                if (!$val)
                    return $val;
                else if (!is_array($val)){
                    // \Yii::$app->d->ddx(explode(',', $val));

                    return explode(',', $val);
                }
                return $val;
                break;
            default:
                return parent::__get($name);
                break;
        }
    }

    // cek profiling
    public function getEnum($column)
    {
        $cache = \Yii::$app->cache;
        $key = 'enum-' . $this->formName() . $column;

        if (!$cache->exists($key)) {
            $dsn = $this->getDb()->dsn;
            $dbname = substr($dsn, strpos($dsn, 'dbname=') + 7);
            $sql = "
                SELECT SUBSTRING(COLUMN_TYPE,5) AS enum
                FROM information_schema.COLUMNS
                WHERE TABLE_SCHEMA='" . $dbname . "' 
                    AND TABLE_NAME='" . $this->tableName() . "'
                    AND COLUMN_NAME='" . $column . "'
            ";
            $enum = \Yii::$app->db->createCommand($sql)->queryOne();
            if(!$enum) return [];
            if ($enum['enum']) {
                $enum = substr($enum['enum'], 2);
                $enum = substr($enum, 0, -2);
                $enum = explode("','", $enum);
                $return = [];
                foreach ($enum as $value) {
                    $return["$value"] = $value;
                }
                $cache->set($key, $return);
            }
        }
        return $cache->get($key);
    }

    // cek profiling
    public function getSet($column)
    {
        $cache = \Yii::$app->cache;
        $key = 'set-' . $this->formName() . $column;

        if (!$cache->exists($key)) {
            $dsn = $this->getDb()->dsn;
            $dbname = substr($dsn, strpos($dsn, 'dbname=') + 7);
            $sql = "
                SELECT SUBSTRING(COLUMN_TYPE,4) AS 'set'
                FROM information_schema.COLUMNS
                WHERE TABLE_SCHEMA='" . $dbname . "' 
                    AND TABLE_NAME='" . $this->tableName() . "'
                    AND COLUMN_NAME='" . $column . "'
            ";
            $set = \Yii::$app->db->createCommand($sql)->queryOne();
            if(!$set) return [];
            if ($set['set']) {
                $set = substr($set['set'], 2);
                $set = substr($set, 0, -2);
                $set = explode("','", $set);
                $return = [];
                foreach ($set as $value) {
                    $return["$value"] = $value;
                }
                $cache->set($key, $return);
            }
        }
        return $cache->get($key);
    }
}