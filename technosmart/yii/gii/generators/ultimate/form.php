<?php

use technosmart\yii\gii\generators\ultimate\Generator;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $generator yii\gii\generators\crud\Generator */
?>

<hr>
<p><i>Model options</i></p>

<?php
echo $form->field($generator, 'tableName')->textInput(['table_prefix' => $generator->getTablePrefix()]);
echo $form->field($generator, 'modelClass');
echo $form->field($generator, 'ns');
echo $form->field($generator, 'baseClass');
echo $form->field($generator, 'db');
?>

<hr>
<p><i>Model Advanced options</i> <a role="button" data-toggle="collapse" href="#model-advanced-options" aria-expanded="false" aria-controls="model-advanced-options"><i class="glyphicon glyphicon-plus"></i></a></p>
<div class="collapse" id="model-advanced-options">
	<?php
	echo $form->field($generator, 'useTablePrefix')->checkbox();
	echo $form->field($generator, 'generateRelations')->dropDownList([
	    Generator::RELATIONS_NONE => 'No relations',
	    Generator::RELATIONS_ALL => 'All relations',
	    Generator::RELATIONS_ALL_INVERSE => 'All relations with inverse',
	]);
	echo $form->field($generator, 'generateLabelsFromComments')->checkbox();
	echo $form->field($generator, 'generateQuery')->checkbox();
	echo $form->field($generator, 'queryNs');
	echo $form->field($generator, 'queryClass');
	echo $form->field($generator, 'queryBaseClass');
	echo $form->field($generator, 'useSchemaName')->checkbox();
	?>
</div>

<hr>
<p><i>Controller options</i></p>

<?php
echo $form->field($generator, 'generateController')->checkbox();
echo $form->field($generator, 'controllerClass');
echo $form->field($generator, 'nsController');
echo $form->field($generator, 'baseControllerClass');
?>

<hr>
<p><i>View options</i></p>

<?php
echo $form->field($generator, 'generateView')->checkbox();
echo $form->field($generator, 'viewPath');
?>

<hr>
<p><i>Advanced options</i> <a role="button" data-toggle="collapse" href="#advanced-options" aria-expanded="false" aria-controls="advanced-options"><i class="glyphicon glyphicon-plus"></i></a></p>
<div class="collapse" id="advanced-options">
	<?php
	echo $form->field($generator, 'enableI18N')->checkbox();
	echo $form->field($generator, 'messageCategory');
	?>
</div>
<hr>