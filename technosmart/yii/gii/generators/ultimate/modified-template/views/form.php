<?php

use yii\helpers\Inflector;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

/* @var $model \yii\db\ActiveRecord */
$model = $modelObject;
$safeAttributes = $model->safeAttributes();
if (empty($safeAttributes)) {
    $safeAttributes = $model->attributes();
}
$modelId = Inflector::camel2id($className);

echo "<?php\n";
?>

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$error = false;
$errorMessage = '';
if ($model['<?= $modelId ?>']->hasErrors()) {
    $error = true; 
    $errorMessage .= Html::errorSummary($model['<?= $modelId ?>'], ['class' => '']);
}
?>

<?= "<?php" ?> if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-left-30 m-margin-left-0">
    <div class="box-8">
<?= "<?php" ?> endif; ?>

<?= "<?php" ?> $form = ActiveForm::begin(['enableClientValidation' => true, 'options' => ['id' => 'app']]); ?>
  
    <?= "<?php" ?> if ($error) : ?>
        <div class="alert alert-danger">
            <?= "<?=" ?> $errorMessage ?>
        </div>
    <?= "<?php" ?> endif; ?>

<?php foreach ($generator->getColumnNames() as $attribute) {
    if (in_array($attribute, $safeAttributes) && $attribute != 'id') {

        echo "    <?= \$form->field(\$model['" . $modelId . "'], '" . $attribute . "')->begin(); ?>\n";
        echo "        <?= Html::activeLabel(\$model['" . $modelId . "'], '" . $attribute . "', ['class' => 'control-label']); ?>\n";
        echo "        " . $generator->generateActiveInput($attribute);
        echo "        <?= Html::error(\$model['" . $modelId . "'], '" . $attribute . "', ['class' => 'help-block']); ?>\n";
        echo "    <?= \$form->field(\$model['" . $modelId . "'], '" . $attribute . "')->end(); ?>\n\n";
    }
} ?>

    <hr class="margin-y-15">

    <?= "<?php" ?> if ($error) : ?>
        <div class="alert alert-danger">
            <?= "<?=" ?> $errorMessage ?>
        </div>
    <?= "<?php" ?> endif; ?>
    
    <div class="form-group clearfix">
        <?= "<?=" ?> Html::submitButton($model['<?= $modelId ?>']->isNewRecord ? 'Create' : 'Update', ['class' => 'btn bg-azure border-azure hover-bg-light-azure']) ?>
        <?= "<?=" ?> Html::resetButton('Reset', ['class' => 'btn text-azure border-azure hover-bg-light-azure']); ?> 
        <?= "<?=" ?> Html::a('Back to list', ['index'], ['class' => 'btn text-azure border-azure hover-bg-azure pull-right']) ?>
    </div>
    
<?= "<?php" ?> ActiveForm::end(); ?>

<?= "<?php" ?> if (!Yii::$app->request->isAjax) : ?>
    </div>
</div>
<?= "<?php" ?> endif; ?>