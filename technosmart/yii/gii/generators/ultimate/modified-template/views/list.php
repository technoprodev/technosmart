<?php

use yii\helpers\Inflector;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$modelId = Inflector::camel2id($className);
$model = $modelObject;
preg_match('~^(\w+)~', $generator->nsController, $matches);
$nsAsset = $matches[0];

echo "<?php\n";
?>

use yii\helpers\Html;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;

$this->registerJsFile('@web/app/<?= $modelId ?>/list.js', ['depends' => 'technosmart\assets_manager\DatatablesAsset']);
?>

<table class="datatables display nowrap table table-striped table-hover table-condensed">
    <thead>
        <tr><?= "\n" ?><?php foreach ($tableSchema->columns as $column): ?><?= $column->name!='id'?"            <th class=\"text-dark f-default\" style=\"border-bottom: 1px\">{$labels[$column->name]}</th>\n":"            <th class=\"text-dark f-default\" style=\"border-bottom: 1px\">Action</th>\n" ?><?php endforeach; ?>
        </tr>
        <tr class="dt-search"><?= "\n" ?><?php foreach ($tableSchema->columns as $column): ?><?= $column->name!='id'?'            <th class="padding-0"><input type="text" style="font-family:FontAwesome;" placeholder="&#xf002; Search ' . strtolower($labels[$column->name]) . '" class="form-control border-none f-default padding-x-5"/></th>' . "\n":'            <th class="padding-0"></th>' . "\n" ?><?php endforeach; ?>
        </tr>
    </thead>
</table>

<?= "<?php" ?> if ($this->context->can('create')): ?>
    <hr class="margin-y-15">
    <div>
        <?= "<?=" ?> Html::a('Create Data', ['create'], ['class' => 'btn btn-sm btn-default bg-azure rounded-xs border-azure']) ?>
    </div>
<?= "<?php" ?> endif; ?>