<?php

use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();
$modelId = Inflector::camel2id($className);

echo "<?php\n";
?>

use yii\helpers\Html;
use yii\widgets\DetailView;

if (isset($title)) $this->title = $title;
if (isset($subtitle)) $this->subtitle = $subtitle;
if (isset($breadcrumbs)) $this->params['breadcrumbs'] = $breadcrumbs;
?>

<?= "<?php" ?> if (!Yii::$app->request->isAjax) : ?>
<div class="box box-break-sm margin-top-15">
    <div class="box-6">
<?= "<?php" ?> endif; ?>
<?php
if (($tableSchema = $generator->getTableSchema()) === false) {
    foreach ($generator->getColumnNames() as $name) {
        echo "            '" . $name . "',\n";
    }
} else {
    foreach ($generator->getTableSchema()->columns as $column) { ?>

<div class="box box-break-sm margin-bottom-10">
    <div class="box-2 padding-x-0 text-right m-text-left"><?= "<?=" ?> $model['<?= $modelId ?>']->attributeLabels()['<?= $column->name ?>'] ?></div>
    <div class="box-10 m-padding-x-0 text-dark"><?= "<?=" ?> $model['<?= $modelId ?>']-><?= $column->name ?> ? $model['<?= $modelId ?>']-><?= $column->name ?> : '<span class="text-gray f-italic">(kosong)</span>' ?></div>
</div>
    <?php }
}
?>

<?= "<?php" ?> if (!Yii::$app->request->isAjax) : ?>
        <hr class="margin-y-15">
        <div class="form-group clearfix">
            <?= "<?=" ?> Html::a('Update', ['update', <?= $urlParams ?>], ['class' => 'btn bg-azure border-azure hover-bg-light-azure']) ?>&nbsp;
            <?= "<?=" ?> Html::a('Delete', ['delete', <?= $urlParams ?>], [
                'class' => 'btn text-azure border-azure hover-bg-light-azure',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item ?',
                    'method' => 'post',
                ],
            ]) ?>
            <?= "<?=" ?> Html::a('Back to list', ['index'], ['class' => 'btn text-azure border-azure hover-bg-azure pull-right']) ?>
        </div>
    </div>
</div>
<?= "<?php" ?> endif; ?>