<?php
/**
 * This is the template for generating a CRUD controller class file.
 */

use yii\db\ActiveRecordInterface;
use yii\helpers\StringHelper;
use yii\helpers\Inflector;


/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$controllerClass = StringHelper::basename($generator->nsController . '//' . $controllerClassName);
$modelClass = StringHelper::basename($generator->ns . '//' . $className);
$modelId = Inflector::camel2id($className);
$title = Inflector::titleize($className);
$titlePlural = Inflector::pluralize($title);

/* @var $class ActiveRecordInterface */
$urlParams = $generator->generateUrlParams();
$actionParams = $generator->generateActionParams();
$actionParamsNull = $generator->generateActionParamsNull();
$actionParamComments = $generator->generateActionParamComments();

echo "<?php\n";
?>
namespace <?= StringHelper::dirname(ltrim($namespacedControllerClass, '\\')) ?>;

use Yii;
use <?= ltrim($namespacedModelClass, '\\') ?>;
use <?= ltrim($generator->baseControllerClass, '\\') ?>;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

/**
 * <?= $controllerClass ?> implements highly advanced CRUD actions for <?= $modelClass ?> model.
 */
class <?= $controllerClass ?> extends <?= StringHelper::basename($generator->baseControllerClass) . "\n" ?>
{
    /*public static $permissions = [
        ['view', 'View <?= $title ?>'], ['create', 'Create <?= $title ?>'], ['update', 'Update <?= $title ?>'], ['delete', 'Delete <?= $title ?>'],
    ];

    public function behaviors()
    {
        return [
            'access' => $this->access([
                [['index'], 'view'],
                [['index', 'create'], 'create'],
                [['index', 'update'], 'update'],
                [['index', 'delete'], 'delete', null, ['POST']],
            ]),
        ];
    }*/

    /**
     * Finds the <?= $modelClass ?> model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * <?= implode("\n     * ", $actionParamComments) . "\n" ?>
     * @return <?=                   $modelClass ?> the loaded model
     * @throws HttpException if the model cannot be found
     */
    protected function findModel(<?= $actionParams ?>)
    {
<?php
if (count($pks) === 1) {
    $condition = '$id';
} else {
    $condition = [];
    foreach ($pks as $pk) {
        $condition[] = "'$pk' => \$$pk";
    }
    $condition = '[' . implode(', ', $condition) . ']';
}
?>
        if (($model = <?= $modelClass ?>::findOne(<?= $condition ?>)) !== null) {
            return $model;
        } else {
            throw new HttpException(400, 'The requested page does not exist.');
        }
    }

    public function actionDatatables()
    {
        $query = new \yii\db\Query();
        $query
            ->select([<?= "\n" ?><?php foreach ($tableSchema->columns as $column): ?><?= "                '$column->name',\n" ?><?php endforeach; ?>
            ])
            ->from('<?= $tableName ?>')
        ;
        
        return $this->datatables($query, Yii::$app->request->post(), <?= $modelClass ?>::getDb());
    }

    /**
     * If param(s) is null, display all datas from models.
     * If all param(s) is not null, display a data from model.
     * <?= implode("\n     * ", $actionParamComments) . "\n" ?>
     * @return mixed
     */
    public function actionIndex(<?= $actionParamsNull ?>)
    {
        // view all data
        if (!$id) {
            return $this->render('list', [
                'title' => 'List of <?= $titlePlural ?>',
            ]);
        }
        
        // view single data
        $model['<?= $modelId ?>'] = $this->findModel(<?= $actionParams ?>);
        return $this->render('one', [
            'model' => $model,
            'title' => 'Detail of <?= $title ?> ' . $model['<?= $modelId ?>']->id,
        ]);
    }

    /**
     * Creates new data(s) from model(s).
     * If submission is successful, the browser will be redirected to the 'view' page.
     * <?= implode("\n     * ", $actionParamComments) . "\n" ?>
     * @return mixed
     */
    public function actionCreate()
    {
        $render = false;

        $model['<?= $modelId ?>'] = isset($id) ? $this->findModel($id) : new <?= $modelClass ?>();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['<?= $modelId ?>']->load($post);

            $transaction['<?= $modelId ?>'] = <?= $modelClass ?>::getDb()->beginTransaction();

            try {
                if (!$model['<?= $modelId ?>']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved. Please try again.');
                }
                
                $transaction['<?= $modelId ?>']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['<?= $modelId ?>']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['<?= $modelId ?>']->rollBack();
            }
        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Add New <?= $title ?>',
            ]);
        else
            return $this->redirect(['index']);
    }

    /**
     * Updates existing data(s) from model(s).
     * If submission is successful, the browser will be redirected to the 'view' page.
     * <?= implode("\n     * ", $actionParamComments) . "\n" ?>
     * @return mixed
     */
    public function actionUpdate(<?= $actionParamsNull ?>)
    {
        $render = false;

        $model['<?= $modelId ?>'] = isset($id) ? $this->findModel($id) : new <?= $modelClass ?>();

        if (Yii::$app->request->isPost) {
            $post = Yii::$app->request->post();

            $model['<?= $modelId ?>']->load($post);

            $transaction['<?= $modelId ?>'] = <?= $modelClass ?>::getDb()->beginTransaction();

            try {
                if (!$model['<?= $modelId ?>']->save()) {
                    throw new \yii\web\HttpException(400, 'Data cannot be saved. Please try again.');
                }
                
                $transaction['<?= $modelId ?>']->commit();
                Yii::$app->session->setFlash('success', 'Data has been saved.');
            } catch (\Exception $e) {
                $render = true;
                $transaction['<?= $modelId ?>']->rollBack();
            } catch (\Throwable $e) {
                $render = true;
                $transaction['<?= $modelId ?>']->rollBack();
            }
        } else {
            $render = true;
        }

        if ($render)
            return $this->render('form', [
                'model' => $model,
                'title' => 'Update <?= $title ?> ' . $model['<?= $modelId ?>']->id,
            ]);
        else
            return $this->redirect(['index']);
    }

    /**
     * Deletes an existing <?= $modelClass ?> model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * <?= implode("\n     * ", $actionParamComments) . "\n" ?>
     * @return mixed
     */
    public function actionDelete(<?= $actionParams ?>)
    {
        $this->findModel(<?= $actionParams ?>)->delete();

        return $this->redirect(['index']);
    }
}
