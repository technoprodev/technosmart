<?php
/**
 * enhanced generator from yii\gii\generators\crud yii\gii\generators\model
 */
namespace technosmart\yii\gii\generators\ultimate;

use Yii;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Connection;
use yii\db\Schema;
use yii\db\TableSchema;
use yii\gii\CodeFile;
use yii\helpers\Inflector;
use yii\base\NotSupportedException;

use yii\db\BaseActiveRecord;
use yii\helpers\VarDumper;
use yii\web\Controller;

/**
 * Generates CRUD
 * This generator will generate one or multiple ActiveRecord classes for the specified database table.
 *
 * @property array                $columnNames      Model column names. This property is read-only.
 * @property string               $controllerID     The controller ID (without the module ID prefix). This property is read-only.
 * @property array                $searchAttributes Searchable attributes. This property is read-only.
 * @property boolean              $tableSchema      This property is read-only.
 * @property \yii\db\TableSchema  $tableSchema      This property is read-only.
 * @property string               $viewPath         The controller view path. This property is read-only.
 *
 */
class Generator extends \yii\gii\generators\model\Generator
{
    const RELATIONS_NONE = 'none';
    const RELATIONS_ALL = 'all';
    const RELATIONS_ALL_INVERSE = 'all-inverse';

    public $tableName;
    public $modelClass;
    public $ns = 'technosmart\models';
    public $baseClass = 'technosmart\yii\db\ActiveRecord';
    public $db = 'db';

    public $useTablePrefix = false;
    public $generateRelations = self::RELATIONS_ALL;
    public $generateLabelsFromComments = false;
    public $generateQuery = false;
    public $queryNs = 'technosmart\models';
    public $queryClass;
    public $queryBaseClass = 'yii\db\ActiveQuery';
    public $useSchemaName = true;

    public $generateController = true;
    public $controllerClass;
    public $nsController = 'technosmart\controllers';
    public $baseControllerClass = 'technosmart\yii\web\Controller';

    public $generateView = true;
    public $viewPath;

    public function getName()
    {
        return 'Ultimate Generator';
    }

    public function getDescription()
    {
        return 'This generator generates an ActiveRecord class for the specified database table. This generator also generates a controller and views that implement CRUD operations for the specified data model.';
    }

    public function rules()
    {
        return array_merge(parent::rules(), [
            [['db', 'ns', 'nsController', 'tableName', 'modelClass', 'baseClass', 'queryNs', 'queryClass', 'queryBaseClass'], 'filter', 'filter' => 'trim'],
            [['ns', 'nsController', 'queryNs'], 'filter', 'filter' => function($value) { return trim($value, '\\'); }],

            [['db', 'ns', 'tableName', 'baseClass', 'queryNs', 'queryBaseClass'], 'required'],
            [['nsController'], 'required', 'when' => function ($model) {
                return $model->generateController;
            }, 'message' => 'Controller Namespace is required if you check Generate Controller.'],
            [['db', 'modelClass', 'queryClass'], 'match', 'pattern' => '/^\w+$/', 'message' => 'Only word characters are allowed.'],
            [['ns', 'nsController', 'baseClass', 'queryNs', 'queryBaseClass'], 'match', 'pattern' => '/^[\w\\\\]+$/', 'message' => 'Only word characters and backslashes are allowed.'],
            [['tableName'], 'match', 'pattern' => '/^([\w ]+\.)?([\w\* ]+)$/', 'message' => 'Only word characters, and optionally spaces, an asterisk and/or a dot are allowed.'],
            [['db'], 'validateDb'],
            [['ns', 'queryNs', 'nsController'], 'validateNamespace'],
            [['tableName'], 'validateTableName'],
            [['modelClass'], 'validateModelClass', 'skipOnEmpty' => false],
            [['baseClass'], 'validateClass', 'params' => ['extends' => ActiveRecord::className()]],
            [['queryBaseClass'], 'validateClass', 'params' => ['extends' => ActiveQuery::className()]],
            [['generateRelations'], 'in', 'range' => [self::RELATIONS_NONE, self::RELATIONS_ALL, self::RELATIONS_ALL_INVERSE]],
            [['generateLabelsFromComments', 'useTablePrefix', 'generateController', 'generateView', 'useSchemaName', 'generateQuery'], 'boolean'],

            [['controllerClass', 'baseControllerClass'], 'filter', 'filter' => 'trim'],
            [['controllerClass'], 'required', 'when' => function ($model) {
                    return ($model->generateController && ((empty($this->tableName) || substr_compare($this->tableName, '*', -1, 1)) && $this->controllerClass == ''));
                }, 'whenClient' => "function (attribute, value) {
                    return ($('#generator-generatecontroller').is(':checked') && $('#model-generator #generator-tablename').val().indexOf('*') === -1 && $('#model-generator #generator-tablename').val() != '');
                }", 'message' => 'Controller Class and Base Controller Class are required if you check Generate Controller.',
                'skipOnEmpty' => false],
            [['baseControllerClass'], 'required', 'when' => function ($model) {
                    return ($model->generateController && ((empty($this->tableName) || substr_compare($this->tableName, '*', -1, 1)) && $this->baseControllerClass == ''));
                }, 'whenClient' => "function (attribute, value) {
                    return ($('#generator-generatecontroller').is(':checked') && $('#model-generator #generator-tablename').val().indexOf('*') === -1 && $('#model-generator #generator-tablename').val() != '');
                }", 'message' => 'Controller Class and Base Controller Class are required if you check Generate Controller.',
                'skipOnEmpty' => false],
            [['baseControllerClass'], 'match', 'pattern' => '/^[\w\\\\]*$/', 'message' => 'Only word characters and backslashes are allowed.'],
            [['controllerClass'], 'match', 'pattern' => '/^\w+$/', 'message' => 'Only word characters are allowed.'],
            [['baseControllerClass'], 'validateClass', 'params' => ['extends' => Controller::className()]],
            [['controllerClass'], 'match', 'pattern' => '/Controller$/', 'message' => 'Controller class name must be suffixed with "Controller".'],
            [['controllerClass'], 'match', 'pattern' => '/(^|\\\\)[A-Z][^\\\\]+Controller$/', 'message' => 'Controller class name must start with an uppercase letter.'],
            [['enableI18N'], 'boolean'],
            [['messageCategory'], 'validateMessageCategory', 'skipOnEmpty' => false],
            ['viewPath', 'safe'],
        ]);
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'tableName' => 'Table Name',
            'modelClass' => 'Model Class',
            'ns' => 'Model Namespace',
            'baseClass' => 'Base Model Class',
            'db' => 'Database Connection ID',

            'useTablePrefix' => 'Use Table Prefix',
            'generateRelations' => 'Generate Relations',
            'generateLabelsFromComments' => 'Generate Labels from DB Comments',
            'generateQuery' => 'Generate ActiveQuery',
            'queryNs' => 'ActiveQuery Namespace',
            'queryClass' => 'ActiveQuery Class',
            'queryBaseClass' => 'ActiveQuery Base Class',
            'useSchemaName' => 'Use Schema Name',

            'generateController' => 'Generate Controller',
            'controllerClass' => 'Controller Class',
            'nsController' => 'Controller Namespace',
            'baseControllerClass' => 'Base Controller Class',

            'generateView' => 'Generate View',
            'viewPath' => 'View Path',
        ]);
    }

    public function hints()
    {
        return array_merge(parent::hints(), [
            'tableName' => 'This is the name of the DB table that the new ActiveRecord class is associated with, e.g. <code>post</code>.
                The table name may consist of the DB schema part if needed, e.g. <code>public.post</code>.
                The table name may end with asterisk to match multiple table names, e.g. <code>tbl_*</code>
                will match tables who name starts with <code>tbl_</code>. In this case, multiple ActiveRecord classes
                will be generated, one for each matching table name; and the class names will be generated from
                the matching characters. For example, table <code>tbl_post</code> will generate <code>Post</code>
                class.',
            'modelClass' => 'This is the name of the ActiveRecord class to be generated. The class name should not contain
                the namespace part as it is specified in "Model Namespace". You do not need to specify the class name
                if "Table Name" ends with asterisk, in which case multiple ActiveRecord classes will be generated.',
            'ns' => 'This is the namespace of the ActiveRecord class to be generated, e.g., <code>app\models</code>',
            'baseClass' => 'This is the base class of the new ActiveRecord class. It should be a fully qualified namespaced class name.',
            'db' => 'This is the ID of the DB application component.',
            'generateRelations' => 'This indicates whether the generator should generate relations based on
                foreign key constraints it detects in the database. Note that if your database contains too many tables,
                you may want to uncheck this option to accelerate the code generation process.',
            'generateLabelsFromComments' => 'This indicates whether the generator should generate attribute labels
                by using the comments of the corresponding DB columns.',
            'useTablePrefix' => 'This indicates whether the table name returned by the generated ActiveRecord class
                should consider the <code>tablePrefix</code> setting of the DB connection. For example, if the
                table name is <code>tbl_post</code> and <code>tablePrefix=tbl_</code>, the ActiveRecord class
                will return the table name as <code>{{%post}}</code>.',
            'useSchemaName' => 'This indicates whether to include the schema name in the ActiveRecord class
                when it\'s auto generated. Only non default schema would be used.',
            'generateQuery' => 'This indicates whether to generate ActiveQuery for the ActiveRecord class.',
            'queryNs' => 'This is the namespace of the ActiveQuery class to be generated, e.g., <code>app\models</code>',
            'queryClass' => 'This is the name of the ActiveQuery class to be generated. The class name should not contain
                the namespace part as it is specified in "ActiveQuery Namespace". You do not need to specify the class name
                if "Table Name" ends with asterisk, in which case multiple ActiveQuery classes will be generated.',
            'queryBaseClass' => 'This is the base class of the new ActiveQuery class. It should be a fully qualified namespaced class name.',

            'controllerClass' => 'This is the name of the controller class to be generated. The class name should not contain
                the namespace part as it is specified in "Controller Namespace". (e.g. <code>PostController</code>),
                and class name should be in CamelCase with an uppercase first letter. Make sure the class
                is using the same namespace as specified by your application\'s controllerNamespace property.',
            'nsController' => 'This is the namespace of the Controller class to be generated, e.g., <code>app\controller</code>',
            'baseControllerClass' => 'This is the class that the new CRUD controller class will extend from.
                You should provide a fully qualified class name, e.g., <code>yii\web\Controller</code>.',

            'viewPath' => 'Specify the directory for storing the view scripts for the controller. You may use path alias here, e.g.,
                <code>/var/www/basic/controllers/views/post</code>, <code>@app/views/post</code>. If not set, it will default
                to <code>@app/views/ControllerID</code>',
        ]);
    }

    public function autoCompleteData()
    {
        $db = $this->getDbConnection();
        if ($db !== null) {
            return [
                'tableName' => function () use ($db) {
                    return $db->getSchema()->getTableNames();
                },
            ];
        } else {
            return [];
        }
    }

    public function requiredTemplates()
    {
        return [
            'model.php',
            'controller.php',
        ];
    }

    public function stickyAttributes()
    {
        return array_merge(parent::stickyAttributes(), [
            'ns', 'db', 'baseClass', 'generateRelations', 'generateLabelsFromComments', 'queryNs', 'queryBaseClass',
            'nsController', 'baseControllerClass'
        ]);
    }

    /**
     * Returns the `tablePrefix` property of the DB connection as specified
     */
    public function getTablePrefix()
    {
      $db = $this->getDbConnection();
      if ($db !== null) {
          return $db->tablePrefix;
      } else {
          return '';
      }
    }

    public function generate()
    {
        // Yii::$app->d->ddx('B');
        $files = [];
        $relations = $this->generateRelations();
        $db = $this->getDbConnection();
        // Yii::$app->d->dd($relations, $db);
        foreach ($this->getTableNames() as $tableName) {
            // model :
            $modelClassName = $this->generateClassName($tableName);
            $queryClassName = ($this->generateQuery) ? $this->generateQueryClassName($modelClassName) : false;
            $tableSchema = $db->getTableSchema($tableName);
            $params = [
                'tableName' => $tableName,
                'className' => $modelClassName,
                'queryClassName' => $queryClassName,
                'tableSchema' => $tableSchema,
                'labels' => $this->generateLabels($tableSchema),
                'rules' => $this->generateRules($tableSchema),
                'relations' => isset($relations[$tableName]) ? $relations[$tableName] : [],
            ];
            // Yii::$app->d->dd($params);
            // Yii::$app->d->dd(Yii::getAlias('@' . str_replace('\\', '/', $this->ns)) . '/' . $modelClassName . '.php');
            $files[] = new CodeFile(
                Yii::getAlias('@' . str_replace('\\', '/', $this->ns)) . '/' . $modelClassName . '.php',
                $this->render('model.php', $params)
            );

            // query :
            if ($queryClassName) {
                $params['className'] = $queryClassName;
                $params['modelClassName'] = $modelClassName;
                $files[] = new CodeFile(
                    Yii::getAlias('@' . str_replace('\\', '/', $this->queryNs)) . '/' . $queryClassName . '.php',
                    $this->render('query.php', $params)
                );
            }

            $this->currentTableName = $tableName;
            
            // controller :
            $this->namespacedModelClass = $this->ns . '\\' . $modelClassName;
            $params['namespacedModelClass'] = $this->namespacedModelClass;
            $controllerClassName = $this->generateControllerClassName($modelClassName);
            $this->namespacedControllerClass = $this->nsController . '\\' . $controllerClassName;
            $params['controllerClassName'] = $controllerClassName;
            $params['namespacedControllerClass'] = $this->namespacedControllerClass;
            $params['pks'] = $db->getTableSchema($tableName)->primaryKey;
            if ($this->generateController) {
                $controllerFile = Yii::getAlias('@' . str_replace('\\', '/', ltrim($this->namespacedControllerClass, '\\')) . '.php');
                $files[] = new CodeFile($controllerFile, $this->render('controller.php', $params));
            }

            // view :
            $params['modelObject'] = new \technosmart\yii\db\DynamicActiveRecord;
            $params['modelObject']::$dynamicTable = $tableName;
            $params['modelObject']::$dynamicDb = $this->db;
            if ($this->generateView) {
                $viewPath = $this->getViewPath();
                $templatePath = $this->getTemplatePath() . '/views';
                foreach (scandir($templatePath) as $file) {
                    if (is_file($templatePath . '/' . $file) && pathinfo($file, PATHINFO_EXTENSION) === 'php') {
                        $files[] = new CodeFile("$viewPath/$file", $this->render("views/$file", $params));
                    }
                }
            }

            // datatables init
            if ($this->generateView) {
                $viewAllJs = Yii::getAlias('@app/web/app/' . $this->getControllerID() . '/list.js');
                $files[] = new CodeFile($viewAllJs, $this->render("list.php", $params));
            }
        }

        return $files;
    }

    /**
     * Generates a controller class name from the specified model class name.
     * @param string $modelClassName the model class name
     * @return string the generated controller class name
     */
    protected function generateControllerClassName($modelClassName)
    {
        $db = $this->getDbConnection();
        if ($db === null) {
            return null;
        }

        if (($table = $db->getTableSchema($this->tableName, true)) !== null) {
            return $this->controllerClass;
        } else {
            return $modelClassName . 'Controller';
        }
    }

// model generator

    /**
     * Validates the namespace.
     *
     * @param string $attribute Namespace variable.
     */
    public function validateNamespace($attribute)
    {
        $value = $this->$attribute;
        $value = ltrim($value, '\\');
        $path = Yii::getAlias('@' . str_replace('\\', '/', $value), false);
        if ($path === false) {
            $this->addError($attribute, "The Controller Namespace is invalid: $ns");
        } elseif (!is_dir($path)) {
            $this->addError($attribute, "Please make sure the directory containing this class exists: $path");
        }
    }

    /**
     * Validates the [[tableName]] attribute.
     */
    public function validateTableName()
    {
        if (strpos($this->tableName, '*') !== false && substr_compare($this->tableName, '*', -1, 1)) {
            $this->addError('tableName', 'Asterisk is only allowed as the last character.');

            return;
        }
        $tables = $this->getTableNames();
        if (empty($tables)) {
            $this->addError('tableName', "Table '{$this->tableName}' does not exist.");
        } else {
            foreach ($tables as $table) {
                $class = $this->generateClassName($table);
                if ($this->isReservedKeyword($class)) {
                    $this->addError('tableName', "Table '$table' will generate a class which is a reserved PHP keyword.");
                    break;
                }
                $pk = $this->getDbConnection()->getTableSchema($table)->primaryKey;
                if (empty($pk)) {
                    $this->addError('tableName', "Table $table must have primary key(s).");
                }
            }
        }
    }

// crud generator
    protected $currentTableName;
    protected $namespacedModelClass;
    protected $namespacedControllerClass;

    /**
     * @return string the controller ID (without the module ID prefix)
     */
    public function getControllerID()
    {
        $pos = strrpos($this->namespacedControllerClass, '\\');
        $class = substr(substr($this->namespacedControllerClass, $pos + 1), 0, -10);

        return Inflector::camel2id($class);
    }

    /**
     * @return string the controller view path
     */
    public function getViewPath()
    {
        if (empty($this->viewPath)) {
            return Yii::getAlias('@app/views/' . $this->getControllerID());
        } else {
            return Yii::getAlias($this->viewPath);
        }
    }

    public function getNameAttribute()
    {
        foreach ($this->getColumnNames() as $name) {
            if (!strcasecmp($name, 'name') || !strcasecmp($name, 'title')) {
                return $name;
            }
        }
        /* @var $class \yii\db\ActiveRecord */
        $class = $this->namespacedModelClass;
        $pk = $class::primaryKey();

        return $pk[0];
    }

    /**
     * Generates code for active field
     * @param string $attribute
     * @return string
     */
    public function generateActiveField($attribute)
    {
        $tableSchema = $this->getTableSchema();
        if ($tableSchema === false || !isset($tableSchema->columns[$attribute])) {
            if (preg_match('/^(password|pass|passwd|passcode)$/i', $attribute)) {
                return "\$form->field(\$model['" . Inflector::camel2id($this->generateClassName($this->currentTableName)) . "'], '$attribute')->passwordInput()";
            } else {
                return "\$form->field(\$model['" . Inflector::camel2id($this->generateClassName($this->currentTableName)) . "'], '$attribute')";
            }
        }
        $column = $tableSchema->columns[$attribute];
        if ($column->phpType === 'boolean') {
            return "\$form->field(\$model['" . Inflector::camel2id($this->generateClassName($this->currentTableName)) . "'], '$attribute')->checkbox()";
        } elseif ($column->type === 'text') {
            return "\$form->field(\$model['" . Inflector::camel2id($this->generateClassName($this->currentTableName)) . "'], '$attribute')->textarea(['rows' => 6])";
        } else {
            if (preg_match('/^(password|pass|passwd|passcode)$/i', $column->name)) {
                $input = 'passwordInput';
            } else {
                $input = 'textInput';
            }
            if (is_array($column->enumValues) && count($column->enumValues) > 0) {
                $dropDownOptions = [];
                foreach ($column->enumValues as $enumValue) {
                    $dropDownOptions[$enumValue] = Inflector::humanize($enumValue);
                }
                return "\$form->field(\$model['" . Inflector::camel2id($this->generateClassName($this->currentTableName)) . "'], '$attribute')->dropDownList("
                    . preg_replace("/\n\s*/", ' ', VarDumper::export($dropDownOptions)).", ['prompt' => ''])";
            } elseif ($column->phpType !== 'string' || $column->size === null) {
                return "\$form->field(\$model['" . Inflector::camel2id($this->generateClassName($this->currentTableName)) . "'], '$attribute')->$input()";
            } else {
                return "\$form->field(\$model['" . Inflector::camel2id($this->generateClassName($this->currentTableName)) . "'], '$attribute')->$input(['maxlength' => true])";
            }
        }
    }

    /**
     * Generates code for active input
     * @param string $attribute
     * @return string
     */
    public function generateActiveInput($attribute)
    {
        $tableSchema = $this->getTableSchema();
        if ($tableSchema === false || !isset($tableSchema->columns[$attribute])) {
            if (preg_match('/^(password|pass|passwd|passcode)$/i', $attribute)) {
                return "<?= Html::activePasswordInput(\$model['" . Inflector::camel2id($this->generateClassName($this->currentTableName)) . "'], '$attribute', ['class' => 'form-control']) ?>\n";
            } else {
                return "<?= Html::activeTextInput(\$model['" . Inflector::camel2id($this->generateClassName($this->currentTableName)) . "'], '$attribute', ['class' => 'form-control']) ?>\n";
            }
        }
        $column = $tableSchema->columns[$attribute];
        if ($column->phpType === 'boolean') {
            return "<div class=\"checkbox\"><?= Html::activeCheckbox(\$model['" . Inflector::camel2id($this->generateClassName($this->currentTableName)) . "'], '$attribute', ['uncheck' => null]) ?></div>\n";
        } elseif ($column->type === 'text') {
            return "<?= Html::activeTextArea(\$model['" . Inflector::camel2id($this->generateClassName($this->currentTableName)) . "'], '$attribute', ['class' => 'form-control', 'rows' => 6]) ?>\n";
        } else {
            if (preg_match('/^(password|pass|passwd|passcode)$/i', $column->name)) {
                $input = 'passwordInput';
            } else {
                $input = 'textInput';
            }
            if (is_array($column->enumValues) && count($column->enumValues) > 0) {
                $dropDownOptions = [];
                foreach ($column->enumValues as $enumValue) {
                    $dropDownOptions[$enumValue] = Inflector::humanize($enumValue);
                }
                return "<?= Html::activeDropDownList(\$model['" . Inflector::camel2id($this->generateClassName($this->currentTableName)) . "'], '$attribute', "
                    . preg_replace("/\n\s*/", ' ', VarDumper::export($dropDownOptions)).", ['prompt' => 'Choose one please', 'class' => 'form-control']) ?>\n";
            } elseif ($column->phpType !== 'string' || $column->size === null) {
                return "<?= Html::activeTextInput(\$model['" . Inflector::camel2id($this->generateClassName($this->currentTableName)) . "'], '$attribute', ['class' => 'form-control']) ?>\n";
            } else {
                return "<?= Html::activeTextInput(\$model['" . Inflector::camel2id($this->generateClassName($this->currentTableName)) . "'], '$attribute', ['class' => 'form-control', 'maxlength' => true]) ?>\n";
            }
        }
    }

    /**
     * Generates code for active search field
     * @param string $attribute
     * @return string
     */
    public function generateActiveSearchField($attribute)
    {
        $tableSchema = $this->getTableSchema();
        if ($tableSchema === false) {
            return "\$form->field(\$model, '$attribute')";
        }
        $column = $tableSchema->columns[$attribute];
        if ($column->phpType === 'boolean') {
            return "\$form->field(\$model, '$attribute')->checkbox()";
        } else {
            return "\$form->field(\$model, '$attribute')";
        }
    }

    /**
     * Generates column format
     * @param \yii\db\ColumnSchema $column
     * @return string
     */
    public function generateColumnFormat($column)
    {
        if ($column->phpType === 'boolean') {
            return 'boolean';
        } elseif ($column->type === 'text') {
            return 'ntext';
        } elseif (stripos($column->name, 'time') !== false && $column->phpType === 'integer') {
            return 'datetime';
        } elseif (stripos($column->name, 'email') !== false) {
            return 'email';
        } elseif (stripos($column->name, 'url') !== false) {
            return 'url';
        } else {
            return 'text';
        }
    }

    /**
     * Generates validation rules for the search model.
     * @return array the generated validation rules
     */
    public function generateSearchRules()
    {
        if (($table = $this->getTableSchema()) === false) {
            return ["[['" . implode("', '", $this->getColumnNames()) . "'], 'safe']"];
        }
        $types = [];
        foreach ($table->columns as $column) {
            switch ($column->type) {
                case Schema::TYPE_SMALLINT:
                case Schema::TYPE_INTEGER:
                case Schema::TYPE_BIGINT:
                    $types['integer'][] = $column->name;
                    break;
                case Schema::TYPE_BOOLEAN:
                    $types['boolean'][] = $column->name;
                    break;
                case Schema::TYPE_FLOAT:
                case Schema::TYPE_DOUBLE:
                case Schema::TYPE_DECIMAL:
                case Schema::TYPE_MONEY:
                    $types['number'][] = $column->name;
                    break;
                case Schema::TYPE_DATE:
                case Schema::TYPE_TIME:
                case Schema::TYPE_DATETIME:
                case Schema::TYPE_TIMESTAMP:
                default:
                    $types['safe'][] = $column->name;
                    break;
            }
        }

        $rules = [];
        foreach ($types as $type => $columns) {
            $rules[] = "[['" . implode("', '", $columns) . "'], '$type']";
        }

        return $rules;
    }

    /**
     * @return array searchable attributes
     */
    public function getSearchAttributes()
    {
        return $this->getColumnNames();
    }

    /**
     * Generates the attribute labels for the search model.
     * @return array the generated attribute labels (name => label)
     */
    public function generateSearchLabels()
    {
        /* @var $model \yii\base\Model */
        $model = new $this->namespacedModelClass();
        $attributeLabels = $model->attributeLabels();
        $labels = [];
        foreach ($this->getColumnNames() as $name) {
            if (isset($attributeLabels[$name])) {
                $labels[$name] = $attributeLabels[$name];
            } else {
                if (!strcasecmp($name, 'id')) {
                    $labels[$name] = 'ID';
                } else {
                    $label = Inflector::camel2words($name);
                    if (!empty($label) && substr_compare($label, ' id', -3, 3, true) === 0) {
                        $label = substr($label, 0, -3) . ' ID';
                    }
                    $labels[$name] = $label;
                }
            }
        }

        return $labels;
    }

    /**
     * Generates search conditions
     * @return array
     */
    public function generateSearchConditions()
    {
        $columns = [];
        if (($table = $this->getTableSchema()) === false) {
            $class = $this->namespacedModelClass;
            /* @var $model \yii\base\Model */
            $model = new $class();
            foreach ($model->attributes() as $attribute) {
                $columns[$attribute] = 'unknown';
            }
        } else {
            foreach ($table->columns as $column) {
                $columns[$column->name] = $column->type;
            }
        }

        $likeConditions = [];
        $hashConditions = [];
        foreach ($columns as $column => $type) {
            switch ($type) {
                case Schema::TYPE_SMALLINT:
                case Schema::TYPE_INTEGER:
                case Schema::TYPE_BIGINT:
                case Schema::TYPE_BOOLEAN:
                case Schema::TYPE_FLOAT:
                case Schema::TYPE_DOUBLE:
                case Schema::TYPE_DECIMAL:
                case Schema::TYPE_MONEY:
                case Schema::TYPE_DATE:
                case Schema::TYPE_TIME:
                case Schema::TYPE_DATETIME:
                case Schema::TYPE_TIMESTAMP:
                    $hashConditions[] = "'{$column}' => \$this->{$column},";
                    break;
                default:
                    $likeConditions[] = "->andFilterWhere(['like', '{$column}', \$this->{$column}])";
                    break;
            }
        }

        $conditions = [];
        if (!empty($hashConditions)) {
            $conditions[] = "\$query->andFilterWhere([\n"
                . str_repeat(' ', 12) . implode("\n" . str_repeat(' ', 12), $hashConditions)
                . "\n" . str_repeat(' ', 8) . "]);\n";
        }
        if (!empty($likeConditions)) {
            $conditions[] = "\$query" . implode("\n" . str_repeat(' ', 12), $likeConditions) . ";\n";
        }

        return $conditions;
    }

    /**
     * Generates URL parameters
     * @return string
     */
    public function generateUrlParams()
    {
        /* @var $class ActiveRecord */
        $class = $this->namespacedModelClass;
        $pks = $this->getDbConnection()->getTableSchema($this->currentTableName, true)->primaryKey;
        if (count($pks) === 1) {
            return "'id' => \$model['" . Inflector::camel2id($this->generateClassName($this->currentTableName)) . "']->{$pks[0]}";
        } else {
            $params = [];
            foreach ($pks as $pk) {
                $params[] = "'$pk' => \$model['" . Inflector::camel2id($this->generateClassName($this->currentTableName)) . "']->$pk";
            }

            return implode(', ', $params);
        }
    }

    /**
     * Generates action parameters
     * @return string
     */
    public function generateActionParams()
    {
        /* @var $class ActiveRecord */
        $class = $this->namespacedModelClass;
        $pks = $this->getDbConnection()->getTableSchema($this->currentTableName, true)->primaryKey;
        if (count($pks) === 1) {
            return '$id';
        } else {
            return '$' . implode(', $', $pks) . ' = null';
        }
    }

    /**
     * Generates action parameters with null value as default
     * @return string
     */
    public function generateActionParamsNull()
    {
        /* @var $class ActiveRecord */
        $class = $this->namespacedModelClass;
        $pks = $this->getDbConnection()->getTableSchema($this->currentTableName, true)->primaryKey;
        if (count($pks) === 1) {
            return '$id = null';
        } else {
            return '$' . implode(' = null, $', $pks) . ' = null';
        }
    }

    /**
     * Generates parameter tags for phpdoc
     * @return array parameter tags for phpdoc
     */
    public function generateActionParamComments()
    {
        /* @var $class ActiveRecord */
        $class = $this->namespacedModelClass;
        $pks = $this->getDbConnection()->getTableSchema($this->currentTableName, true)->primaryKey;
        if (($table = $this->getTableSchema()) === false) {
            $params = [];
            foreach ($pks as $pk) {
                $params[] = '@param ' . (substr(strtolower($pk), -2) == 'id' ? 'integer' : 'string') . ' $' . $pk;
            }

            return $params;
        }
        if (count($pks) === 1) {
            return ['@param ' . $table->columns[$pks[0]]->phpType . ' $id'];
        } else {
            $params = [];
            foreach ($pks as $pk) {
                $params[] = '@param ' . $table->columns[$pk]->phpType . ' $' . $pk;
            }

            return $params;
        }
    }

    /**
     * Returns table schema for current model class or false if it is not an active record
     * @return boolean|\yii\db\TableSchema
     */
    public function getTableSchema()
    {
        return $this->getDbConnection()->getTableSchema($this->currentTableName, true);
    }

    /**
     * @return array model column names
     */
    public function getColumnNames()
    {
        /* @var $model \yii\base\Model */
        $model = new \technosmart\yii\db\DynamicActiveRecord;
        // Yii::$app->d->ddx($this->currentTableName);
        $model::$dynamicTable = $this->currentTableName;

        return $model->attributes();
    }
}
