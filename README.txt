/*========================================================================*\
  Plan
\*========================================================================*/
current plan
    ulik tryout
    pokemon
        buka gift, ksh makan absol, koin + revive item
        jalankan egg
        Habiskan FRP di heathran 2145 2681
        minimal sehari 1 trap shadow bagon
        main spinda
        skip party hat
        skip pikachu, stantler wild/task
    adsense
        103.129.223.115 cPHlxYEwxct4
        clovemedicalpublic@gmail.com
        root : ,dc$Qtjul!oqW1E]
        user4admin : rQNTc(BpIM)a=&T4
        ---
        benerin *, li
        cantumin sumber
        pages
        analityc
        ---
        counter
        localin gambar
        fix url skip it match but not found
    technoartcss.com
        kakukan teknis
        iklanin
        download all source template
    selesaikan sisa sisa sadewa & masukptn
    branding
        bayar
        technoprodev.com
        prafandy.com
    technosmart
        yii (web open source + mobile wrapper)
        vue (web open source + mobile wrapper + bisa akses komponen native + tampilan ui nya native)

/*========================================================================*\
  Technosmart
\*========================================================================*/
To Do List
    minimum starter
        all input type (duit, decimal, waktu, dll)
        datatables search by range
    error
        benerin redirect slash
        hilangin folder technosmart
        bikin standalone gii dengan satu pintu variable
        unikin access token
        url
            benerin url module kl bentrok dengan controller
            benerin urlManager yang tidak bisa multiple module
            standalone action dan action di SiteController ngga perlu dialiasin lagi di url manager
        action SiteController
            hilangin extend ke SiteController
                yang nggak pengen login, jadi ada login
                kalau memang perlu login dan harus nulis ulang, ya gk masalah, karena kita bisa tahu kalau aplikasinya butuh action apa saja
            jika diperlukan secara global, ubah menjadi standalone action
        update old code
            benerin upload file, di beforeSave ada yang dipindahin ke beforeValidate
            penamaan view
            datatables
                benerin petik petik di list.js
                hilangin &nbsp;
            if(isset($value['id']) && $value['id'] menjadi if ($value['id']
            $error = true;                        mjd $error = true;
            -
            copas folder console terus di rename
            use actionDatatables
            benerin style button
        in error page: go to previous page
Directory Structure
    app_new
        assets/              contains custom app asset
        assets_manager/      contains asset manager for assets folder
        config/              contains web app configuration
        console
            config/          contains console app configuration
            controllers/     contains console app controller
            migrations/      contains database migrations
            runtime/         contains generated files during runtime
            yii              is entry script for console app
        controllers/         contains web controller
        environments/        contains files to be copied when changing environment
            dev/             contains files for dev environment
            prod/            contains files for prod environment
            index.php        is configuration for changing environment
            init             is entry script for changing environment
        models/              contains app specific model
        other/               contains all files that is not needed by app to run
        runtime/             contains generated files during runtime
        views/               contains view files for web app
        web/
            app/             contains custom app asset, called without asset manager
            assets/          contains generated assets by assets_manager
            img/             contains static img asset
            upload/          contains uploaded file
            .htaccess        is apache config for pretty url
            index.php        is entry script for web app
            robots.txt       is configuration for Search Engine Crawler
    technosmart
        assets/              contains assets for all apps
            technoart/       contains technoart template
            technosmart/     contains custom technosmart asset
        assets_manager/      contains asset manager for assets folder, used by all apps
        config/              contains configuration for all apps
        models/              contains model for all apps
        modules/             contains common features
        other/               contains all files that is not needed by app to run
        yii/                 contains yii2 framework override
    vendor/                  contains our yii2 framework
Advance Example
    tabel
        beberapa pilihan primary key diantaranya
            auto increment system
            combination increment tabel sequence: integer/lowercase/uppercase/combination
            mengandung makna Proses Bisnis (Custom Code)
            user defined, cek dulu ke database agar tidak duplikat
        memiliki created_by, updated_by, created_at, updated_by, jika diperlukan
        memiliki status: deleted, disabled, actived, draft, jika diperlukan
    list (datatables, infinite scroll)
        server side datasource
        search all and search each column
        various search type (input text, dropdown, checkbox or radio, range)
        action to another page
        action pop modal up
        action call ajax (with confirmation & then redraw)
        bulk action by checkbox (ex: for delete)
        export to excel, pdf, clipboard, etc
    form
        production standart
            use try catch
            use transaction
        input type
            1 to many & enum (dropdown, radio, dropdown by search)
            Many to many & set (dropdown multiple, checkbox)
            Upload file
            Upload picture with preview
            Time
            Date
            Datetime
            Masked input (phone, currency)
            WYSIWYG
        validation
            client side standalone ?
            server side
            by ajax
        dinamis input with validation & various input type
    permission
    migration
    alter database
    dll
        change environment
        email
        download html
        chart
        akses api
        file cache, memory cache
        file download with auth
        --
        unit test
        stress test, dev db with initial data from faker
        handling error ke email developer
        cronjob
        lazy loading
        elastic search (for catalog search)
    common feature
        role & permission management
        auth
            register
            activate
            login using username/email/hp/both
            forgot password
            reset password
            view profile
            edit profile
            change password
            logout
        user management
            detail
            create
            update
            disable
            soft delete
            reset password
Protokol Init New Project
    hapus
        folder & file yang ada di gitignore
        dev & common feature
            console/controllers/DevController
            controllers/DevController
            models/Dev*
            view/dev
            web/app/dev
            web/upload/dev-file
    rename this
        folder environments/dev/app_starter mjd app_new
        folder environments/prod/app_starter mjd app_new
        app_starter mjd app_new
        dbname=starter mjd app_new
    create db app_new
    starter di other/db.sql mjd new-app (ubah jadi new_app) & import
    cmd
        php init
        yii migrate-init
        create app db migration
        yii migrate
        yii migrate-dev
        yii init/permission
    jika child app
        tambah bootstrap.php Yii::setAlias('@app_new_parent', dirname(dirname(__DIR__)) . '/app_new_parent');
        useridentity pakai model/user di parent
        dbname=app_new_parent
    choose layout
        pilih layout/main.php atau layout/main-blog.php
        rename menjadi layout/main.php
        hapus yang tidak dipilih
    choose homepage
        pilih site/index.php atau site/index-blog.php
        rename menjadi site/index.php
        hapus yang tidak dipilih
    implementasi auth & user: normalize model/User, model/Login, site/signup
        model user
            tidak boleh diubah
            wajib override rule
            wajib dijadiin user_identity
            tabel user kolom akunnya bisa required dengan cara migration
            site/signup & user/create wajib pakai scenario password
            actionSignup wajib use modified model user langsung
            jika di production actionSignup sebaiknya override dari awal
        user management: dev/user/* | user/*
            controllers/UserController
            models/UserExtend
            view/user
            web/app/user
        model user: no connection | connection ke user_extend
        actionSignup: override copas (agar bisa manggil modified user) | override with modifying (save model user_extend)
        (Login) actionLogin: override copas (agar bisa manggil modified Login) | sama
        (Login) model login: override copas getUser() (agar bisa manggil modified Login) | sama
        (Login) model user: override do nothing | override User::findBy
Protokol Development
    ubah nama tabel
        rename nama kelas controller
        rename nama kelas model
        rename nama folder view
        rename nama folder web/app
        rename list-index.js bagian url
    ubah kolom
        model
            change di komentar
            change di rule
            change di label
        view
            change detail
            change list-index
            change form
        web/app
            change list.js
    ubah database
    clear cache
        yii init/cache
    ubah permission
        yii init/permission (setiap kali mengganti permission di controller)
    penamaan view
        (list/detail/form)
        (list/detail/form)-action
        action saja (jika isi viewnya campur2 atau nama actionnya adalah list/detail/form)
        contoh:
            pegawai/index
                list
                detail
            pegawai/create
                form
                atau from-create
            pegawai/update
                form
                atau from-update
            ---
            kepegawaian/pegawai
                list-pegawai
                detail-pegawai
            kepegawaian/pegawai-create
                form-pegawai
                atau form-pegawai-create
            kepegawaian/pegawai-update
                form-pegawai
                atau form-pegawai-update
Cheatsheet
    database
        yii\db\ActiveRecord::find() return yii\db\ActiveQuery which extends yii/db/query
        yii\db\Query dbms-independent method call yii\db\QueryBuilder dbms-dependent method, create yii\db\Command, call yii\db\Command method
        dao
            $params = [':id' => $_GET['id'], ':status' => 1];
            $post = Yii::$app->db->createCommand('SELECT * FROM post WHERE id=:id AND status=:status')->bindValues($params)->queryOne();
            $post = Yii::$app->db->createCommand('SELECT * FROM post WHERE id=:id AND status=:status', $params)->queryOne();
            Yii::$app->db->createCommand()->insert('user', [
                'name' => 'Sam',
                'age' => 30,
            ])->execute();
            Yii::$app->db->createCommand()->batchInsert('user', ['name', 'age'], [
                ['Tom', 30],
                ['Jane', 20],
                ['Linda', 25],
            ])->execute();
            Yii::$app->db->createCommand('UPDATE post SET status=1 WHERE id=1')->execute();
            Yii::$app->db->createCommand()->update('user', ['status' => 1], 'age > 30')->execute();
            Yii::$app->db->createCommand()->delete('user', 'status = 0')->execute();
        query builder
            $query = new \yii\db\Query()
                ->select(['id', 'name'])
                ->addSelect('id, name')
                ->from('public.user u, public.post p');
                ->from(['public.user u', 'public.post p']);
                ->where('status=:status', [':status' => $status])
                ->andWhere(['status' => 1, 'type' => [1, 2]])
                ->orWhere(['like', 'name', 'test'])
                ->andWhere(['and', 'type=1', ['or', 'id=1', 'id=2']])
                ->where(['between', 'date', "2014-12-31", "2015-02-31" ])
                ->filterWhere()
                ->filterAndWhere()
                ->filterOrWhere()
                ->orderBy(['id' => SORT_ASC, 'name' => SORT_DESC])
                ->addOrderBy('id ASC, name DESC')
                ->groupBy(['id', 'status'])
                ->addGroupBy('age')
                ->having()
                ->andHaving()
                ->orHaving()
                ->limit(10)
                ->offset(10)
                ->join('LEFT JOIN', 'post', 'post.user_id = user.id')
                ->union($query2)
                
                ->indexBy('id') // returns [100 => ['id' => 100, 'username' => '...', ...], 101 => [...], 103 => [...], ...]
                ->indexBy(function ($row) {
                    return $row['id'] . $row['name'];
                })
                
                ->all() //returns an array of rows with each row being an associative array of name-value pairs.
                ->one() //returns the first row of the result. use with limit(1)
                ->column() //returns the first column of the result.
                ->scalar() //returns a scalar value located at the first row and first column of the result.
                ->exists() //returns a value indicating whether the query contains any result.
                ->count() //returns the result of a COUNT query.
                
                ->sum('count')
                ->average('count')
                ->max('count')
                ->min('count')

                ->batch($size = 100) or ->each()
            $command = $query->createCommand();
            echo $command->sql; // show the SQL statement
            print_r($command->params); // show the parameters to be bound
            $rows = $command->queryAll(); // returns all rows of the query result
        active record
            $customers = Customer::find() //return active query
                ->indexBy('id') //we can call method in query building from yii\db\Query
                ->all(); //return active record
            
            $customer = Customer::findOne(123); //scalar
            $customers = Customer::findAll([100, 101, 123, 124]); //array of scalar
            $customer = Customer::findOne(['id' => 123, 'status' => Customer::STATUS_ACTIVE]); //an associative array

            Working with Relational Data
            class Customer extends ActiveRecord
            {
                // ...

                public function getOrders()
                {
                    return $this->hasMany(Order::className(), ['customer_id' => 'id']);
                }
            }

            class Order extends ActiveRecord
            {
                // ...

                public function getCustomer()
                {
                    return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
                }
            }

            // SELECT * FROM `customer` WHERE `id` = 123
            $customer = Customer::findOne(123);

            // SELECT * FROM `order` WHERE `customer_id` = 123
            // $orders is an array of Order objects
            $orders = $customer->orders;

            When you access a relation property for the first time, a SQL statement will be executed, like shown in the above example. If the same property is accessed again, the previous result will be returned without re-executing the SQL statement. To force re-executing the SQL statement, you should unset the relation property first: unset($customer->orders).

                Note: While this concept looks similar to the object property feature, there is an important difference. For normal object properties the property value is of the same type as the defining getter method. A relation method however returns an yii\db\ActiveQuery instance, while accessing a relation property will either return a yii\db\ActiveRecord instance or an array of these.

                $customer->orders; // is an array of `Order` objects
                $customer->getOrders(); // returns an ActiveQuery instance

                This is useful for creating customized queries, which is described in the next section.

                
                Relations via a Junction Table
                When declaring such relations, you would call either via() or viaTable() to specify the junction table. The difference between via() and viaTable() is that the former specifies the junction table in terms of an existing relation name while the latter directly uses the junction table. For example,

            class Order extends ActiveRecord
            {
                public function getItems()
                {
                    return $this->hasMany(Item::className(), ['id' => 'item_id'])
                        ->viaTable('order_item', ['order_id' => 'id']);
                }
            }

            or alternatively,

            class Order extends ActiveRecord
            {
                public function getOrderItems()
                {
                    return $this->hasMany(OrderItem::className(), ['order_id' => 'id']);
                }

                public function getItems()
                {
                    return $this->hasMany(Item::className(), ['id' => 'item_id'])
                        ->via('orderItems');
                }
            }

            The usage of relations declared with a junction table is the same as that of normal relations. For example,

            // SELECT * FROM `order` WHERE `id` = 100
            $order = Order::findOne(100);

            // SELECT * FROM `order_item` WHERE `order_id` = 100
            // SELECT * FROM `item` WHERE `item_id` IN (...)
            // returns an array of Item objects
            $items = $order->items;

            Lazy Loading
            // SELECT * FROM `customer` WHERE `id` = 123
            $customer = Customer::findOne(123);

            // SELECT * FROM `order` WHERE `customer_id` = 123
            $orders = $customer->orders;

            // no SQL executed
            $orders2 = $customer->orders;

            // SELECT * FROM `customer` LIMIT 100;
            // SELECT * FROM `orders` WHERE `customer_id` IN (...)
            $customers = Customer::find()
                ->with('orders')
                ->limit(100)
                ->all();

            foreach ($customers as $customer) {
                // no SQL executed
                $orders = $customer->orders;
            } will generate 101 sql

            To solve this performance problem, you can use the so-called eager loading approach as shown below,

            // SELECT * FROM `customer` LIMIT 100;
            // SELECT * FROM `orders` WHERE `customer_id` IN (...)
            $customers = Customer::find()
                ->with('orders')
                ->limit(100)
                ->all();

            foreach ($customers as $customer) {
                // no SQL executed
                $orders = $customer->orders;
            } will generate 2 sql
    snippet
        Yii::getAlias('@yii')
        Yii::getAlias('@app')
        Yii::getAlias('@runtime')
        Yii::getAlias('@webroot')
        Yii::getAlias('@web')
        Yii::getAlias('@vendor')
        Yii::getAlias('@bower')
        Yii::getAlias('@npm')
        Yii::getAlias('@technosmart')
        Yii::getAlias('@app_recruit')
        Yii::getAlias('@console')
        __DIR__
        dirname(__DIR__)
        php_sapi_name()
        get_class(Yii::$app)
        "Url::to(['site/index', 'id' => 1]) : " . Url::to(['site/index', 'id' => 1]) . '<br>' .
        "Url::to(['index', 'id' => 1]) : " . Url::to(['index', 'id' => 1]) . '<br>' .
        "Url::to(['site', 'id' => 1]) : " . Url::to(['site', 'id' => 1]) . '<br>' .
        "Url::to(['asdf', 'id' => 1]) : " . Url::to(['asdf', 'id' => 1]) . '<br>' .
        "Url::to(['/', 'id' => 1]) : " . Url::to(['/', 'id' => 1]) . '<br>' .
        "Url::to(['', 'id' => 1]) : " . Url::to(['', 'id' => 1]) . '<br>' .
        "Url::to() : " . Url::to() . '<br>' .
        "Url::to(['dev/index', 'id' => 1]) : " . Url::to(['dev/index', 'id' => 1]) . '<br>' .
        "Url::to(['dev/form', 'id' => 1]) : " . Url::to(['dev/form', 'id' => 1]) . '<br>' .
        di child app, jika ingin join table melalui model di app_new_parent : return $this->hasOne(\app_new_parent\models\User::className(), ['id' => 'id']);
    expert
        perbedaan with dan joinWith
            https://stackoverflow.com/questions/25600048/what-is-the-difference-between-with-and-joinwith-in-yii2-and-when-to-use-them

        perbedaan where clause and on clause
            https://stackoverflow.com/questions/354070/sql-join-where-clause-vs-on-clause

        kl mau manggil with, harus definisiin relationnya dulu

        kl mau nambahin select di active record, harus tambahin public property nya dulu di model, dimana public propertynya bisa dikombinasiin dengan method getProperty() dan setProperty()

        kl mau nambahin public property di asArray ? gak bisa

            add select | add property
        ->  bisa       | bisa
        []  bisa       | tidak bisa
Development Rules
    standar penamaan
        variabel camelCase
        variabel for column snake_case
        variabel for row camelCases
        function camelCase
        class CamelCase
        html attribute & value snake-case
        url snake-case dari actionnya yang actionCamelCase
        class file CamelCase.php
        view file snake-case.php
        partial view file _snake-case.php
    tinggalkan code untuk dicek nanti
        kasih komen <!-- cek --> diakhiri dengan <!-- endcek -->
    return api
        error from user
            return [
                'code' => 400,
                'message' => 'Validation Failed',
                'errors' => {
                    [
                        'column' => 'error',
                    ],
                },
            ];
        error from server
            return [
                'code' => 500,
                'message' => 'Internal Server Error',
                'description' => 'We\'ve faced a problem creating the pesanan, please contact us for assistance.',
            ];
        success
            return [
                'code' => 200,
                'message' => 'Data Inserted',
                'description' => 'Pesanan created successfully',
            ];
            return [
                'code' => 200,
                'message' => 'Data Found',
                'data' => [
                    'karyawanBanks' => [
                        index => [
                            'column' => 'name',
                        ],
                    ],
                    'karyawanBank' => [
                        'column' => 'name',
                    ],
                ],
            ];

/*========================================================================*\
  Non Code
\*========================================================================*/
UI Breakdown
    Jenis-jenis komponen berdasarkan fungsi semanticnya
        h1 - h6, p, span, div, ul ol li, img
        a, button, all input
    Jenis-jenis komponen berdasarkan behavior
        Information
            give meaning
            do not add if it is not Copywriting
        Action-reaction (input + navigasi)
            do not give meaning
            do not add if it is not Clearwriting
    Jenis-jenis komponen pada kenyataannya
        Unique Information
            Do not share similiar UI / semantic function with other component
        List Information
            Multiple Informations with similiar style
            Can have main action-reaction : filter, sorting, export
            Can have bulk action-reaction : multiple delete
            Can have single action-reaction : update, delete
            Better only preview information
        Detail Information
            One item in list information but with detailed information
        Form
            Submit form with 1 independent button
        Navigasi
            Only action-reaction
Authorization
    Hint
        fitur adalah aksi yang disertai oleh reaksi; dan informasi
        component adalah element2 di html penyusun fitur (navigasi, informasi dan input)
        bisa muncul/diakses fitur ditentukan oleh hak akses yang dimiliki oleh role yang dimiliki oleh user

    kesimpulan
        1.  1 user bisa diassign ke banyak roles
        2.  1 role bisa digrant banyak permissions
        3.  1 permission bisa melihat/mengakses 1 atau beberapa url atau controller/action
        4.  1 url atau controller/action memproses 1 atau beberapa kondisi yang berbeda tergantung dari params get/post
        5.  1 kondisi merender 1 gui
        6.  1 gui terdiri dari 1 atau beberapa fitur.
        7.  1 independent fitur terdiri dari 1 atau beberapa dependent fitur
        8.  1 independent/dependent component/fungsionalitas bisa menuju atau ajax request ke 1 url atau controller/action w/wo params get/post

    contoh
        1.  (user administrator) punya (role admin) dan (role manage user)
        2.  (role manage user) punya akses untuk (melihat data2 user), (menambah data2 user), (mengubah data2 user), (menghapus data2 user)
        3.  (permission menambah data2 user) bisa melihat/mengakses url atau controller/action (user/create)
        4.  url atau controller/action (user/create) bisa memproses kondisi (new request di browser), (validasi ajax), (submittan form)
        5.  (kondisi new request di browser) merender gui (form-create)
        6.  (gui form-create) terdiri dari fitur (tambah data user)
        7.  (tambah data user: tombol submit) terdiri dari beberapa dependent fitur (validasi), (tombol reset)
        8.  (tambah data user: tombol submit dan validasi) menuju/ajax ke url atau controller/action (user/create) with params post
Urutan Pengerjaan Projek
    Menentukan apa yang harus dikerjakan
        BPMN: Garis besar bisnis proses disusun secara kronologi, bisnis proses online menjadi fitur
        Mockup: Layout + Navigation Menu + Pages Content (view)
            Mengedukasi user & nyaman dipakai, karena masalah tidak akan selesai jika user tidak bisa / malas menggunakan aplikasi
            UX: Ease of reading characters on screen
            UX: Organisation of menu, information & input on each page must educate user
            UX: Behavior of flow when using the system must make sense and ease
            UI: Interactivity of website interface
        Url tree: Controller/Action
        ERD
            Simple/easy to implement
            Must group table into object and relate it into fitur
    Pengerjaan
        Implementasi
            Maintainable Code
            Fast Development
            System Reliability: Can it satisfactorily perform when processing all possible input as planned, with minimum time spend, in a specified environment, for a long time period
        Pengujian
    Project support
        Laporan Pengerjaan, Manual Guide, Video Guide
Struktur Proposal Produk
    Judul Formal - Tagline
    Inti proposal
        Gimana caranya agar orang yang tidak tertarik memahami keseluruhan isi proposal
        Tidak terlihat jualan
        Bahas permasalah + solusi, tanpa menjelaskan produk
        Disusun dalam bentuk infographic
    Masalah / kondisi existing
        Apa yang akan diselesaikan oleh solusi
    Solusi
        Perkenalan solusi ke client
        Infographic alur solusi
        Pembahasan masing2 solusi dan betapa penting manfaatnya jika solusi diterapkan
        Screenshot solusi
    Price - Choose what suits you

    Tantangan : Menjelaskan sedetail mungkin tanpa membosankan
    Menggunakan Icon agar eye catching
    Lebih baik banyak paragraf dari pada 1 paragraf banyak kalimat
Postponed Plan
    backend : RoR, Django, Java, Go Lang
    frontend : Vue, Android, React & React Native, iOS, Angular & Ionic
    recruit
        Test
        Lengkapin fitur2
        Todo
            Required declaration
            selection dan view dibalik
            kata Search menjadi Filter
            time remaining di online test
            tidak ada tanggal seleksi yang berhimpitan
            tanggal seleksi tidak bisa dimundurin ke kemarin
            tombol apply now menjadi you have applied to this job
            tombol start now menjadi you have taken this test
            3 total di selection
        Fitur
            applicant
                My Vacancy
                    Online test v
                    Download v
                    Completed
                Update Profile v
                Announcement & Article x
            recruitment team
                Manage Vacancy
                    Selection v
                    Reschedule v
                    (ditrigger oleh cronjob)
                    Update Info v
                Manage User x
                Announcement & Article x
                Company (pages2) x
        Marketing
            video fiverr : isi paypal
            instagram
            ajak kerja sama (dr semua media sosial)
            telpon, gratis 6 bulan, bisa lihat demonya
/*========================================================================*\
  Other
\*========================================================================*/
touch ~/.android/repositories.cfg
sdkmanager --licenses (belum tentu wajib, boleh dijalankan)
sdkmanager "platforms;android-26" (juga ngedownload folder build-tools/26.0.2 dan platform-tools)

sdkmanager "system-images;android-26;google_apis;x86" (juga ngedownload folder emulator)
avdmanager create avd -n google9 -k "system-images;android-26;google_apis;x86" -d "Nexus 9" -f (ngga nambahin apa2 di folder sdk, tapi nambahin isi di folder .android/avd)
emulator @google9 (gk bisa karena butuh hardware acceleration, sehingga ngga bisa lanjut ke tahap selanjutnya yang dijelasin oleh https://stackoverflow.com/questions/4974568/how-do-i-launch-the-android-emulator-from-the-command-line)

gradlew installMockDebug --offline -a
adb shell am start -n yourpackagename/.activityname